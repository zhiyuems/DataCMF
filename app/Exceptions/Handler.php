<?php
namespace App\Exceptions;
use Throwable;
use App\Bin\Enum\ApiCode;
use App\Bin\Enum\ApiSubCode;
use App\Bin\Enum\LogsLevel AS LogsLevelEnum;
use App\Service\LogsService;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Arr;
use ToolLibrary\Unit\Tools;

class Handler extends ExceptionHandler
{

    /**
     * 未报告的异常类型的列表.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * 验证异常从未刷新的输入列表.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * 注册应用程序的异常处理回调.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * 将身份验证异常转换为响应.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return $request->expectsJson()
            ? apiFailResponse('INVALID_AUTH_TOKEN', null, 'ERROR_INSUFFICIENT_AUTHORIZATION')
            : redirect()->guest($exception->redirectTo() ?? route('login'));
    }

    /**
     * 将验证异常转换为JSON响应。
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Validation\ValidationException  $exception
     * @return \Illuminate\Http\JsonResponse
     */
    protected function invalidJson($request, ValidationException $exception)
    {
        // 待定
        dd('invalidJson?');
        return response()->json([
            'message' => $exception->getMessage(),
            'errors' => $exception->errors(),
        ], $exception->status);
    }

    /**
     * 将给定的异常转换为数组。
     *
     * @param  \Throwable  $e
     * @return array
     */
    protected function convertExceptionToArray(Throwable $e)
    {
        $_data = $this->_formatThrowableData($e);
        $response = apiFailResponse('UNKNOW_ERROR', $e->getMessage(), 'ERROR_SERVICE_UNAVAILABLE' , $_data)
            ->getContent();
        if (Tools::isJson($response)){
            return json_decode($response, true);
        }else{
            $response = [];
            $responseConfig = config('api.response_configure');
            $response[$responseConfig['code']] = ApiCode::ERROR_SERVICE_UNAVAILABLE;
            $response[$responseConfig['msg']] = ApiCode::getMsg($response[$responseConfig['code']], 'name');
            $response[$responseConfig['sub_code']] = ApiSubCode::UNKNOW_ERROR;
            $response[$responseConfig['sub_msg']] = ApiSubCode::getMsg($response[$responseConfig['sub_code']], 'name');
            $response[$responseConfig['data']] = $_data;
            return $response;
        }
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $e
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $e)
    {
        $this->_logRecord($request, $e);
        return parent::render($request, $e);
    }

    protected function _logRecord($request, Throwable $e)
    {
        $_data = $this->_formatThrowableData($e);
        $response = apiFailResponse('UNKNOW_ERROR', $e->getMessage(), 'ERROR_SERVICE_UNAVAILABLE' , $_data);

        $_logsService = new LogsService('system');
        $_logsService->setLogLevel(LogsLevelEnum::ERROR);
        $_logsService->setRequest($request);
        $_logsService->setResponse($response);
        $_logsService->record_sys($e->getMessage());

    }

    protected function _formatThrowableData(Throwable $e)
    {
        $_data = [];
        if (config('app.debug')) {
            $_data['exception'] = get_class($e);
            $_data['file'] = $e->getFile();
            $_data['line'] = $e->getLine();
            $_data['trace'] = collect($e->getTrace())->map(function ($trace) {
                return Arr::except($trace, ['args']);
            })->all();
        }
        return $_data;
    }

}
