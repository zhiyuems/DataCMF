<?php
namespace App\Rules;
use Illuminate\Contracts\Validation\Rule;
use ToolLibrary\Unit\Tools;

/**
 * Meta元数据验证器
 */
class Meta implements Rule
{
    protected $meta = [];
    protected $transKey = 'meta';
    protected $error = [];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(array $meta)
    {
        $this->meta = $meta;
    }

    /**
     * 判断是否通过验证规则。
     *
     * @param  string  $attribute 待验证表单字段名称
     * @param  mixed  $value      待验证表单字段值
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ( !is_array($value) ){
            $value = Tools::isJson($value)?json_decode($value, true):[];
        }

        if ( empty($value) ){
            $this->transKey = 'meta';
            return false;
        }

        // 检查可支持的mate
        array_map(function($metaKey) use (&$value){
            if (array_key_exists($metaKey, $value)){
                unset($value[$metaKey]);
            }
        }, $this->meta);

        if ( empty($value) ){
            return true;
        }

        $this->transKey = 'meta_between';
        $this->error['key'] = key($value);

        return empty($value);
    }

    /**
     * 获取校验错误信息。
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.'.$this->transKey,[
            'meta' => implode(', ', $this->meta)
        ]);
    }
}
