<?php
namespace App\Models\Permission;
use App\Models\BaseModels AS Model;
use App\Bin\Traits\Permission\HasModels;

/**
 * 模型 - 角色关联权限
 */
class RolesHasPermission extends Model
{
    use HasModels;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [];

    /**
     * 角色外键字段名称
     *
     * @var string $rolePivotKey
     */
    private $rolePivotKey;

    /**
     * 权限外键字段名称
     *
     * @var string $permissionPivotKey
     */
    private $permissionPivotKey;

    public function __construct (array $attributes = [])
    {
        $attributes['guard_name'] = $attributes['guard_name'] ?? config('auth.defaults.guard');

        $this->fillable[] = $this->rolePivotKey
            = config('permission.column_names.role_pivot_key','role_id');
        $this->fillable[] = $this->permissionPivotKey
            = config('permission.column_names.permission_pivot_key','permission_id');

        parent::__construct($attributes);
    }

    public function getTable()
    {
        return config('permission.table_names.role_has_permissions', parent::getTable());
    }

    /**
     * 清空指定角色权限
     *
     * @param int $roleId
     *
     * @return bool
     */
    public function clearRolePermissionTo($roleId)
    {
        return $this->where($this->rolePivotKey, $roleId)
            ->delete();
    }

}
