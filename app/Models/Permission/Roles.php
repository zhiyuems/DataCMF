<?php
namespace App\Models\Permission;
use Spatie\Permission\Contracts\Permission;
use Spatie\Permission\Contracts\Role;
use Spatie\Permission\Models\Role AS Model;
use Illuminate\Support\Facades\DB;
use App\Bin\Traits\Permission\HasModels;
use App\Models\Permission\RolesHasPermission AS mRolesHasPermission;
use Spatie\Permission\PermissionRegistrar;

class Roles extends Model
{
    use HasModels;

    public function __construct (array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'team_id',
        'name',
        'alias',
        'parent_id',
        'sort',
        'guard_name',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * 获取指定角色信息
     *
     * @param int $roleId 角色ID
     *
     * @return \Illuminate\Database\Eloquent\Model|object|static|null
     */
    public function oneInfo($roleId, $teamId)
    {
        return $this->whereGuardName($this->guard_name)
            ->whereTeamId($teamId)
            ->where($this->getKeyName(), $roleId)
            ->first();
    }

    /**
     * 通过角色ID获取相关角色信息
     *
     * @param array|int $id
     * @param array $field
     * @param int $teamId
     * @param string $guardName
     *
     * @return \Illuminate\Database\Eloquent\Model|object|static|null
     */
    public function roleIdInfo($id, $field=null, $teamId=null, $guardName=null, $isPluck=false)
    {
        $id = is_array($id)?$id:[$id];
        $_m = $this->whereGuardName($guardName?:$this->guard_name)
            ->whereTeamId($teamId)
            ->whereIn('id',$id);

        return $isPluck?
            $_m->pluck($field[0],$field[1]):
            $_m->select($field)->get();
    }

    /**
     * 清空角色全部权限
     *
     * @param int $roleId
     *
     * @return bool
     */
    public function clearPermissionTo($roleId, $teamId=null)
    {
        DB::beginTransaction();

        // 清空角色相关权限关联
        $_mRolesHasPermission = new mRolesHasPermission;
        $_mRolesHasPermission->guard_name = $this->guard_name;
        $_mRolesHasPermission->clearRolePermissionTo($roleId);

        // 清空角色
        if ( $this->whereGuardName($this->guard_name)->whereTeamId($teamId)
            ->where($this->getKeyName(), $roleId)->delete() )
        {
            DB::commit();
            return true;
        }
        DB::rollBack();
        return false;
    }

    /**
     * Grant the given permission(s) to a role.
     *
     * @param string|int|array|\Spatie\Permission\Contracts\Permission|\Illuminate\Support\Collection $permissions
     *
     * @return $this
     */
    public function givePermissionTo(...$permissions)
    {
        $permissions = collect($permissions)
            ->flatten()
            ->reduce(function ($array, $permission) {
                if (empty($permission)) {
                    return $array;
                }

                $permission = $this->getStoredPermission($permission);
                if (! $permission instanceof Permission) {
                    return $array;
                }

                try {
                    if (!$permission->guard_name)$permission->guard_name=$permission->toArray()['guard_name'];
                    if (!$permission->teams_id)$permission->teams_id=$permission->toArray()['teams_id'];
                }catch (\Exception $e){
                    //
                }

                $this->ensureModelSharesGuard($permission);

                $array[$permission->getKey()] = PermissionRegistrar::$teams && ! is_a($this, Role::class) ?
                    [PermissionRegistrar::$teamsKey => getPermissionsTeamId()] : [];

                return $array;
            }, []);

        $model = $this->getModel();

        if ($model->exists) {
            $this->permissions()->sync($permissions, false);
            $model->load('permissions');
        } else {
            $class = \get_class($model);

            $class::saved(
                function ($object) use ($permissions, $model) {
                    if ($model->getKey() != $object->getKey()) {
                        return;
                    }
                    $model->permissions()->sync($permissions, false);
                    $model->load('permissions');
                }
            );
        }

        if (is_a($this, get_class(app(PermissionRegistrar::class)->getRoleClass()))) {
            $this->forgetCachedPermissions();
        }

        return $this;
    }
}
