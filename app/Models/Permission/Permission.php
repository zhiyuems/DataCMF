<?php
namespace App\Models\Permission;
use Spatie\Permission\Models\Permission AS Model;
use App\Bin\Traits\Permission\HasModels;
/**
 * 系统模型 - 后台菜单(权限明细)
 */
class Permission extends Model
{
    use HasModels;

    /**
     * 要返回分页的模型数.
     *
     * @var int
     */
    protected $perPage = 20;

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * 获取指定菜单信息
     *
     * @param int $roleId 菜单ID
     *
     * @return \Illuminate\Database\Eloquent\Model|object|static|null
     */
    public function oneInfo($roleId)
    {
        return $this->whereGuardName($this->guard_name)
            ->where($this->getKeyName(), $roleId)
            ->first();
    }

}
