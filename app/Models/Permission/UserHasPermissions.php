<?php
namespace App\Models\Permission;
use App\Models\BaseModels AS Model;
use App\Bin\Traits\Permission\HasModels;

/**
 * 模型 - 用户关联权限
 */
class UserHasPermissions extends Model
{
    use HasModels;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [];

    /**
     * 权限外键字段名称
     *
     * @var string $permissionsPivotKey
     */
    private $permissionsPivotKey;

    /**
     * 用户外键字段名称
     *
     * @var string $userPivotKey
     */
    private $userPivotKey;

    /**
     * 权限认证模块标识外键字段名称
     *
     * @var string $teamPivotKey
     */
    private $teamPivotKey;

    /**
     * 权限模块
     *
     * @var string $modelType
     */
    private $modelType = \App\Models\System\Admin::class;

    public function __construct (array $attributes = [])
    {
        $this->fillable[] = $this->permissionsPivotKey
            = config('permission.column_names.permission_pivot_key','permission_id');
        $this->fillable[] = 'model_type';
        $this->fillable[] = $this->userPivotKey
            = config('permission.column_names.model_morph_key','model_id');
        $this->fillable[] = $this->teamPivotKey
            = config('permission.column_names.team_foreign_key','team_id');

        parent::__construct($attributes);
    }

    public function getTable()
    {
        return config('permission.table_names.model_has_permissions', parent::getTable());
    }

    /**
     * 清空指定用户权限
     *
     * @param int $userId
     *
     * @return bool
     */
    public function clearUserPermissionTo($userId, $teamId=null)
    {
        return $this->whereTeamId($teamId)
            ->where('model_type', $this->modelType)
            ->where($this->userPivotKey, $userId)
            ->delete();
    }

}
