<?php
namespace App\Models\Permission;
use App\Models\BaseModels AS Model;
use App\Bin\Traits\Permission\HasModels;

/**
 * 模型 - 用户关联角色
 */
class UserHasRoles extends Model
{
    use HasModels;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [];

    /**
     * 角色外键字段名称
     *
     * @var string $rolePivotKey
     */
    private $rolePivotKey;

    /**
     * 权限外键字段名称
     *
     * @var string $permissionPivotKey
     */
    private $modelPivotKey;

    /**
     * 权限模块
     *
     * @var string $modelType
     */
    private $modelType = \App\Models\System\Admin::class;

    public function __construct (array $attributes = [])
    {
        $this->fillable[] = $this->rolePivotKey
            = config('permission.column_names.role_pivot_key','role_id');
        $this->fillable[] = 'model_type';
        $this->fillable[] = $this->modelPivotKey
            = config('permission.column_names.model_morph_key','model_id');

        parent::__construct($attributes);
    }

    public function getTable()
    {
        return config('permission.table_names.model_has_roles', parent::getTable());
    }

    /**
     * 清空指定用户角色
     *
     * @param int $userId
     *
     * @return bool
     */
    public function clearUserRoleTo($userId, $teamId=null)
    {
        return $this->whereGuardName($this->guard_name)->whereTeamId($teamId)
            ->where('model_type', $this->modelType)
            ->where($this->modelPivotKey, $userId)
            ->delete();
    }

}
