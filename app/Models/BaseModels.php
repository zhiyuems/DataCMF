<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Cache;

class BaseModels extends Model
{
    use HasFactory;

    /**
     * 缓存驱动
     *
     * @var \Illuminate\Support\Facades\Cache|\Illuminate\Contracts\Cache\Repository  $_cache
     */
    protected $_cache;

    /**
     * 缓存前缀
     *
     * @var string $_cacheName
     */
    protected $_cacheName = 'DB';

    /**
     * 要返回分页的模型数.
     *
     * @var int
     */
    protected $perPage = 20;

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'created_at'   =>'date:Y-m-d H:i:s',
        'updated_at'   =>'date:Y-m-d H:i:s',
    ];

    public function __construct (array $attributes = [])
    {
        parent::__construct($attributes);
        if(method_exists($this,'_initialize'))call_user_func_array([$this,'_initialize'], func_get_args());
    }

    /**
     * 获取指定信息
     *
     * @param int $id 主键ID
     *
     * @return \Illuminate\Database\Eloquent\Model|object|static|null
     */
    public function oneInfo($id)
    {
        return $this->where($this->getKeyName(), $id)
            ->first();
    }

    /**
     * 缓存驱动调用
     *
     * @param string $store 驱动名称
     *
     * @return \Illuminate\Contracts\Cache\Repository
     */
    public function cache($store=null)
    {
        if (!$this->_cache){
            $store = empty($store)?env('CACHE_DRIVER','file'):$store;
            $this->_cache = Cache::store($store);
        }
        return $this->_cache;
    }

    /**
     * 统一缓存命名格式
     *
     * @param array|string $key
     *
     * @return string
     */
    public function formatCacheName($key=null)
    {
        $key = is_array($key)?[$key]:func_get_args();
        $key = implode(':', $key);
        $key = empty($key)?'*':$key;

        return sprintf('%s:%s', $this->_cacheName, $key);
    }
}
