<?php
namespace App\Models;
use App\Models\BaseModels AS Model;
use Illuminate\Database\QueryException;

/**
 * 模型 - 图片资源
 */
class ResourcesImage extends Model
{
    protected $table = 'resources_image';

    /**
     * 创建资源数据
     *
     * @param array $data
     *
     * @return bool|int
     * @throw \Illuminate\Database\QueryException
     */
    public function create(array $data)
    {
        if (!isset($data['created_at']))$data['created_at']=date('Y-m-d H:i:s');
        if (!isset($data['updated_at']))$data['updated_at']=date('Y-m-d H:i:s');
        try {
            $_rs = $this->insertGetId($data);
        }catch (QueryException $e) {
            report($e);
            $_rs = false;
        }
        return $_rs;
    }

    /**
     * 查询HASH是否存在
     *
     * @param string $hash
     *
     * @return bool
     */
    public function existHash($hash)
    {
        return (bool)$this->where('file_hash', $hash)->value('id');
    }

    /**
     * 删除指定HASH或本地文件
     *
     * @param string $path
     * @param string $hash
     *
     * @return bool
     */
    public function deleteFileOrHash($path, $hash)
    {
        return (bool)$this->where('file_hash', $hash)
            ->orWhere('file_root_path', $path)
            ->delete();
    }
}
