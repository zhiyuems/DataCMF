<?php
namespace App\Models\Sanctum;
use Laravel\Sanctum\PersonalAccessToken as SanctumPersonalAccessToken;

class PersonalAccessToken extends SanctumPersonalAccessToken
{
    /**
     * 要返回分页的模型数.
     *
     * @var int
     */
    protected $perPage = 20;

    /**
     * 查找与给定令牌匹配的令牌实例。
     *
     * @param  string  $token 待查找的令牌字符串，例如：xxx|xxxxx 或 xxxxx
     * @return \Laravel\Sanctum\PersonalAccessToken|null
     */
    public static function findToken($token)
    {
        if (strpos($token, '|') === false) {
            return static::where('token', hash('sha256', $token))->first();
        }

        [$id, $token] = explode('|', $token, 2);

        if ($instance = static::find($id)) {
            return hash_equals($instance->token, hash('sha256', $token)) ? $instance : null;
        }
    }
}
