<?php
namespace App\Models\System;
use App\Models\BaseModels AS Model;

class Dept extends Model
{
    protected $table = 'system_dept';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'parent_id',
        'name',
        'leader',
        'phone',
        'email',
        'sort',
        'status',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];
}
