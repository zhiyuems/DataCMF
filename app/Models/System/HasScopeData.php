<?php
namespace App\Models\System;
use App\Models\BaseModels AS Model;

/**
 * 模型 - 模型数据范围
 */
class HasScopeData extends Model
{
    protected $table = 'system_model_has_scope_data';
}
