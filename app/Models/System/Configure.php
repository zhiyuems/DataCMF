<?php
namespace App\Models\System;
use App\Models\BaseModels AS Model;
use Illuminate\Support\Facades\Cache;
use App\Bin\Enum\Common AS CommonEnum;

/**
 * 模型 - 系统全局配置
 */
class Configure extends Model
{
    protected $table = 'system_configure';

    /**
     * 缓存驱动
     *
     * @var \Illuminate\Support\Facades\Cache|\Illuminate\Contracts\Cache\Repository  $_cache
     */
    protected $_cache;

    /**
     * 缓存key名称
     * @var string $_cacheName
     */
    protected $_cacheName = 'DB:CONFIG';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'variable_label',
        'variable_name',
        'variable_value',
        'variable_tip',
        'is_systemvar',
        'group',
        'group_son',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
    ];

    public function _initialize()
    {
        $this->_cache = Cache::store(env('CACHE_DRIVER','file'));
    }

    /**
     * 创建新变量值
     *
     * @param array  $value   创建变量数据
     * @param string $keyName [可选] 指定变量名数据创建
     *
     * @return \App\Models\System\Configure
     */
    public function createVariableKey(array $value, string $keyName = null)
    {
        if (empty($keyName)){
            array_walk($this->fillable, function($keyField) use (&$value) {
                if (!isset($value[$keyField])){
                    if ($keyField === 'is_systemvar') {
                        $value[$keyField] = CommonEnum::STATUS_CLOSE;
                    } else if ($keyField === 'group') {
                        $value[$keyField] = 'extend';
                    } else if ($keyField === 'group_son') {
                        $value[$keyField] = 'default';
                    } else {
                        $value[$keyField] = null;
                    }
                }
            });
            return $this->create($value);
        }else{
            $value['variable_name'] = $keyName;
            return $this->createVariableKey($value);
        }
    }

    /**
     * 获取指定变量值
     *
     * @param string $keyName
     * @param  string|array|int|null  $key
     * @param  mixed  $default
     *
     * @return mixed
     */
    public function getVariableKeyValue($keyName, $key=null, $default = null, string $keyFieldName='')
    {
        $keyFieldName = empty($keyFieldName)?'variable_name':$keyFieldName;
        if( $this->_cache->has($this->_cacheName) ) {
            $_cacheData = json_decode($this->_cache->get($this->_cacheName), true);
            if ($keyFieldName === 'variable_name'){
                if(!isset($_cacheData[$keyName]))return false;
                $_cacheData = $_cacheData[$keyName];
            }else{
                foreach ($_cacheData AS $item){
                    if ($item[$keyFieldName] == $keyName){
                        $_cacheData = $item;
                        break;
                    }
                }
                unset($item);
            }
        }else{
            $_cacheData = $this->where($keyFieldName, $keyName)->first();
            if (!$_cacheData)return false;
        }
        return empty($key)?$_cacheData:data_get($_cacheData, $key, $default);
    }

    /**
     * 设置指定变量值
     *
     * @param string $key
     * @param array  $value
     *
     * @return bool
     */
    public function setVariableValue(string $key, array $value, string $keyFieldName='')
    {
        $keyFieldName = empty($keyFieldName)?$this->getKeyName():$keyFieldName;

        // 更新系统变量信息
        $_isUpdate = $this->where($keyFieldName, $key)
            ->update($value);

        if ( $_isUpdate ){ // 更新缓存
            $_cacheData = $this->getAllConfig();
            $_cacheData[$key] = $value;
            $this->_cache->set($this->_cacheName, json_encode($_cacheData));
        }
        return $_isUpdate;
    }

    /**
     * 获取全部变量配置(带缓存)
     *
     * @return array
     */
    public function getAllConfig($isCache=true)
    {
        if ( $isCache === true ){
            if( $this->_cache->has($this->_cacheName) ) {
                return json_decode($this->_cache->get($this->_cacheName), true);
            }
        }
        $_list = $this->get()->toArray();

        // 格式化数据
        $_aliasList = [];
        array_map(function($item) use (&$_aliasList) {

            $item['variable_value'] = empty($item['variable_value'])?'':$item['variable_value'];
            $item['variable_tip'] = empty($item['variable_tip'])?'':$item['variable_tip'];

            $_aliasList[$item['variable_name']] = $item;
        }, $_list);
        $_list = $_aliasList;

        if ($isCache===true)$this->_cache->set($this->_cacheName, json_encode($_list));
        return $_list;
    }

    /**
     * 获取所有配置信息并格式化分组
     *
     * @return array
     */
    public function getAllGroupConfig($isSys=null)
    {
        $_list = $this->getAllConfig(true);
        $_config = [];
        array_map(function($item) use (&$_config) {
            $_group = $item['group'];
            if (!isset($_config[$_group]))$_config[$_group]=[];
            $_config[$_group][] = $item;
        }, $_list);

        // 子分组未完成

        return $_config;
    }

    /**
     * 重置配置缓存
     *
     * @return void
     */
    public function configResetCache()
    {
        if( $this->_cache->has($this->_cacheName) ) {
            $this->_cache->forget($this->_cacheName);
            $this->getAllConfig(true);
        }
    }
}
