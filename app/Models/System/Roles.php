<?php
namespace App\Models\System;
use App\Models\BaseModels AS Model;

/**
 * 模型 - 管理角色信息
 */
class Roles extends Model
{
    protected $table = 'system_roles';
}
