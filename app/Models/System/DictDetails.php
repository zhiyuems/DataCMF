<?php
namespace App\Models\System;
use App\Bin\Enum\Common as CommonEnum;
use App\Models\BaseModels AS Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * 模型 - 数据字典 - 字典明细
 *
 * @method DictDetails dictTypeDetails($typeKey)->scopeDictTypeDetails 只获取指定字典类型的字典明细
 */
class DictDetails extends Model
{
    protected $table = 'system_dict_details';

    protected $primaryKey = 'dict_code';

    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'dict_type',
        'dict_code',
        'dict_label',
        'dict_value',
        'remark',
        'sort',
        'status',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'updated_at',
    ];

    /**
     * 字段Map
     *
     * @var array $fieldMap
     */
    protected $fieldMap = [
        'type' => 'dict_type',
        'code' => 'dict_code',
        'label' => 'dict_label',
        'value' => 'dict_value',
    ];

    /**
     * 创建数据字典明细
     *
     * @param array $data
     *
     * @return DictDetails
     */
    public function createDetails(array $data)
    {
        $_data = $this->formatField($data);
        return $this->create($_data);
    }

    /**
     * 获取数据字典明细列表
     *
     * @param string $status 字典类型状态
     *
     * @return DictDetails
     */
    public function getDictList($type=null, $status=null, $paginate=null)
    {
        if ( $status ){
            if(!in_array($status, [CommonEnum::STATUS_OPEN, CommonEnum::STATUS_CLOSE])){
                $status = CommonEnum::STATUS_OPEN;
            }
            $_m = $this->detailsSelect()->where('status', $status);
        }else{
            $_m = $this->detailsSelect();
        }

        if ($type){
            $_m = $_m->where('dict_type', $type);
        }

        return $paginate?$_m->paginate($paginate):$_m->get();
    }

    /**
     * 作用域 - 字典明细 字段别名转化
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeDetailsSelect(Builder $query, $isComplete=true)
    {
        if ($isComplete===true){
            return $query->select('dict_type as type',
                'dict_label as label', 'dict_code as code', 'dict_value as value',
                'remark', 'sort', 'status',
                self::CREATED_AT, self::UPDATED_AT);
        }
        return $query->select('dict_type as type',
            'dict_code as name', 'dict_label as label', 'dict_value as value',
            'remark');
    }

    /**
     * 作用域 - 只获取指定字典类型的字典明细
     *
     * @param Builder $query
     * @param string|array $typeKey
     *
     * @return Builder
     */
    public function scopeDictTypeDetails(Builder $query, $typeKey)
    {
        $_q = $query->detailsSelect(false);
        if (is_array($typeKey)){
            $_q = $_q->whereIn('dict_type', $typeKey);
        }else{
            $_q = $_q->where('dict_type', $typeKey);
        }
        return $_q;
    }

    /**
     * 访问器 - 字典备注 字段格式化
     *
     * @param string $value
     *
     * @return string
     */
    public function getRemarkAttribute($value)
    {
        return is_null($value)?'':$value;
    }

    /**
     * 格式化字段名称
     *
     * @param array $data
     *
     * @return array
     */
    public function formatField(array $data)
    {
        $_data = [];
        array_walk($data, function($value, $fieldName) use (&$_data){
            if (isset($this->fieldMap[$fieldName])){
                $fieldName = $this->fieldMap[$fieldName];
            }
            $_data[$fieldName] = $value;
        });
        return $_data;
    }
}
