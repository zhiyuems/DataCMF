<?php
namespace App\Models\System;
use App\Models\BaseModels AS Model;

/**
 * 模型 - 模型字段范围
 */
class HasScopeField extends Model
{
    protected $table = 'system_model_has_scope_field';
}
