<?php
namespace App\Models\System;
use App\Bin\Enum\UserInfo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Bin\Traits\Sanctum\HasApiTokens;
use App\Bin\Traits\Permission\HasRoles;

class Admin extends Authenticatable
{
    use HasRoles, HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    protected $table = 'system_admin';
    protected $guard_name = 'admin'; // 使用任何你想要的守卫

    public function guardName()
    {
        return $this->guard_name;
    }

    /**
     * 要返回分页的模型数.
     *
     * @var int
     */
    protected $perPage = 20;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'avatar',
        'username',
        'account',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'birthday'   =>'date:Y-m-d',
        'created_at'   =>'date:Y-m-d H:i:s',
        'updated_at'   =>'date:Y-m-d H:i:s',
    ];

    /**
     * 属性修改器 - 生日
     *
     * @return string
     */
    public function getBirthdayAttribute($value)
    {
        return $value?:date('Y-m-d',0);
    }

    /**
     * 属性修改器 - 性别(明文)
     *
     * @return string
     */
    public function getSexLabelAttribute($value)
    {
        return UserInfo::getMsg($this->sex,'name',
            UserInfo::getMsg(UserInfo::SEX_UNKNOWN,'name'));
    }

    /**
     * 属性修改器 - 账号头像URL地址补全
     *
     * @return string
     */
    public function getAvatarAttribute($value)
    {
        return asset($value);
    }

    /**
     * 属性修改器 - 提前管理员所有分配角色名称
     *
     * @return string 例如：'超级管理员,运营管理员'
     */
    public function getRoleNameAttribute()
    {
        return implode(',', $this->roles->pluck('alias')->toArray());
    }

    /**
     * 属性修改器 - 提前管理员所有分配角色ID
     *
     * @return array 例如：[1,2]
     */
    public function getRoleIdsAttribute()
    {
        return $this->roles->pluck('id')->toArray();
    }
}
