<?php
namespace App\Models\System;
use App\Models\BaseModels AS Model;
use App\Models\System\DictDetails AS mDictDetails;
use App\Bin\Enum\Common AS CommonEnum;
use Illuminate\Database\Eloquent\Builder;

class DictType extends Model
{
    protected $table = 'system_dict_type';

    protected $primaryKey = 'dict_key';

    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'dict_label',
        'dict_key',
        'remark',
        'status',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'updated_at',
    ];

    /**
     * 字段Map
     *
     * @var array $fieldMap
     */
    protected $fieldMap = [
        'label' => 'dict_label',
        'code' => 'dict_key',
    ];

    public function _initialize()
    {
        $this->cache();
    }

    /**
     * 获取全部字典类型及明细配置信息(带缓存)
     *
     * @param bool $isCache
     *
     * @return array
     */
    public function getAllDictList(bool $isCache=true)
    {
        if ( $isCache === true ){
            $cacheName = $this->formatCacheName();
            if( $this->_cache->has($cacheName) ) {
                return json_decode($this->_cache->get($cacheName), true);
            }
        }

        $_typeList = $this->get();
        $_typeKeys = data_get($_typeList->toArray(), '*.dict_key');

        $_dictList = (new mDictDetails)->dictTypeDetails($_typeKeys)
            ->get()->toArray();

        $_dictLists = [];
        foreach ($_dictList AS $item){
            $_type = $item['dict_type'];
            unset($item['dict_type'],);
            $item['remark'] = empty($item['remark'])||is_null($item['remark'])?'':$item['remark'];
            if(!isset($_dictLists[$_type]))$_dictLists[$_type]=[];
            $_dictLists[$_type][$item['name']] = $item;

            if ($isCache===true){
                $this->_cache->set($this->formatCacheName($_type,$item['name']),
                    json_encode($item));
            }
        }
        return $_dictLists;
    }

    /**
     * 获取数据字典所有类型
     *
     * @param string $status 字典类型状态
     *
     * @return DictType
     */
    public function getTypeList($status=null)
    {
        if ( $status ){
            if(!in_array($status, [CommonEnum::STATUS_OPEN, CommonEnum::STATUS_CLOSE])){
                $status = CommonEnum::STATUS_OPEN;
            }
        }else{
            $status = CommonEnum::STATUS_OPEN;
        }

        $_m = $this->detailsSelect()->where('status', $status);

        return $_m->get();
    }

    /**
     * 获取相关的所有字典详情列表
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subdict()
    {
        return $this->hasMany(mDictDetails::class,'dict_type','dict_key');
    }

    /**
     * 作用域 - 字典类型 字段别名转化
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeDetailsSelect(Builder $query)
    {
        return $query->select('dict_label as label', 'dict_key as code',
            'remark', 'status',
            self::CREATED_AT, self::UPDATED_AT);
    }

    /**
     * 访问器 - 字典备注 字段格式化
     *
     * @param string $value
     *
     * @return string
     */
    public function getRemarkAttribute($value)
    {
        return is_null($value)?'':$value;
    }

    /**
     * 格式化字段名称
     *
     * @param array $data
     *
     * @return array
     */
    public function formatField(array $data)
    {
        $_data = [];
        array_walk($data, function($value, $fieldName) use (&$_data){
            if (isset($this->fieldMap[$fieldName])){
                $fieldName = $this->fieldMap[$fieldName];
            }
            $_data[$fieldName] = $value;
        });
        return $_data;
    }

}
