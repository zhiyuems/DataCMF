<?php
namespace App\Models\System\Logs;
use App\Models\BaseModels AS Model;

/**
 * 模型 - 日志 - 系统日志
 */
class System extends Model
{
    protected $table = 'system_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'log_type',
        'log_level',
        'header',
        'ip_address',
        'url',
        'param',
        'method',
        'response',
        'operator',
        'notes',
        'class_name',
        'class_method',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'updated_at',
    ];

    /**
     * 访问器 - 请求头信息
     *
     * @return array
     */
    public function getHeaderAttribute($value)
    {
        return json_decode($value, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
    }

    /**
     * 访问器 - 请求参数
     *
     * @return array
     */
    public function getParamAttribute($value)
    {
        return json_decode($value, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
    }

    /**
     * 访问器 - 响应内容
     *
     * @return array
     */
    public function getResponseAttribute($value)
    {
        return json_decode($value, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
    }

    /**
     * 访问器 - 操作人
     *
     * @return array
     */
    public function getOperatorAttribute($value)
    {
        $_value = ['id'=>0, 'username'=>'未知'];
        if (stripos($value,'|') === false){
            return $_value;
        }
        list($id, $username) = explode('|', $value);
        $_value['id'] = $id;
        $_value['username'] = $username;

        return $_value;
    }

    /**
     * 访问器 - 日志描述/注释
     *
     * @return array
     */
    public function getNotesAttribute($value)
    {
        if (empty($value) && (!empty($this->class_name))){
            try {
                $_docParser = new \App\Bin\Unit\DocParserUnit();
                $_reflection = new \ReflectionClass ( $this->class_name );
                $doc = $_reflection->getMethod($this->class_method)->getDocComment();
                $doc = $_docParser->parse($doc);
                $value = $doc['description'];
            }catch (\Exception $e){
                $value = '';
            }
        }

        return $value;
    }
}
