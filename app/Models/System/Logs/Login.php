<?php
namespace App\Models\System\Logs;
use App\Models\BaseModels AS Model;

/**
 * 模型 - 日志 - 登录日志
 */
class Login extends Model
{
    protected $table = 'system_logs_login';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'account',
        'ip_address',
        'browser',
        'system',
        'response',
        'notes',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'updated_at',
    ];

    /**
     * 访问器 - 响应内容
     *
     * @return array
     */
    public function getResponseAttribute($value)
    {
        return json_decode($value, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
    }
}
