<?php
namespace App\Http\Requests;
use App\Http\Requests\SceneValidator;
use Illuminate\Foundation\Http\FormRequest as BaseFormRequest;
use Illuminate\Contracts\Validation\Validator;

class FormRequest extends BaseFormRequest
{
    use SceneValidator;

    protected $rules = [];

    /**
     * 判断用户是否有请求权限
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * 预设验证场景
     *
     * @return array
     */
    public function scene()
    {
        return [];
    }

    /**
     * [覆盖方法] 处理失败的验证
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
//        throw (new \Illuminate\Validation\ValidationException($validator))
//            ->errorBag($this->errorBag)
//            ->redirectTo($this->getRedirectUrl());
        abort(apiFailResponse('MISSING_PARAMETER', $validator->errors()->first(),'ERROR_MISSING_REQUIRED_ARGUMENTS'));
    }
}
