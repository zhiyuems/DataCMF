<?php
namespace App\Http\Requests;
use App\Bin\Contract\Validation\FormValidator;

/**
 * 文件上传表单验证
 *
 * @package App\Http\Requests\Upload
 */
class UploadRequest extends FormRequest implements FormValidator
{
    protected $autoValidate = false;

    /**
     * 预设验证场景
     *
     * @return array
     */
    public function scene()
    {
        return [
            // 文件上传
            'file' => [
                'file',
            ],
            // 图片上传
            'image' => [
                'file',
            ]
        ];
    }

    /**
     * 获取验证错误的自定义属性
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'file' => __($this->scene === 'image'?'term.file_image':'term.file'),
        ];
    }

    /**
     * 应用于请求的验证规则
     *
     * @return array
     */
    public function rules()
    {
        $_rules = [
            // 需要上传的文件
            'file' => ['required'],
        ];
        if ( $this->scene === 'image' ){
            // 验证的文件必须是图片（jpg，jpeg，png，bmp，gif，svg，或 webp）
            $_rules['file'] = array_merge($_rules['file'],['image']);
        }else{
            $_rules['file'] = array_merge($_rules['file'],['file']);
        }
        return $_rules;
    }
}
