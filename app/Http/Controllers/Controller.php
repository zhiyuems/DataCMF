<?php
namespace App\Http\Controllers;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Service\LogsService;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * 日志处理服务
     *
     * @var LogsService $service
     */
    private $_logService;

    /**
     * 构造方法 __construct
     * - 继承类尽可能使用_initialize方法来代替
     *
     * @return void
     */
    public function __construct()
    {
        $this->_logService = new LogsService('system');
        if(method_exists($this,'_initialize'))call_user_func_array([$this,'_initialize'], func_get_args());
    }

    /**
     * 获取资源方法到Ability名称的映射.
     *
     * @return array
     */
    protected function resourceAbilityMap()
    {
        return [
            'index' => 'viewAny',
            'show' => 'view',
            'create' => 'create',
            'store' => 'create',
            'edit' => 'update',
            'update' => 'update',
            'destroy' => 'delete',
        ];
    }

    /**
     * 获取没有模型参数的资源方法列表.
     *
     * @return array
     */
    protected function resourceMethodsWithoutModels()
    {
        return ['index', 'create', 'store'];
    }

    /**
     * Execute an action on the controller.
     * - 扩展前置、后置控制器方法
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function callAction($method, $parameters)
    {
        if (isset($parameters[0]) && $parameters[0] instanceof \Illuminate\Http\Request){
            $this->_logService->setRequest($parameters[0]);
        }else{
            $this->_logService->setRequest(request());
        }

        if(method_exists($this,'beforeAction')) { // 前置方法存在
            $this->beforeAction(...array_values($parameters));
        }

        $return = $this->{$method}(...array_values($parameters));

        if(method_exists($this,'afterAction')) { // 后置方法存在
            $this->afterAction(...array_values($parameters));
        }

        $this->_logService->setModule($this, $method);
        if($return)$this->_logService->setResponse($return);
        $this->_logService->record_sys();

        return $return;
    }
}
