<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
/**
 * 中间件 请求语言切换
 */
class RequestLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $_clientLang = $request->header('accept-language', App::getLocale());
        if (empty($_clientLang)){
            $_clientLang = App::getLocale();
        }else{
            $_clientLang = explode(',', $_clientLang)[0];
        }
        $_clientLang = str_replace('-','_', $_clientLang);

        // 设置语言
        App::setLocale($_clientLang);

        return $next($request);
    }
}
