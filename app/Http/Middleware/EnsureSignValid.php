<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Http\Request;
use App\Service\SignService;
use Illuminate\Support\Arr;

class EnsureSignValid
{
    /**
     * Handle an incoming request.
     * Verify the validity of sign.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        [$param, $sign] = [
            $request->except(['sign']),
            $request->input('sign'),
        ];

        if ( empty($param) )return $next($request);
        if( $this->checkRoute($request) === true )return $next($request);

        if ( empty($sign) ) {
            abort(apiFailResponse('MISSING_SIGNATURE',null,'ERROR_MISSING_REQUIRED_ARGUMENTS'));
        }

        if ( SignService::check($param, $sign) === false ) {
            abort(apiFailResponse('INVALID_SIGNATURE',null,'ERROR_ILLEGAL_PARAMETER'));
        }
        $request->offsetUnset('sign');

        return $next($request);
    }

    /**
     * 路由检查
     *
     * @param Request $request
     *
     * @return bool
     */
    private function checkRoute(Request $request)
    {
        $_ignoreRoute = config('api.sign_routes');
        $_currentRoute = $request->getPathInfo();
        $_isIgnore = Arr::last($_ignoreRoute, function($value, $key) use ($_currentRoute) {
            return $value === $_currentRoute;
        });
        if ( is_null($_isIgnore) === true ){
            return false;
        }
        return true;
    }
}
