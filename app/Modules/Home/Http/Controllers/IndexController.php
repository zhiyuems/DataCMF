<?php
namespace Modules\Home\Http\Controllers;

class IndexController extends Controller
{
    public function index()
    {
        return view('welcome');
    }
}
