<?php
namespace Modules\Admin\Http\Requests;
use App\Http\Requests\FormRequest;
use App\Bin\Contract\Validation\FormValidator;
use App\Bin\Enum\Menu AS MenuEnum;
use App\Rules\Meta AS rulesMeta;
use ToolLibrary\Unit\Tools;

/**
 * 后台管理菜单表单验证
 *
 * @package App\Http\Requests\Admin
 */
class MenuRequest extends FormRequest implements FormValidator
{
    protected $autoValidate = false;

    /**
     * 元数据
     * - 可支持的元数据
     */
    protected $meta = [
        'title',
        'hidden',
        'affix',
        'icon',
        'type',
        'hiddenBreadcrumb',
        'active',
        'color',
        'fullpage',
    ];

    /**
     * 预设验证场景
     *
     * @return array
     */
    public function scene()
    {
        $_rules = [
            'parent_id',
            'ability',
            'active',
            'component',
            'meta',
            'name',
            'path',
            'sort',
        ];

        $this->menuType($_rules);

        return [
            // 管理菜单创建
            'create' => $_rules,
            // 管理菜单信息更新
            'update' => $_rules
        ];
    }

    /**
     * 获取验证错误的自定义属性
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'parent_id' => sprintf('%s %s', __('term.superior') , __('term.permissions.as')),
            'ability' => __('term.role_permissionsid'),
            'password' => __('admin.password'),
        ];
    }

    /**
     * 应用于请求的验证规则
     *
     * @return array
     */
    public function rules()
    {
        $_rules = [
            // [后端] 菜单ID
            'id' => ['filled', 'numeric', 'integer','min:0'],
            // [后端] 上级菜单ID
            'parent_id' => ['filled', 'numeric', 'integer','min:0'],
            // [前端] 权限标识
            'ability' => ['required', 'string'],
            // [前端] 菜单高亮，子节点或详情页需要高亮的上级菜单路由地址
            'active' => ['filled', 'string'],
            // [前端] 视图路径
            'component' => ['filled', 'string'],
            // [前端] Meta元数据
            'meta' => ['required', new rulesMeta($this->meta)],
            // 'exclude_if:meta,array|json',
            // [前端] 视图别名
            'name' => ['required', 'string'],
            // [前端] 路由路径
            'path' => ['required'],
            // 菜单排序
            'sort' => ['filled', 'numeric', 'integer','min:0'],
        ];

        if ( $this->scene === 'create' ){
            $_rules['ability'] = array_merge(['unique:App\Models\Permission\Permission,name'],$_rules['ability']);

        } else if ( $this->scene === 'update' ){
            $_menuId = $this->route('menu');
            // 获取当前需要排除的id,这里的 role 是 路由 {} 中的参数
            $_rules['ability'] = array_merge(['unique:App\Models\Permission\Permission,name,'.$_menuId],$_rules['ability']);
        }

        return $_rules;
    }

    /**
     * 根据菜单类型进行表单验证组合
     *
     * @param array $_rules
     *
     * @return array
     */
    private function menuType(&$_rules)
    {
        $_metaData = $this->offsetExists('meta')?$this->offsetGet('meta'):[];
        if ( !is_array($_metaData) ){
            $_metaData = Tools::isJson($_metaData)?json_decode($_metaData, true):[];
        }
        if (empty($_metaData)){
            abort(apiFailResponse('MISSING_PARAMETER',
                __('validation.required',['attribute'=>'Meta']),
                'ERROR_MISSING_REQUIRED_ARGUMENTS'));
        }
        if (empty($_metaData['type'])){
            abort(apiFailResponse('MISSING_PARAMETER',
                __('validation.required',['attribute'=>__('term.menu_type')]),
                'ERROR_MISSING_REQUIRED_ARGUMENTS'));
        }

        $_menuType = $_metaData['type'];
        if ( $_menuType ===  MenuEnum::TYPE_CATALOGUE ) { // 目录
            unset($_rules[array_search('component', $_rules)]);
        } else if ( $_menuType ===  MenuEnum::TYPE_IFRAME ) { // Iframe
            unset($_rules[array_search('component', $_rules)]);
        } else if ( $_menuType ===  MenuEnum::TYPE_LINK ) { // 外链
            unset($_rules[array_search('name', $_rules)],
                $_rules[array_search('component', $_rules)]);
        }

        return array_values($_rules);
    }
}
