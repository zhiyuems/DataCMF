<?php
namespace Modules\Admin\Http\Requests;
use App\Http\Requests\FormRequest;
use App\Bin\Contract\Validation\FormValidator;
use Illuminate\Validation\Rule;
use App\Bin\Enum\Common AS CommonEnum;

/**
 * 岗位表单验证
 *
 * @package Modules\Admin\Http\Requests
 */
class JobsRequest extends FormRequest implements FormValidator
{
    protected $autoValidate = false;

    /**
     * 预设验证场景
     *
     * @return array
     */
    public function scene()
    {
        $_data = [
            'name',
            'code',
            'sort',
            'status',
        ];
        return [
            // 管理菜单创建
            'create' => $_data,
            // 管理菜单信息更新
            'update' => $_data
        ];
    }

    /**
     * 获取验证错误的自定义属性
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => __('term.jobs'),
            'code' => __('term.jobs_code'),
        ];
    }

    /**
     * 应用于请求的验证规则
     *
     * @return array
     */
    public function rules()
    {
        $_rules = [
            // 岗位名称
            'name' => ['required'],
            // 岗位编码
            'code' => ['filled'],
            // 显示排序
            'sort' => ['filled', 'numeric', 'integer','min:0'],
            // 状态
            'status' => ['filled', Rule::in([CommonEnum::STATUS_OPEN,CommonEnum::STATUS_CLOSE])],
        ];
        if ( $this->scene === 'create' ){
            $_rules['name'] = array_merge(['unique:Modules\Admin\Models\System\Jobs,name'],$_rules['name']);

        } else if ( $this->scene === 'update' ){
            $_jobsId = $this->route('job');
            // 获取当前需要排除的id,这里的 role 是 路由 {} 中的参数
            $_rules['name'] = array_merge(['unique:Modules\Admin\Models\System\Jobs,name,'.$_jobsId],$_rules['name']);
        }
        return $_rules;
    }
}
