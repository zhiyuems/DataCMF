<?php
namespace Modules\Admin\Http\Requests;
use App\Http\Requests\FormRequest;
use App\Bin\Contract\Validation\FormValidator;
use Illuminate\Validation\Rule;
use App\Bin\Enum\Common AS CommonEnum;

/**
 * 部门表单验证
 *
 * @package Modules\Admin\Http\Requests
 */
class DeptRequest extends FormRequest implements FormValidator
{
    protected $autoValidate = false;

    /**
     * 预设验证场景
     *
     * @return array
     */
    public function scene()
    {
        $_data = [
            'parent_id',
            'name',
            'leader',
            'phone',
            'email',
            'sort',
            'status',
        ];
        return [
            // 管理菜单创建
            'create' => $_data,
            // 管理菜单信息更新
            'update' => $_data
        ];
    }

    /**
     * 获取验证错误的自定义属性
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'parent_id' => sprintf('%s %s', __('term.superior') , __('term.dept')),
            'name' => __('term.dept'),
        ];
    }

    /**
     * 应用于请求的验证规则
     *
     * @return array
     */
    public function rules()
    {
        $_rules = [
            // 上级部门ID
            'parent_id' => ['filled', 'numeric', 'integer','min:0'],
            // 部门名称
            'name' => ['required'],
            // 部门负责人
            'leader' => ['filled'],
            // 联系电话
            'phone' => ['filled'],
            // 电子邮箱
            'email' => ['filled', 'email:rfc,dns'],
            // 菜单排序
            'sort' => ['filled', 'numeric', 'integer','min:0'],
            // 状态
            'status' => ['filled', Rule::in([CommonEnum::STATUS_OPEN,CommonEnum::STATUS_CLOSE])],
        ];
        if ( $this->scene === 'create' ){
            $_parentId = intval($this->get('parent_id', 0));

            $_rules['name'] = array_merge([
                Rule::unique(\Modules\Admin\Models\System\Dept::class)->where(function($query) use ($_parentId){
                    return $query->where('parent_id', $_parentId);
                })
            ],$_rules['name']);

        } else if ( $this->scene === 'update' ){
            $_deptId = $this->route('dept');
            // 获取当前需要排除的id,这里的 role 是 路由 {} 中的参数
            $_rules['name'] = array_merge(['unique:Modules\Admin\Models\System\Dept,name,'.$_deptId],$_rules['name']);
        }
        return $_rules;
    }
}
