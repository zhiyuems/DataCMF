<?php
namespace Modules\Admin\Http\Requests;
use App\Http\Requests\FormRequest;
use App\Bin\Contract\Validation\FormValidator;
use App\Bin\Enum\Common AS CommonEnum;
use Illuminate\Validation\Rule;

/**
 * 后台管理系统配置表单验证
 *
 * @package Modules\Admin\Http\Requests
 */
class SystemSettingRequest extends FormRequest implements FormValidator
{
    protected $autoValidate = true;

    /**
     * 获取验证错误的自定义属性
     *
     * @return array
     */
    public function attributes()
    {
        return [
        ];
    }

    /**
     * 应用于请求的验证规则
     *
     * @return array
     */
    public function rules()
    {
        $_rules = [
            // 展示名称
            'variable_label' => ['required', 'string'],
            // 变量名称
            'variable_name' => ['required', 'string'],
            // 变量值
            'variable_value' => ['string'],
            // 变量提示
            'variable_tip' => ['string'],
            // 变量备注
            'variable_describe' => ['string'],
            // 是否是系统变量(Y是 N否)
            'is_systemvar' => ['filled', Rule::in([CommonEnum::STATUS_OPEN,CommonEnum::STATUS_CLOSE])],
            // 分组
            'group' => ['required', 'string'],
            // 子分组
            'group_son' => ['filled', 'string'],
        ];

        return $_rules;
    }

}
