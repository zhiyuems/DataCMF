<?php
namespace Modules\Admin\Http\Requests;
use App\Http\Requests\FormRequest;
use App\Bin\Contract\Validation\FormValidator;
use Illuminate\Validation\Rule;
use App\Bin\Enum\Common AS CommonEnum;

/**
 * 字典类型表单验证
 *
 * @package Modules\Admin\Http\Requests
 */
class DictTypeRequest extends FormRequest implements FormValidator
{
    protected $autoValidate = false;

    /**
     * 预设验证场景
     *
     * @return array
     */
    public function scene()
    {
        $_data = [
            'label',
            'code',
            'status',
            'remark',
        ];
        return [
            // 管理菜单创建
            'create' => $_data,
            // 管理菜单信息更新
            'update' => $_data,
            'destroy' => ['type']
        ];
    }

    /**
     * 获取验证错误的自定义属性
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'parent_id' => sprintf('%s %s', __('term.superior') , __('term.dict')),
            'name' => __('term.dept'),
        ];
    }

    /**
     * 应用于请求的验证规则
     *
     * @return array
     */
    public function rules()
    {
        $_rules = [
            // 字典名称(展示名称)
            'label' => ['string'],
            // 字典编码(代码)
            'code' => ['string'],
            // 状态
            'status' => ['filled', Rule::in([CommonEnum::STATUS_OPEN,CommonEnum::STATUS_CLOSE])],
            // 备注
            'remark' => ['nullable','string'],
        ];
        if ( $this->scene === 'create' ){
            $_rules['label'] = array_merge(['required'],$_rules['label']);
            $_rules['code'] = array_merge(['required', Rule::unique(\Modules\Admin\Models\System\DictType::class,'dict_key')],$_rules['code']);

        } else if ( $this->scene === 'update' ){
            unset($_rules['code']);
        }
        return $_rules;
    }
}
