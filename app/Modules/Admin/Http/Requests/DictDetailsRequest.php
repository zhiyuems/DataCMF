<?php
namespace Modules\Admin\Http\Requests;
use App\Http\Requests\FormRequest;
use App\Bin\Contract\Validation\FormValidator;
use Illuminate\Validation\Rule;
use App\Bin\Enum\Common AS CommonEnum;

/**
 * 字典明细表单验证
 *
 * @package Modules\Admin\Http\Requests
 */
class DictDetailsRequest extends FormRequest implements FormValidator
{
    protected $autoValidate = false;

    /**
     * 预设验证场景
     *
     * @return array
     */
    public function scene()
    {
        $_data = [
            'type',
            'label',
            'code',
            'value',
            'sort',
            'status',
            'remark',
        ];
        return [
            // 管理菜单创建
            'create' => $_data,
            // 管理菜单信息更新
            'update' => $_data,
            'destroy' => ['type']
        ];
    }

    /**
     * 获取验证错误的自定义属性
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'parent_id' => sprintf('%s %s', __('term.superior') , __('term.dict')),
            'name' => __('term.dept'),
        ];
    }

    /**
     * 应用于请求的验证规则
     *
     * @return array
     */
    public function rules()
    {
        $_rules = [
            // 归属字典类型
            'type' => ['required','string', Rule::exists(\Modules\Admin\Models\System\DictType::class,'dict_key')],
            // 字典名称(展示名称)
            'label' => ['string'],
            // 字典编码(代码)
            'code' => ['string'],
            // 字典键值
            'value' => ['filled','string'],
            // 显示排序
            'sort' => ['filled', 'numeric', 'integer','min:0'],
            // 状态
            'status' => ['filled', Rule::in([CommonEnum::STATUS_OPEN,CommonEnum::STATUS_CLOSE])],
            // 备注
            'remark' => ['nullable','string'],
        ];
        if ( $this->scene === 'create' ){
            $_rules['label'] = array_merge(['required'],$_rules['label']);
            $_rules['code'] = array_merge(['required'],$_rules['code']);
            $_rules['value'] = array_merge(['required'],$_rules['value']);

            $_dictType = $this->get('type');
            $_rules['code'] = array_merge([
                Rule::unique(\Modules\Admin\Models\System\DictDetails::class,'dict_code')
                    ->where(function($query) use ($_dictType){
                    return $query->where('dict_type', $_dictType);
                })
            ],$_rules['code']);

        } else if ( $this->scene === 'update' ){
            unset($_rules['code']);
        }
        return $_rules;
    }
}
