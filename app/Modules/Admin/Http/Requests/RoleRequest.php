<?php
namespace Modules\Admin\Http\Requests;
use App\Http\Requests\FormRequest;
use App\Bin\Contract\Validation\FormValidator;

/**
 * 后台管理角色表单验证
 *
 * @package App\Http\Requests\Admin
 */
class RoleRequest extends FormRequest implements FormValidator
{
    protected $autoValidate = false;

    /**
     * 预设验证场景
     *
     * @return array
     */
    public function scene()
    {
        return [
            // 管理角色创建
            'create' => [
                'parent_id',
                'name',
                'alias',
                'sort',
            ],
            // 管理角色信息更新
            'update' => [
                'parent_id',
                'name',
                'alias',
                'sort',
                'permissions',
            ]
        ];
    }

    /**
     * 获取验证错误的自定义属性
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'parent_id' => sprintf('%s %s', __('term.superior') , __('term.role')),
            'name' => __('term.role_permissionsid'),
            'password' => __('admin.password'),
        ];
    }

    /**
     * 应用于请求的验证规则
     *
     * @return array
     */
    public function rules()
    {
        $_rules = [
            // 上级角色ID
            'parent_id' => ['filled', 'numeric', 'integer','min:0'],
            // 角色权限标识
            'name' => ['alpha'],
            // 角色别名(表单展示)
            'alias' => [],
            // 角色排序
            'sort' => ['filled', 'numeric', 'integer','min:0'],
            // 角色关联权限列表
            'permissions' => ['filled', 'json'],
        ];
        if ( $this->scene === 'create' ){
            $_rules['name'] = array_merge(['required','unique:App\Models\Permission\Roles,name'],$_rules['name']);
            $_rules['alias'] = array_merge(['required'],$_rules['alias']);

        } else if ( $this->scene === 'update' ){
            $_roleId = $this->route('role');
            // 获取当前需要排除的id,这里的 role 是 路由 {} 中的参数
            $_rules['name'] = array_merge(['filled','unique:App\Models\Permission\Roles,name,'.$_roleId],$_rules['name']);
            $_rules['alias'] = array_merge(['filled'],$_rules['alias']);
        }
        return $_rules;
    }
}
