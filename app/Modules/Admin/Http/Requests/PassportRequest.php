<?php
namespace Modules\Admin\Http\Requests;
use App\Http\Requests\FormRequest;
use App\Bin\Contract\Validation\FormValidator;

/**
 * 后台管理员表单验证
 *
 * @package App\Http\Requests\Admin
 */
class PassportRequest extends FormRequest implements FormValidator
{
    protected $autoValidate = false;

    /**
     * 预设验证场景
     *
     * @return array
     */
    public function scene()
    {
        return [
            // 管理员登录
            'login' => [
                'account',
                'password',
            ],
            // 管理员资料创建
            'create' => [
                'avatar',
                'username',
                'account',
                'password',
                'role',
            ],
            // 管理员资料更新
            'update' => [
                'avatar',
                'username',
                'account',
                'password',
                'role'
            ]
        ];
    }

    /**
     * 获取验证错误的自定义属性
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'username' => __('term.username'),
            'account' => __('term.account'),
            'password' => __('term.password'),
            'role' => __('term.role'),
        ];
    }

    /**
     * 获取已定义验证规则的错误消息
     *
     * @return array
     */
    public function messages()
    {
        return [
//            'account.required' => ':attribute必填',
//            'password.required'  => ':attribute必填',
        ];
    }

    /**
     * 应用于请求的验证规则
     *
     * @return array
     */
    public function rules()
    {
        $_rules = [
            'id' => [],
            'avatar' => ['filled'],
            'role' => ['filled', 'json'],
            'username' => [],
            'account' => ['alpha_dash'],
            'password' => []
        ];
        if ( $this->scene === 'create' ){
            $_rules['avatar'] = array_merge(['url'],$_rules['avatar']);
            $_rules['username'] = array_merge(['required'],$_rules['username']);
            $_rules['account'] = array_merge(['required'],$_rules['account']);
            $_rules['password'] = array_merge(['required','confirmed'],$_rules['password']);
            $_rules['role'] = array_merge(['required'],$_rules['role']);

        } else if ( $this->scene === 'update' ){
            $_rules['id'] = array_merge(['required'],$_rules['id']);
            $_rules['username'] = array_merge(['filled'],$_rules['username']);
            $_rules['account'] = array_merge(['filled'],$_rules['account']);
            $_rules['password'] = array_merge(['filled'],$_rules['password']);

        } else if ( $this->scene === 'login' ) {
            $_rules['account'] = array_merge(['required'],$_rules['account']);
            $_rules['password'] = array_merge(['required'],$_rules['password']);
        }
        return $_rules;
    }
}
