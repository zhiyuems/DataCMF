<?php
namespace Modules\Admin\Http\Controllers\System;
use Modules\Admin\Http\Controllers\Controller;
use Modules\Admin\Service\RoleService;
use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\RoleRequest;

/**
 * 后台角色权限控制器
 */
class RolePermissionController extends Controller
{
    /**
     * 角色处理服务
     *
     * @var \Admin\Service\RoleService $service
     */
    private $service;

    protected $abilityName = 'system.role';

    /**
     * 语言包 - 术语 - Key
     */
    private $langTermKey = 'term.role';

    public function _initialize ()
    {
        $this->service = new RoleService();
        $this->service->setGuardName(self::GUARD_NAME);
        $this->service->setGuardModuleId(self::GUARD_MODULE_ID);
    }

    /**
     * 获取指定角色权限配置信息（展示编辑页面）
     * - GET /{id}/edit
     *
     * @powerName update
     * @param Request $request
     * @param int     $id       角色ID
     *
     */
    public function edit(Request $request, $id)
    {
        $this->authorize($this->abilityName.'.update');

        $this->service->setRequest($request);

        return apiSuccessResponse([]);
    }

    /**
     * 更新指定角色权限配置信息（保存编辑数据）
     * - PUT/PATHCH /{id}
     *
     * @powerName update
     * @param Request $request
     * @param int     $id       角色ID
     *
     */
    public function update(RoleRequest $request,$id)
    {
        $this->authorize($this->abilityName.'.update');

        $request->validate('update');
        $_param = $request->validated();

        return apiSuccessResponse([],
            __('message.data.update.success',['attribute'=>__($this->langTermKey)]));
    }

}
