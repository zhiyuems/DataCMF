<?php
namespace Modules\Admin\Http\Controllers\System;
use App\Bin\Enum\Menu as MenuEnum;
use App\Bin\Unit\MetaUnit;
use Modules\Admin\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\MenuRequest;
use Modules\Admin\Service\MenuService;
use ToolLibrary\Unit\DataType;
use ToolLibrary\Unit\Tools;
use ToolLibrary\Unit\Arr;

/**
 * 后台菜单控制器
 */
class MenuController extends Controller
{
    /**
     * 菜单处理服务
     *
     * @var \Modules\Admin\Service\MenuService $service
     */
    private $service;

    protected $abilityName = 'system.menu';

    /**
     * 语言包 - 术语 - Key
     */
    private $langTermKey = 'term.role';

    public function _initialize ()
    {
        $this->service = new MenuService();
        $this->service->setGuardName(self::GUARD_NAME);
        $this->service->setGuardModuleId(self::GUARD_MODULE_ID);
        $this->authorizeResource($this->service->getModel());
    }

    /**
     * 获取管理后台全部菜单列表
     * - GET /
     *
     * @powerName viewAny
     * @param Request $request
     */
    public function index(Request $request)
    {
        $this->service->setRequest($request);

        // 获取菜单列表
        $_list = $this->service->allList();

        return apiSuccessResponse($_list);
    }

    /**
     * 获取当前管理员后台菜单
     *
     * @param Request $request
     */
    public function toMyMenu(Request $request)
    {
        $this->authorize(sprintf('%s.%s', $this->abilityName, 'myinfo'));

        $this->service->setRequest($request);
        return apiSuccessResponse($this->service->myList());
    }

    /**
     * 创建管理菜单信息（创建数据提交）
     * - POST /
     *
     * @powerName create
     * @param MenuRequest $request
     *
     */
    public function store(MenuRequest $request)
    {
        $request->validate('create');
        $_param = $request->validated();

        // 格式化参数
        $this->_formatParam($_param);

        if ( $this->service->create($_param) ) {
            return apiSuccessResponse([],
                __('message.data.create.success',['attribute'=>__($this->langTermKey)]));
        }
        return apiFailResponse('DATA_CREATION_FAILED',
            __('message.data.create.fail',['attribute'=>__($this->langTermKey)]));
    }

    /**
     * 更新指定菜单信息（保存编辑数据）
     * - PUT/PATHCH /{id}
     *
     * @powerName update
     * @param MenuRequest $request
     * @param int     $id       菜单ID
     *
     */
    public function update(MenuRequest $request,$id)
    {
        $this->service->setRequest($request);

        $request->validate('update');
        $_param = $request->validated();

        // 格式化参数
        $this->_formatParam($_param);

        // 获取指定菜单信息
        $_info = $this->service->single($id);
        if (!$_info){
            return apiFailResponse('DATA_UPDATE_FAILED',
                __('message.data.not_exist'));
        }
        $_info = $_info->toArray();
        unset($_info['id']);

        // 比对提取需要更改的数据信息
        $_upInfo = Arr::diff($_param, $_info);

        if (empty($_upInfo) && empty($_role)){
            return apiFailResponse('INVALID_PARAMETER',
                __('message.data.no_updatable',['attribute'=>__($this->langTermKey)]));
        }

        // 更新更改数据
        if ( $this->service->update($_upInfo, $id) )
        {
            return apiSuccessResponse([],
                __('message.data.update.success',['attribute'=>__($this->langTermKey)]));
        }
        return apiFailResponse('DATA_UPDATE_FAILED',
            __('message.data.update.fail',['attribute'=>__($this->langTermKey)]));
    }

    /**
     * 删除指定菜单信息
     * - DELETE /{id}
     *
     * @powerName delete
     * @param Request $request
     * @param int     $id       菜单ID
     */
    public function destroy($id)
    {
        // 判断菜单是否存在
        if ( !$this->service->isExists($id) ) {
            return apiFailResponse('INVALID_PARAMETER',
                __('message.data.not_exist'));
        }

        // 执行删除
        if ( $this->service->delete($id) )
        {
            return apiSuccessResponse([], __('message.data.delete.success',['attribute'=>__($this->langTermKey)]));
        }
        return apiFailResponse('DATA_DELETE_FAILED',
            __('message.data.delete.fail',['attribute'=>__($this->langTermKey)]));
    }

    /**
     * 批量删除指定菜单信息
     * - DELETE /batch
     *
     * @powerName delete.batch
     * @param Request $request
     */
    public function destroyBatch(Request $request)
    {
        $this->authorize(sprintf('%s.%s', $this->abilityName, 'delete.batch'));

        $_ids = $request->input('ids',[]);
        if (!is_array($_ids)){
            $_ids = Tools::isJson($_ids)?json_decode($_ids, true):[];
        }

        // 判断菜单是否存在
        if ( !$this->service->isExists($_ids) ) {
            return apiFailResponse('INVALID_PARAMETER',
                __('message.data.not_exist'));
        }

        // 执行删除
        if ( $this->service->delete($_ids) )
        {
            return apiSuccessResponse([], __('message.data.delete.success',['attribute'=>__($this->langTermKey)]));
        }
        return apiFailResponse('DATA_DELETE_FAILED',
            __('message.data.delete.fail',['attribute'=>__($this->langTermKey)]));
    }

    /**
     * 格式化外部传参
     *
     * @param array $param
     *
     * @return array
     */
    private function _formatParam(array &$param)
    {
        $param['parent_id'] = DataType::numberIsset($param, 'parent_id')?$param['parent_id']:0;
        $param['sort'] = DataType::numberIsset($param, 'sort')?$param['sort']:50;

        // Meta元数据格式化
        if ( !is_array($param['meta']) ){
            $param['meta'] = Tools::isJson($param['meta'])?json_decode($param['meta'], true):[];
        }
        $param = MetaUnit::flat($param);

        // 变量名称格式化
        if ( in_array($param['meta_type'], [
            MenuEnum::TYPE_CATALOGUE, // 目录
            MenuEnum::TYPE_MENU, // 菜单
            MenuEnum::TYPE_BUTTON, // 按钮
        ]) ){
            $param['view_name'] = $param['name'];
        } else if ( $param['meta_type'] ===  MenuEnum::TYPE_IFRAME ) { // Iframe
            $param['view_name'] = $param['name'];
        } else if ( $param['meta_type'] ===  MenuEnum::TYPE_LINK ) { // 外链
            $param['view_name'] = $param['ability'];
        }

        $param['name'] = $param['ability'];   // 服务端权限标识
        unset($param['ability']);

        return $param;
    }
}
