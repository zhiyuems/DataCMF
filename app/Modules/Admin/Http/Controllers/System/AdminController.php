<?php
namespace Modules\Admin\Http\Controllers\System;
use Modules\Admin\Http\Controllers\Controller;
use Modules\Admin\Http\Requests\PassportRequest;
use Modules\Admin\Service\UserService;
use Illuminate\Http\Request;
use ToolLibrary\Unit\Arr;
use ToolLibrary\Unit\Tools;

/**
 * 超管后台管理员控制器
 */
class AdminController extends Controller
{
    /**
     * 管理员处理服务
     *
     * @var \Admin\Service\UserService $service
     */
    private $service;

    protected $abilityName = 'system.admin';

    /**
     * 语言包 - 术语 - Key
     */
    private $langTermKey = 'term.administrators';

    public function _initialize ()
    {
        $this->service = new UserService();
        $this->service->setGuardName(self::GUARD_NAME);
        $this->service->setGuardModuleId(self::GUARD_MODULE_ID);
        $this->authorizeResource($this->service->getModel());
    }

    /**
     * 获取全部管理员列表
     * - GET /
     *
     * @powerName viewAny
     * @param Request $request
     */
    public function index(Request $request)
    {
        $this->service->setRequest($request);

        // 获取管理员列表
        $_list = $this->service->allList()->toArray();

        return apiSuccessResponse($_list);
    }

    /**
     * 创建管理员信息（创建页展示）
     * - GET /create
     *
     * @powerName create
     *
     */
    public function create()
    {

    }

    /**
     * 创建管理员信息（创建数据提交）
     * - POST /
     *
     * @powerName create
     * @param PassportRequest $request
     *
     */
    public function store(PassportRequest $request)
    {
        $request->validate('create');
        $_param = $request->validated();

        if ( $this->service->create($_param) ) {
            return apiSuccessResponse([],
                __('message.data.create.success',['attribute'=>__($this->langTermKey)]));
        }
        return apiFailResponse('DATA_CREATION_FAILED',
            __('message.data.create.fail',['attribute'=>__($this->langTermKey)]));
    }

    /**
     * 获取指定管理管理员信息（展示编辑页面）
     * - GET /{id}/edit
     *
     * @powerName update
     * @param Request $request
     * @param int     $id       管理员ID
     *
     */
    public function edit($id)
    {
        // 获取指定管理员信息
        $_roleInfo = $this->service->single($id);
        $_roleInfo = $_roleInfo->toArray();

        return apiSuccessResponse($_roleInfo);
    }

    /**
     * 更新指定管理员信息（保存编辑数据）
     * - PUT/PATHCH /{id}
     *
     * @powerName update
     * @param Request $request
     * @param int     $id       管理员ID
     *
     */
    public function update(PassportRequest $request,$id)
    {
        $this->service->setRequest($request);

        $request->validate('update');
        $_param = $request->validated();

        if (!empty($_param['role'])) {
            if (is_array($_param['role'])){
                $_role = $_param['role'];
            }else if (Tools::isJson($_param['role'])){
                $_role = json_decode($_param['role'], true);
            }else{
                return apiFailResponse('INVALID_PARAMETER',
                    __('message.data.update.invalid_parameter',['attribute'=>__('term.user.role')]));
            }
        }
        unset($_param['role']);

        // 获取指定管理员信息
        $_info = $this->service->single($id);
        if (!$_info){
            return apiFailResponse('DATA_UPDATE_FAILED',
                __('message.data.not_exist'));
        }
        $_info = $_info->toArray();
        unset($_info['id']);

        // 比对提取需要更改的数据信息
        $_upInfo = Arr::diff($_param, $_info);
        if (empty($_upInfo) && empty($_role)){
            return apiFailResponse('INVALID_PARAMETER',
                __('message.data.no_updatable',['attribute'=>__($this->langTermKey)]));
        }

        if ( !empty($_role) ) {
            // 更新角色
            $_upInfo['role'] = $_role;
        }

        // 更新更改数据
        if ( $this->service->update($_upInfo, $id) )
        {
            return apiSuccessResponse([],
                __('message.data.update.success',['attribute'=>__($this->langTermKey)]));
        }
        return apiFailResponse('DATA_UPDATE_FAILED',
            __('message.data.update.fail',['attribute'=>__($this->langTermKey)]));
    }

    /**
     * 删除指定管理管理员信息
     * - DELETE /{id}
     *
     * @powerName delete
     * @param Request $request
     * @param int     $id       管理员ID
     *
     */
    public function destroy(Request $request, $id)
    {
        // 判断管理员是否存在
        if ( !$this->service->isExists($id) ) {
            return apiFailResponse('INVALID_PARAMETER',
                __('message.data.not_exist'));
        }

        // 执行删除
        if ( $this->service->delete($id) )
        {
            return apiSuccessResponse([], __('message.data.delete.success',['attribute'=>__($this->langTermKey)]));
        }
        return apiFailResponse('DATA_DELETE_FAILED',
            __('message.data.delete.fail',['attribute'=>__($this->langTermKey)]));
    }
}
