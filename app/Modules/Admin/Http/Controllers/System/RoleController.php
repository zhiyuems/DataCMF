<?php
namespace Modules\Admin\Http\Controllers\System;
use Modules\Admin\Http\Controllers\Controller;
use Modules\Admin\Service\RoleService;
use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\RoleRequest;
use ToolLibrary\Unit\Arr;
use ToolLibrary\Unit\DataType;

/**
 * 后台角色控制器
 */
class RoleController extends Controller
{
    /**
     * 角色处理服务
     *
     * @var \Admin\Service\RoleService $service
     */
    private $service;

    protected $abilityName = 'system.role';

    /**
     * 语言包 - 术语 - Key
     */
    private $langTermKey = 'term.role';

    public function _initialize ()
    {
        $this->service = new RoleService();
        $this->service->setGuardName(self::GUARD_NAME);
        $this->service->setGuardModuleId(self::GUARD_MODULE_ID);
        $this->authorizeResource($this->service->getModel());
    }

    /**
     * 获取管理后台全部角色列表
     * - GET /
     *
     * @powerName viewAny
     * @param Request $request
     */
    public function index(Request $request)
    {
        $this->service->setRequest($request);

        // 获取角色列表
        $_list = $this->service->allList();

        return apiSuccessResponse($_list);
    }

    /**
     * 创建管理角色信息（创建页展示）
     * - GET /create
     *
     * @powerName create
     *
     */
    public function create()
    {

    }

    /**
     * 创建管理角色信息（创建数据提交）
     * - POST /
     *
     * @powerName create
     * @param RoleRequest $request
     *
     */
    public function store(RoleRequest $request)
    {
        $request->validate('create');
        $_param = $request->validated();

        $_param['parent_id'] = DataType::numberIsset($_param, 'parent_id')?$_param['parent_id']:0;
        $_param['sort'] = DataType::numberIsset($_param, 'sort')?$_param['sort']:50;

        if ( $this->service->create($_param) ) {
            return apiSuccessResponse([],
                __('message.data.create.success',['attribute'=>__($this->langTermKey)]));
        }
        return apiFailResponse('DATA_CREATION_FAILED',
            __('message.data.create.fail',['attribute'=>__($this->langTermKey)]));
    }

    /**
     * 获取指定管理角色信息（展示编辑页面）
     * - GET /{id}/edit
     *
     * @powerName update
     * @param Request $request
     * @param int     $id       角色ID
     *
     */
    public function edit(Request $request, $id)
    {
        $this->service->setRequest($request);

        // 获取指定角色信息
        $_roleInfo = $this->service->single($id)->toArray();

        return apiSuccessResponse($_roleInfo);
    }

    /**
     * 更新指定管理角色信息（保存编辑数据）
     * - PUT/PATHCH /{id}
     *
     * @powerName update
     * @param Request $request
     * @param int     $id       角色ID
     *
     */
    public function update(RoleRequest $request,$id)
    {
        $request->validate('update');
        $_param = $request->validated();

        if (!empty($_param['permissions'])) {
            $_permissions = json_decode($_param['permissions'], true);
        }
        unset($_param['permissions']);

        // 获取指定角色信息
        $_info = $this->service->single($id);
        if (!$_info){
            return apiFailResponse('DATA_UPDATE_FAILED',
                __('message.data.not_exist'));
        }
        $_info = $_info->toArray();
        unset($_info['id']);

        // 比对提取需要更改的数据信息
        $_upInfo = Arr::diff($_param, $_info);
        if (empty($_upInfo) && empty($_permissions) ){
            return apiFailResponse('INVALID_PARAMETER',
                __('message.data.no_updatable',['attribute'=>__($this->langTermKey)]));
        }

        if ( !empty($_permissions) ) {
            // 更新角色权限
            $_upInfo['permissions'] = $_permissions;
        }

        // 更新更改数据
        if ( $this->service->update($_upInfo, $id) )
        {
            return apiSuccessResponse([],
                __('message.data.update.success',['attribute'=>__($this->langTermKey)]));
        }
        return apiFailResponse('DATA_UPDATE_FAILED',
            __('message.data.update.fail',['attribute'=>__($this->langTermKey)]));
    }

    /**
     * 删除指定管理角色信息
     * - DELETE /{id}
     *
     * @powerName delete
     * @param Request $request
     * @param int     $id       角色ID
     *
     */
    public function destroy(Request $request, $id)
    {
        // 判断角色是否存在
        if ( !$this->service->isExists($id) ) {
            return apiFailResponse('INVALID_PARAMETER', __('message.data.not_exist'));
        }

        // 判断是否允许删除角色
        $_error = $this->service->isDeleteAllowed($id);
        if ( $_error['allow'] === false ){
            return apiFailResponse('INVALID_PARAMETER', $_error['msg']);
        }

        // 执行删除
        if ( $this->service->delete($id) )
        {
            return apiSuccessResponse([],
                __('message.data.delete.success',['attribute'=>__($this->langTermKey)]));
        }
        return apiFailResponse('DATA_DELETE_FAILED',
            __('message.data.delete.fail',['attribute'=>__($this->langTermKey)]));
    }
}
