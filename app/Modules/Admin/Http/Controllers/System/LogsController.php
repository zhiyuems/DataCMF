<?php
namespace Modules\Admin\Http\Controllers\System;
use Modules\Admin\Http\Controllers\Controller;
use Modules\Admin\Service\LogsService;
use App\Bin\Enum\LogsType AS LogsTypeEnum;
use App\Bin\Enum\LogsLevel AS LogsLevelEnum;
use Illuminate\Http\Request;

/**
 * 日志控制器
 */
class LogsController extends Controller
{
    /**
     * 日志处理服务
     *
     * @var LogsService $service
     */
    private $service;

    protected $abilityName = 'system.log';

    /**
     * 语言包 - 术语 - Key
     */
    private $langTermKey = 'term.log';

    public function _initialize ()
    {
        $this->service = new LogsService();
        $this->service->setGuardName(self::GUARD_NAME);
        $this->service->setGuardModuleId(self::GUARD_MODULE_ID);
    }

    /**
     * 获取全部日志类型列表
     * - GET /logstype
     *
     * @powerName viewAny
     * @param Request $request
     */
    public function toLogsType()
    {
        $this->authorize(sprintf('%s.%s', $this->abilityName, 'viewAny'));

        $_arrChangeKey = function (&$data, $oldName='name', $newName='label'){
            $_value = $data[$oldName];
            unset($data[$oldName]);
            $data[$newName] = $_value;
        };

        $_list = array_values(LogsTypeEnum::data());
        foreach ($_list AS $key => $item){
            $_arrChangeKey($item);
            if ($item['value'] === LogsTypeEnum::OPERATION){
                $item['type'] = 'system';
                $_children = array_values(LogsLevelEnum::data());
                array_walk($_children, function($item, $key) use (&$_children, $_arrChangeKey){
                    $_arrChangeKey($item);
                    $item['type'] = 'system';
                    $_children[$key] = $item;
                });
                $item['children'] = $_children;
            }else if($item['value'] === LogsTypeEnum::LOGIN){
                $item['type'] = 'login';
            }else if($item['value'] === LogsTypeEnum::SQL){
                $item['type'] = 'sql';
            }
            $_list[$key] = $item;
        }


        return apiSuccessResponse($_list);
    }

    /**
     * 获取日志记录列表
     * - GET /operation
     *
     * @powerName viewAny
     * @param Request $request
     */
    public function toRecordList(Request $request)
    {
        $this->service->setRequest($request);

        $_list = $this->service->allList();

        return apiSuccessResponse($_list);
    }

}
