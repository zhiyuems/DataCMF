<?php
namespace Modules\Admin\Http\Controllers\System;
use Modules\Admin\Http\Controllers\Controller;
use Modules\Admin\Service\SettingService;
use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\SystemSettingRequest AS SysSettingRequest;
use ToolLibrary\Unit\Arr;
use App\Bin\Enum\Common AS CommonEnum;
use ToolLibrary\Unit\Tools;

/**
 * 后台系统配置控制器
 */
class SettingController extends Controller
{
    /**
     * 系统配置处理服务
     *
     * @var \Modules\Admin\Service\SettingService $service
     */
    private $service;

    protected $abilityName = 'system.setting';

    /**
     * 语言包 - 术语 - Key
     */
    private $langTermKey = 'term.sys_setting';

    public function _initialize ()
    {
        $this->service = new SettingService();
        $this->service->setGuardName(self::GUARD_NAME);
        $this->service->setGuardModuleId(self::GUARD_MODULE_ID);
    }

    /**
     * 获取系统配置信息
     * - GET /
     *
     * @powerName viewAny
     */
    public function toAllSetting(Request $request)
    {
        $this->authorize(sprintf('%s.%s', $this->abilityName, 'viewAny'));

        $this->service->setRequest($request);

        $_allSetting = $this->service->getAllSetting();

        return apiSuccessResponse($_allSetting);
    }

    /**
     * 更新或创建系统配置信息
     * - PUT /
     *
     * @powerName update
     */
    public function toCreateOrUpdate(SysSettingRequest $request)
    {
        $this->authorize(sprintf('%s.%s', $this->abilityName, 'update'));

        $request->validate();
        $_param = $request->validated();

        $_createOrUpdate = 'create';
        if ( $this->service->isExists($_param['variable_name']) ){
            $_createOrUpdate = 'update';

            // 获取指定系统变量信息
            $_info = $this->service->single($_param['variable_name']);
            if ($_info['is_systemvar'] === CommonEnum::STATUS_OPEN){
                if ( isset($_param['is_systemvar']) && (
                        $_info['is_systemvar'] !== $_param['is_systemvar']
                        || $_info['variable_name'] !== $_param['variable_name']
                    ) ){
                    return apiFailResponse('INVALID_PARAMETER',
                        __('message.data.prohibit_update.prohibit',['attribute'=>__($this->langTermKey)]));
                }
                $_param['variable_name'] = $_info['variable_name'];
                $_param['is_systemvar'] = CommonEnum::STATUS_OPEN;
            }

            // 比对提取需要更改的数据信息
            $_upInfo = Arr::diff($_param, $_info);
            if (empty($_upInfo) && empty($_role)){
                return apiFailResponse('INVALID_PARAMETER',
                    __('message.data.no_updatable',['attribute'=>__($this->langTermKey)]));
            }
            $_isCreateOrUpdate = $this->service->update($_param, $_param['variable_name'], 'variable_name');
        }else{
            $_isCreateOrUpdate = $this->service->create($_param);
        }

        if ( $_isCreateOrUpdate ) {
            return apiSuccessResponse([],
                __('message.data.'.$_createOrUpdate.'.success',['attribute'=>__($this->langTermKey)]));
        }
        return apiFailResponse('DATA_CREATION_FAILED',
            __('message.data.'.$_createOrUpdate.'.fail',['attribute'=>__($this->langTermKey)]));
    }

    /**
     * 批量更新系统配置信息
     * - PUT /batch
     *
     * @powerName update
     */
    public function toBatchUpdate(Request $request)
    {
        $this->authorize(sprintf('%s.%s', $this->abilityName, 'update'));

        $_upList = $request->input('list');
        if ( !is_array($_upList) ){
            if ( !Tools::isJson($_upList) ){
                return apiFailResponse('DATA_CREATION_FAILED',
                    __('message.data.update.invalid_parameter',['attribute'=>'list']));
            }
            $_upList = json_decode($_upList, true);
        }

        $_isUpdate = $this->service->batchUpdate($_upList);

        if ( $_isUpdate ) {
            return apiSuccessResponse([],
                __('message.data.update.success',['attribute'=>__($this->langTermKey)]));
        }
        return apiFailResponse('DATA_CREATION_FAILED',
            __('message.data.update.fail',['attribute'=>__($this->langTermKey)]));
    }
}
