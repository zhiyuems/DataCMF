<?php
namespace Modules\Admin\Http\Controllers\System;
use Modules\Admin\Http\Controllers\Controller;
use Modules\Admin\Http\Requests\DictDetailsRequest;
use Modules\Admin\Service\DictService;
use Illuminate\Http\Request;
use ToolLibrary\Unit\Arr;
use App\Bin\Enum\Common AS CommonEnum;

/**
 * 字典明细控制器
 */
class DictController extends Controller
{
    /**
     * 字典明细处理服务
     *
     * @var DictService $service
     */
    private $service;

    protected $abilityName = 'system.dict';

    /**
     * 语言包 - 术语 - Key
     */
    private $langTermKey = 'term.dict';

    public function _initialize ()
    {
        $this->service = new DictService();
        $this->service->setGuardName(self::GUARD_NAME);
        $this->service->setGuardModuleId(self::GUARD_MODULE_ID);
        $this->authorizeResource($this->service->getModel());
    }

    /**
     * 获取全部字典明细列表
     * - GET /
     *
     * @powerName viewAny
     * @param Request $request
     */
    public function index(Request $request)
    {
        $this->service->setRequest($request);

        // 获取字典明细列表
        $_list = $this->service->allList();

        return apiSuccessResponse($_list);
    }

    /**
     * 创建字典明细信息（创建页展示）
     * - GET /create
     *
     * @powerName create
     *
     */
    public function create()
    {

    }

    /**
     * 创建字典明细信息（创建数据提交）
     * - POST /
     *
     * @powerName create
     * @param DictDetailsRequest $request
     *
     */
    public function store(DictDetailsRequest $request)
    {
        $request->validate('create');
        $_param = $request->validated();
        $_param['sort'] = empty($_param['sort'])||!is_numeric($_param['sort'])?50:intval($_param['sort']);
        $_param['status'] = empty($_param['status'])?CommonEnum::STATUS_OPEN:$_param['status'];

        if ( $this->service->create($_param) ) {
            return apiSuccessResponse([],
                __('message.data.create.success',['attribute'=>__($this->langTermKey)]));
        }
        return apiFailResponse('DATA_CREATION_FAILED',
            __('message.data.create.fail',['attribute'=>__($this->langTermKey)]));
    }

    /**
     * 获取指定管理字典明细信息（展示编辑页面）
     * - GET /{id}/edit
     *
     * @powerName update
     * @param Request $request
     * @param int     $id       字典明细ID
     *
     */
    public function edit($id)
    {
        // 获取指定字典明细信息
        $_roleInfo = $this->service->single($id);
        $_roleInfo = $_roleInfo->toArray();

        return apiSuccessResponse($_roleInfo);
    }

    /**
     * 更新指定字典明细信息（保存编辑数据）
     * - PUT/PATHCH /{id}
     *
     * @powerName update
     * @param DictDetailsRequest $request
     * @param string      $dictCode  字典编码
     *
     */
    public function update(DictDetailsRequest $request,$dictCode)
    {
        $this->service->setRequest($request);

        $request->validate('update');
        $_param = $request->validated();

        // 获取指定字典明细信息
        $_info = $this->service->single($dictCode);
        if (!$_info){
            return apiFailResponse('DATA_UPDATE_FAILED',
                __('message.data.not_exist'));
        }
        $_info = $_info->makeHidden('code')->toArray();

        // 比对提取需要更改的数据信息
        $_upInfo = Arr::diff($_param, $_info);
        if (empty($_upInfo) && empty($_role)){
            return apiFailResponse('INVALID_PARAMETER',
                __('message.data.no_updatable',['attribute'=>__($this->langTermKey)]));
        }

        // 更新更改数据
        if ( $this->service->update($_upInfo, $dictCode) )
        {
            return apiSuccessResponse([],
                __('message.data.update.success',['attribute'=>__($this->langTermKey)]));
        }
        return apiFailResponse('DATA_UPDATE_FAILED',
            __('message.data.update.fail',['attribute'=>__($this->langTermKey)]));
    }

    /**
     * 删除指定管理字典明细信息
     * - DELETE /{id}
     *
     * @powerName delete
     * @param Request $request
     * @param int     $id       字典明细ID
     *
     */
    public function destroy(DictDetailsRequest $request, $id)
    {
        $request->validate('destroy');
        $_param = $request->validated();

        // 判断字典明细是否存在
        if ( !$this->service->isExists($id, $_param['type']) ) {
            return apiFailResponse('INVALID_PARAMETER',
                __('message.data.not_exist'));
        }

        // 执行删除
        if ( $this->service->delete($id, $_param['type']) )
        {
            return apiSuccessResponse([], __('message.data.delete.success',['attribute'=>__($this->langTermKey)]));
        }
        return apiFailResponse('DATA_DELETE_FAILED',
            __('message.data.delete.fail',['attribute'=>__($this->langTermKey)]));
    }

}
