<?php
namespace Modules\Admin\Http\Controllers;
use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\PassportRequest;
use App\Service\LogsService;

/**
 * 超管后台认证
 */
class PassportController extends Controller
{
    /**
     * 日志处理服务
     *
     * @var LogsService $service
     */
    private $service;

    public function _initialize ()
    {
        $this->service = new LogsService('login');
    }

    /**
     * 管理员登录
     *
     * @param string $account   管理员登录账号
     * @param string $password  管理员登录密码
     * @param string $code      登录验证码
     *
     */
    public function toLogin (PassportRequest $request)
    {
        $this->service->setRequest($request);

        $request->validate('login');
        $_param = $request->validated();

        // 验证即登录
        if ( $this->auth->once($_param) ) {
            // 清除旧Token信息
            $this->auth->user()->tokens()->delete();

            // 获取用户信息
            $_userInfo = $this->auth->user()->makeHidden(['created_at','updated_at']);
            $_userInfo->append(['sex_label']);
            $_userInfo['dashboard'] = 0;

            // 用户角色信息
            $_roleInfo = $_userInfo['roles']->toArray();
            $_userInfo['role'] = data_get($_roleInfo,'*.name');
            $_userInfo['role_label'] = data_get($_roleInfo,'*.alias');
            unset($_userInfo['roles']);

            // 生成Token令牌
            $_token = $this->auth->user()->createToken()
                ->plainTextToken;
            [$id, $token] = explode('|', $_token, 2);

            $_response = apiSuccessResponse([
                'token' => $token,
                'userInfo' => $_userInfo
            ], __('message.login.success', ['attribute'=>$_param['account']]));

            $this->service->setResponse($_response);
            $this->service->record_login($_param['account'], '用户登录');
        }else{
            $_response = apiFailResponse( 'INVALID_LOGIN_INFO');

            $this->service->setResponse($_response);
            $this->service->record_login($_param['account'], '用户登录失败');
        }

        return $_response;
    }

    /**
     * 管理员退出登录
     * - 仅支持在头信息携带Authorization Bearer {xxx}时调用
     *
     * @param \Illuminate\Http\Request $request
     */
    public function toLogout(Request $request)
    {
        $this->service->setRequest($request);

        $_user = $request->user();

        if ($_user && $_user->tokens()->delete()) {
            $_response = apiSuccessResponse([], __('message.logout.success', ['attribute'=>$_user->account]));
        }else{
            $_response = apiFailResponse( 'PASSPORT_LOGOUT_FAIL');
        }

        $this->service->setResponse($_response);
        $this->service->record_login($_user->account, '用户退出');

        return $_response;
    }

}
