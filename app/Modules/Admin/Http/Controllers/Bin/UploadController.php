<?php
namespace Modules\Admin\Http\Controllers\Bin;
use Modules\Admin\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UploadRequest;
use App\Service\UploadService;

/**
 * 控制器 - 文件上传
 */
class UploadController extends Controller
{
    /**
     * 上传资源处理服务
     *
     * @var \App\Service\UploadService $service
     */
    private $service;

    public function _initialize()
    {
        $this->service = new UploadService;
    }

    /**
     * 统一静态资源上传处理方法
     *
     * @param UploadRequest $request
     * @param string $type 上传文件类型：image、file
     *
     */
    public function toUpload(UploadRequest $request, $type)
    {
        if ($type!=='image'){
            return apiFailResponse('FILE_FAIL_TYPE',null,'ERROR_ILLEGAL_PARAMETER');
        }

        $this->service->setCurrentType($type);

        $request->validate($type);
        $_param = $request->validated();

        $_fileInfo = $this->service->upload($_param['file']);

        return apiSuccessResponse([
            'file_name' => $_fileInfo['file_name'],
            'file_url' => $_fileInfo['file_url'],
            'file_path' => $_fileInfo['file_path'],
            'file_size' => $_fileInfo['size'],
            'file_hash' => $_fileInfo['file_hash'],
        ]);
    }
}
