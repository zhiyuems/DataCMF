<?php
namespace Modules\Admin\Http\Controllers\Bin;
use Modules\Admin\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * 控制器 - 系统类信息
 */
class SystemController extends Controller
{
    /**
     * 获取系统环境信息
     */
    public function toEnvironment(Request $request)
    {
        $_DataCMF = 'DataCMF ( Laravel '. \Illuminate\Foundation\Application::VERSION .' )';
        $_DataCMF = '<a title="DataCMF" target="_blank" href="https://gitee.com/nxqf/DataCMF">'.$_DataCMF.'</a>';

        $_environment = [];
        $_environment[] = ['label' => '服务端框架', 'value' => $_DataCMF];
        $_environment[] = ['label' => '服务器信息', 'value' => php_uname()];
        $_environment[] = ['label' => '服务器软件', 'value' => $request->server('SERVER_SOFTWARE')];
        $_environment[] = ['label' => 'PHP 版本', 'value' => PHP_VERSION . ' / '. php_sapi_name()];
        $_environment[] = ['label' => '最大上传限制', 'value' => get_cfg_var("upload_max_filesize") ? get_cfg_var("upload_max_filesize") : "不允许上传附件"];
        $_environment[] = ['label' => '最大执行时间', 'value' => get_cfg_var("max_execution_time") . "秒 "];
        $_environment[] = ['label' => '最大运行内存', 'value' => get_cfg_var("memory_limit") ? get_cfg_var("memory_limit") : "无"];
        $_environment[] = ['label' => 'MySQL 版本', 'value'=> DB::select('SHOW VARIABLES LIKE "version"')[0]->Value];

        return apiSuccessResponse($_environment);
    }
}
