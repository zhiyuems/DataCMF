<?php
namespace Modules\Admin\Http\Controllers;
use App\Http\Controllers\Controller AS BaseController;
use Illuminate\Support\Facades\Auth;

/**
 * @property \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard|mixed|void auth
 */
class Controller extends BaseController
{

    /**
     * API认证看守者名称
     * @var string
     */
    protected const GUARD_NAME = 'admin';

    /**
     * API认证模块标识
     * @var int
     */
    protected const GUARD_MODULE_ID = 1;

    /**
     * 权限组别名(权限组前缀)
     *
     * @var string $abilityName
     */
    protected $abilityName = '';

    /**
     * 权限组资源API方法Map
     *
     * @var array $abilityResourceMap
     */
    protected $abilityResourceMap = [
        'index' => 'viewAny',
        'show' => 'view',
        'create' => 'create',
        'store' => 'create',
        'edit' => 'update',
        'update' => 'update',
        'destroy' => 'delete',
    ];

    /**
     * 构造方法 __construct
     * - 继承类尽可能使用_initialize方法来代替
     *
     * @return void
     */
    public function __construct()
    {
        // 权限模块分组标识
        setPermissionsTeamId(self::GUARD_MODULE_ID);

        parent::__construct();
    }

    /**
     * 魔术方法 __get
     *
     * @param string $name 属性名
     *
     * @return mixed|void
     */
    public function __get(string $name)
    {
        if( $name === 'auth' ) {
            return $this->auth = Auth::guard(self::GUARD_NAME);
        }
    }

    /**
     * 获取资源方法到Ability名称的映射.
     *
     * @return array
     */
    protected function resourceAbilityMap()
    {
        $_map = [];
        foreach ($this->abilityResourceMap AS $funName => $mapName)
        {
            $_map[$funName] = sprintf('%s%s', $this->abilityName?$this->abilityName.'.':'', $mapName);
        }
        return $_map;
    }

    /**
     * 获取没有模型参数的资源方法列表.
     *
     * @return array
     */
    protected function resourceMethodsWithoutModels()
    {
        return ['index', 'create', 'store'];
    }

}
