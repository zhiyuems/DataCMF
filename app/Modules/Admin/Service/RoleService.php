<?php
namespace Modules\Admin\Service;
use Modules\Admin\Service\BasePermissionService AS Service;
use Illuminate\Http\Request;
use Modules\Admin\Models\Permission\Roles AS mRole;
use Modules\Admin\Models\System\Admin AS mAdmin;
use Illuminate\Support\Facades\DB;
use ToolLibrary\Unit\Tree;

/**
 * 服务类 - 角色管理服务
 */
class RoleService extends Service
{
    /**
     * 数据模型
     *
     * @var \Illuminate\Database\Eloquent\Model|mRole $model
     */
    protected $model;

    /**
     * 构造方法
     *
     * @param \Illuminate\Http\Request $request
     *
     */
    public function _initialize(Request $request=null)
    {
        $this->setModel(new mRole);
    }

    /**
     * 创建角色信息
     *
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Model|$this
     */
    public function create(array $data, ...$variable)
    {
        $data[config('permission.column_names.team_foreign_key')] = $this->guardModuleId;
        $data['guard_name'] = $this->guardName;

        return $this->model->create($data);
    }

    /**
     * 删除角色信息
     *
     * @param int $roleId
     *
     * @return bool
     */
    public function delete($id, ...$variable)
    {
        return $this->model->clearPermissionTo($id, $this->guardModuleId);
    }

    /**
     * 更新指定角色信息
     *
     * @param array $data 更新数据
     * @param int   $id   角色ID
     *
     * @return bool
     */
    public function update($data, $id, ...$variable)
    {
        if ( empty($data) )return false;

        $_isPermissionsUpdate = true;
        DB::beginTransaction();
        // 更新角色权限
        if (!empty($data['permissions'])){
            $_m = $this->model->findById($id, $this->guardName);
            $_m->guard_name = $this->guardName;

            // 获取已有权限列表
            $_havePermissions = $_m->permissions()->pluck('id','name')->toArray();
            if(empty($_havePermissions)){
                $_m->givePermissionTo($data['permissions']);
            }else{
                $_permissionsIds = array_values($_havePermissions);

                // 比较提取增加 或 撤销的权限
                $_addRole = array_values(array_diff($data['permissions'],$_permissionsIds));
                $_delRole = array_values(array_diff($_permissionsIds, $data['permissions']));

                if (!empty($_addRole)){$_m->givePermissionTo($_addRole);}
                if (!empty($_delRole)){
                    $_d = [];
                    array_walk($_havePermissions, function($id,$name) use ($_m,$_delRole,&$_d) {
                        if(in_array($id, $_delRole)) {
                            $_d[] = $name;
                        }
                    });
                    $_m->revokePermissionTo($_d);
                }
            }

            // 释放临时变量
            unset($_m,$_d, $_havePermissions,$_permissionsIds,
                $_addRole, $_delRole);
        }
        unset($data['permissions']);

        if (empty($data) && $_isPermissionsUpdate){
            $_isUpdate = $_isPermissionsUpdate;
        }else{
            $_isUpdate = $this->model->whereGuardName($this->guardName)
                ->where($this->model->getKeyName(), $id);

            if ( (bool)config('permission.teams') === true ) {
                $_isUpdate = $_isUpdate->whereTeamId($this->guardModuleId);
            }
            $_isUpdate = $_isUpdate->update($data);
        }
        $_isUpdate?DB::commit():DB::rollBack();
        $this->_clearCache();
        return $_isUpdate;
    }

    /**
     * 获取管理后台全部角色列表
     *
     * @return array
     */
    public function allList(...$variable)
    {
        $_list = $this->model->whereTeamId($this->guardModuleId)
            ->whereGuardName($this->guardName)->get()
            ->makeHidden(['team_id','guard_name'])
            ->toArray();

        // 将列表数据转化为树状结构
        $_list = Tree::generateTree($_list,'id', 'parent_id','children');

        return $_list;
    }

    /**
     * 获取管理后台指定角色信息
     *
     * @param int $roleId 角色ID
     *
     * @return false|array
     */
    public function single($id, ...$variable)
    {
        $_info = $this->model->oneInfo($id, $this->guardModuleId);
        if (!$_info)return false;

        // 获取关联权限列表
        $_permissionsList = $this->model
            ->findById($id, $this->guardName)
            ->permissions()->select('id','name','parent_id','meta_type')->get();

        $_powerList = $_permissionsList->toArray();

        $_info->permissions = data_get($_powerList,'*.id');
        $_info->permissions_name = data_get($_powerList,'*.name');

        return $_info->makeHidden(['team_id','guard_name']);
    }

    /**
     * 指定角色信息是否存在
     *
     * @param int $roleId 角色ID
     *
     * @return bool
     */
    public function isExists($id)
    {
        return $this->model->whereTeamId($this->guardModuleId)
            ->whereGuardName($this->guardName)
            ->where($this->model->getKeyName(), $id)
            ->isExists();
    }

    /**
     * 指定角色信息是否允许删除
     *
     * @param int $roleId 角色ID
     *
     * @return array
     */
    public function isDeleteAllowed($id)
    {
        $_m = $this->model->whereTeamId($this->guardModuleId)
            ->whereGuardName($this->guardName)
            ->where($this->model->getKeyName(), $id);

        $_info = $_m->first();

        // 是否是超管角色
        if ( $_info->name === config('permission.SuperAdministrator') ){
            return [
                'allow'=>false,
                'msg'=>__('message.data.prohibit_delete.prohibit',['attribute'=>__('term.super_administrators')])
            ];
        }

        // 查询具有指定角色的用户
        if( (new mAdmin)->role($_info->name)->count() > 0 ){
            return [
                'allow'=>false,
                'msg' => __('message.data.prohibit_delete.exist_data',[
                    'attribute' => __('term.role'),
                    'dataname'  => __('term.administrators')
                ])
            ];
        }

        return ['allow'=>true,];
    }
}
