<?php
namespace Modules\Admin\Service;
use Modules\Admin\Service\BasePermissionService AS Service;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Models\System\Admin AS mAdmin;
use Modules\Admin\Models\Permission\Roles AS mRoles;
use Illuminate\Support\Facades\Hash;

/**
 * 服务类 - 管理员管理服务
 */
class UserService extends Service
{
    /**
     * 数据模型
     *
     * @var \Modules\Admin\Models\System\Admin $model
     */
    protected $model;

    /**
     * 构造方法
     *
     * @param \Illuminate\Http\Request $request
     *
     */
    public function _initialize(...$attribute)
    {
        $this->setModel(new mAdmin);
    }

    /**
     * 创建数据信息
     *
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Model|false
     */
    public function create(array $data, ...$variable)
    {
        $data['avatar'] = parse_url($data['avatar'])['path'];
        $data['password'] = Hash::make($data['password']);

        if ( !empty($data['role']) ) { // 关联管理角色
            $_role = json_decode($data['role'], true);
            unset($data['role']);
        }

        // 创建管理员及角色信息关联
        DB::beginTransaction();
        if ($_user = $this->model->create($data)) {
            if(isset($_role))$this->assignRole($_user, $_role);

            DB::commit();
            return $_user;
        }
        DB::rollBack();
        return false;
    }

    /**
     * 删除数据信息
     *
     * @param int $id
     *
     * @return bool
     */
    public function delete($id, ...$variable)
    {
        $_m = $this->model->find($id);

        DB::beginTransaction();

        // 撤销所有角色
        $_haveRole = $_m->getRoleNames()->toArray();
        $this->removeRole($_m, $_haveRole);

        // 撤销权限
        $this->removePermissions($_m);
        if ($this->model->softDelete($id)){
            DB::commit();
            $this->_clearCache();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * 更新指定数据信息
     *
     * @param array $data 更新数据
     * @param int $id ID
     *
     * @return bool|int|\Illuminate\Database\Eloquent\Model
     */
    public function update($data, $id, ...$variable)
    {
        if ( empty($data) )return false;

        $_isRoleUpdate = true;
        DB::beginTransaction();
        // 更新角色信息
        if (!empty($data['role'])){
            $_m = $this->model->find($id);

            // 获取已有角色列表
            $_haveRole = $this->model->getRolePluck('id','name', $id);
            $_roleIds = array_values($_haveRole);

            // 比较提取增加 或 撤销的角色
            $_addRole = array_values(array_diff($data['role'],$_roleIds));
            $_delRole = array_values(array_diff($_roleIds, $data['role']));
            if (!empty($_addRole)){$this->assignRole($_m, $_addRole);}
            if (!empty($_delRole)){$this->removeRole($_m, $_delRole, $_haveRole);}

            // 释放临时变量
            unset($_m,$_haveRole,$_roleIds,
                $_addRole, $_delRole);
        }
        unset($data['role']);

        if (empty($data) && $_isRoleUpdate){
            $_isUpdate = $_isRoleUpdate;
        }else{
            // 更新管理员信息
            $_isUpdate = $this->model
                ->where($this->model->getKeyName(), $id)
                ->update($data);
        }
        $_isUpdate?DB::commit():DB::rollBack();
        $this->_clearCache();
        return $_isUpdate;
    }

    /**
     * 获取指定数据信息
     *
     * @param int $id ID
     *
     * @return false|array|\Illuminate\Database\Eloquent\Model
     */
    public function single($id, ...$variable)
    {
        $_info = $this->model->oneInfo($id);
        if (!$_info)return false;

        $_info->append(['roleName','roleIds']);
        $_info->makeHidden(['roles']);
        return $_info;
    }

    /**
     * 获取数据列表
     *
     * @return \Modules\Admin\Models\System\Admin;
     */
    public function allList(...$variable)
    {
        $_pageSize = intval($this->request->input('pageSize', 20));

        // 模糊搜索关键词(账号/昵称)
        if ($this->request->exists('keyword')) {
            $_keyword = $this->request->input('keyword');
            $this->model = $this->model->where(function($query) use ($_keyword) {
                $query->where('username', 'like', '%'.$_keyword.'%')
                    ->orWhere('account', 'like', '%'.$_keyword.'%');
            });
        }

        // 根据角色过滤管理员列表数据
        if ($this->request->exists('roleName')) {
            $this->model = $this->model->role($this->request->input('roleName'));
        }

        $_list = $this->model->paginate($_pageSize);
        $_list->append(['roleName','roleIds']);
        $_list->makeHidden(['roles']);

        return $_list;
    }

    /**
     * 关联指定角色
     *
     * @param mAdmin $model  用户模型
     * @param array  $role   关联角色id列表
     *
     * @return true
     */
    public function assignRole(mAdmin $model, $role)
    {
        $_mRoles = new mRoles;
        $_mRoles->guard_name = $this->guardName;

        $role = data_get($_mRoles->roleIdInfo($role,['name'],$this->guardModuleId)
            ->toArray(), '*.name');
        $model->assignRole($role);

        return true;
    }

    /**
     * 撤销指定角色
     *
     * @param mAdmin $model  用户模型
     * @param array $undoRole 要撤销角色别名列表
     * @param array $haveRole 已有角色别名列表,例如：['administrator'=>1, ...]
     *
     * @return true
     */
    public function removeRole(mAdmin $model, $role, $haveRole=null)
    {
        if (empty($haveRole)){
            array_map(function($name) use ($model) {
                $model->removeRole($name);
            }, $role);
        }else{
            array_walk($haveRole, function($id,$name) use ($model,$role) {
                if(in_array($id, $role)) {
                    $model->removeRole($name);
                }
            });
        }
        return true;
    }

    /**
     * 撤销指定权限
     *
     * @param mAdmin $model  用户find(x)模型
     * @param \Spatie\Permission\Contracts\Permission|\Spatie\Permission\Contracts\Permission[]|string|string[] $permission
     *
     * @return true
     */
    public function removePermissions(mAdmin $model, $permissions=null)
    {
        if (empty($permissions)){
            $model->permissions()->detach();
        }else{
            $model->revokePermissionTo($permissions);
        }
        return true;
    }
}
