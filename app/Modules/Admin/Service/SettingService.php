<?php
namespace Modules\Admin\Service;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Service\BasePermissionService AS Service;
use Modules\Admin\Models\System\Configure as mConfigure;
use ToolLibrary\Unit\Arr;
use ToolLibrary\Unit\Tree;

class SettingService extends Service
{
    /**
     * 数据模型
     *
     * @var mConfigure $model
     */
    protected $model;

    /**
     * 构造方法
     *
     * @param \Illuminate\Http\Request $request
     *
     */
    public function _initialize()
    {
        $this->setModel(new mConfigure);
    }

    /**
     * 获取全部配置数据信息
     *
     * @return array
     */
    public function getAllSetting()
    {
        $_dataFormat = $this->request->input('format','default');
        if ( $_dataFormat === 'group' ) { // 将分组结构列表数据
            $_list = $this->model->getAllGroupConfig();
        }else{
            $_list = $this->model->getAllConfig();
        }

        return $_list;
    }

    /**
     * 获取指定数据信息
     *
     * @param string $id ID
     * @param string $modelKey 查询字段名
     *
     * @return false|array|\Illuminate\Database\Eloquent\Model
     */
    public function single($id, ...$variable)
    {
        $variable[0] = empty($variable[0])?'':$variable[0];

        $_info = $this->model->getVariableKeyValue($id, null, null, $variable[0]);
        if (!$_info)return false;
        return $_info;
    }

    /**
     * 创建数据信息
     *
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data, ...$variable)
    {
        return $this->model->createVariableKey($data);
    }

    /**
     * 更新指定数据信息
     *
     * @param array $data 更新数据
     * @param string $id ID
     * @param string $modelKey 查询字段名
     *
     * @return bool|int|\Illuminate\Database\Eloquent\Model
     */
    public function update($data, $id, ...$variable)
    {
        if ( empty($data) )return false;

        $variable[0] = empty($variable[0])?'':$variable[0];

        // 更新系统变量信息
        $_isUpdate = $this->model->setVariableValue($id, $data, $variable[0]);

        return $_isUpdate;
    }

    /**
     * 批量更新指定数据信息
     *
     * @param array $list 更新数据
     *
     * @return bool|int|\Illuminate\Database\Eloquent\Model
     */
    public function batchUpdate(array $list)
    {
        if ( empty($list) )return false;

        $_keyName = 'variable_name';

        $_variableKeys = data_get($list, '*.'.$_keyName);
        $_variableOldData = $this->model->whereIn($_keyName, $_variableKeys)->get()->toArray();
        $_variableOldData = Arr::keyArrange($_variableOldData, $_keyName);

        DB::beginTransaction();
        // 更新
        $_upNum = 0;
        array_map(function($item) use ($_keyName,$_variableOldData, &$_upNum) {
            // 比对提取需要更改的数据信息
            $_upInfo = Arr::diff($item, $_variableOldData[$item[$_keyName]]);
            if ( !empty($_upInfo) ){
                if ( $this->model->where($_keyName, $item[$_keyName])
                    ->update($_upInfo) ){
                    $_upNum++;
                }
            }
        }, $list);
        if ( count($list) === $_upNum ){
            DB::commit();
            $this->model->configResetCache();
            return true;
        }
        DB::rollBack();
        return false;
    }

    /**
     * 指定系统变量信息是否存在
     *
     * @param string|array $roleId 系统变量名称
     *
     * @return bool
     */
    public function isExists($id, $keyName='variable_name')
    {
        if (is_array($id)){
            $_m = $this->model->whereIn($keyName, $id);
        }else{
            $_m = $this->model->where($keyName, $id);
        }

        return $_m->isExists();
    }
}
