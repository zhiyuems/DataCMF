<?php
namespace Modules\Admin\Service;
use Modules\Admin\Service\BasePermissionService AS Service;
use Modules\Admin\Models\System\Jobs AS mJobs;
use ToolLibrary\Unit\Tree;

/**
 * 服务类 - 岗位管理服务
 */
class JobsService extends Service
{
    /**
     * 数据模型
     *
     * @var mJobs $model
     */
    protected $model;

    /**
     * 构造方法
     *
     * @param \Illuminate\Http\Request $request
     *
     */
    public function _initialize(...$attribute)
    {
        $this->setModel(new mJobs);
    }

    /**
     * 创建数据信息
     *
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Model|false
     */
    public function create(array $data, ...$variable)
    {
        // 创建岗位信息
        if ($_data = $this->model->create($data)) {
            return $_data;
        }
        return false;
    }

    /**
     * 删除数据信息
     *
     * @param int $id
     *
     * @return bool
     */
    public function delete($id, ...$variable)
    {
        $_m = $this->model->find($id);

        if ($_m->delete()){
            return true;
        }

        return false;
    }

    /**
     * 更新指定数据信息
     *
     * @param array $data 更新数据
     * @param int $id ID
     *
     * @return bool|int|\Illuminate\Database\Eloquent\Model
     */
    public function update($data, $id, ...$variable)
    {
        if ( empty($data) )return false;

        // 更新岗位信息
        $_isUpdate = $this->model
            ->where($this->model->getKeyName(), $id)
            ->update($data);

        return $_isUpdate;
    }

    /**
     * 获取指定数据信息
     *
     * @param int $id ID
     *
     * @return false|array|\Illuminate\Database\Eloquent\Model
     */
    public function single($id, ...$variable)
    {
        $_info = $this->model->oneInfo($id);
        if (!$_info)return false;

        return $_info;
    }

    /**
     * 获取数据列表
     *
     * @return array
     */
    public function allList(...$variable)
    {
        $_pageSize = intval($this->request->input('pageSize', 20));

        $_list = $this->model->paginate($_pageSize);

        return $_list;
    }

}
