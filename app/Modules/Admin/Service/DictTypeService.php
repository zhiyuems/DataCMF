<?php
namespace Modules\Admin\Service;
use Modules\Admin\Service\BasePermissionService AS Service;
use Modules\Admin\Models\System\DictType AS mDictType;
use Modules\Admin\Models\System\DictDetails AS mDictDetails;
use Illuminate\Support\Facades\DB;

/**
 * 服务类 - 字典类型管理服务
 */
class DictTypeService extends Service
{
    /**
     * 字典类型数据模型
     *
     * @var mDictType $model
     */
    protected $model;

    /**
     * 构造方法
     */
    public function _initialize()
    {
        $this->setModel(new mDictType);
    }

    /**
     * 创建数据信息
     *
     * @param array $data
     *
     * @return mDictType|false
     */
    public function create(array $data)
    {
        // 创建字典类型信息
        if ($_data = $this->model->create($this->model->formatField($data))) {
            return $_data;
        }
        return false;
    }

    /**
     * 删除数据信息
     *
     * @param int $id
     *
     * @return bool
     */
    public function delete($id)
    {
        $_m = $this->model->find($id);
        $_mDictDetails = new mDictDetails;
        $_isExistsDetails = $_mDictDetails->where('dict_type', $id)->limit(1)->value('dict_type');

        DB::beginTransaction();
        if ($_m->delete()){
            if ($_isExistsDetails){
                if ($_mDictDetails->where('dict_type', $id)->delete()){
                    DB::commit();
                    return true;
                }
            }else{
                DB::commit();
                return true;
            }
        }
        DB::rollBack();
        return false;
    }

    /**
     * 更新指定数据信息
     *
     * @param array $data 更新数据
     * @param int $id ID
     *
     * @return bool|int|mDictType
     */
    public function update($data, $id)
    {
        if ( empty($data) )return false;

        // 更新字典类型信息
        $_isUpdate = $this->model
            ->where($this->model->getKeyName(), $id)
            ->update($this->model->formatField($data));

        return $_isUpdate;
    }

    /**
     * 获取指定数据信息
     *
     * @param int $id ID
     *
     * @return false|mDictType
     */
    public function single($id)
    {
        $_info = $this->model->oneInfo($id);
        if (!$_info)return false;

        return $_info;
    }

    /**
     * 获取数据列表
     *
     * @return array
     */
    public function allList()
    {
        $_list = $this->model->getTypeList()->toArray();

        return $_list;
    }

}
