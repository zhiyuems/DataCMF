<?php
namespace Modules\Admin\Service;
use App\Bin\Enum\LogsType;
use Modules\Admin\Service\BasePermissionService AS Service;

/**
 * 服务类 - 日志管理服务
 */
class LogsService extends Service
{
    /**
     * 日志数据模型
     *
     * @var array $typeModel
     */
    protected $typeModel = [
        'system' => \Modules\Admin\Models\System\Logs\System::class,
        'login'  => \Modules\Admin\Models\System\Logs\Login::class,
        'sql'  => \Modules\Admin\Models\System\Logs\Login::class,
    ];

    /**
     * 日志数据模型
     *
     * @var \Modules\Admin\Models\System\Logs\System|\Modules\Admin\Models\System\Logs\Login $model
     */
    protected $model;

    /**
     * 设置Models类
     *
     * @param string $models
     *
     * @return void
     */
    public function setModel($models)
    {
        if (!isset($this->typeModel[$models])){
            abort(apiFailResponse('INVALID_PARAMETER',null,'ERROR_ILLEGAL_PARAMETER'));
        }
        parent::setModel(new $this->typeModel[$models]);
    }

    /**
     * 获取数据列表
     *
     * @return \Modules\Admin\Models\BaseModels;
     */
    public function allList()
    {
        $_pageSize = intval($this->request->input('pageSize', 20));

        $_logTypeAs = $this->request->input('log_type','system');
        $this->setModel($_logTypeAs);
        // 根据日志类型过滤列表数据
        $_subType = null;
        $_logLevel = null;
        if ($this->request->exists('type')) {
            $_subType = $this->request->input('type');
        }
        if ($this->request->exists('level')) {
            $_logLevel = $this->request->input('level');
        }


        $_list = $this->model->getLogsList($_subType, $_pageSize, $_logLevel);

        return $_list;
    }

}
