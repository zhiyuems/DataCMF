<?php
namespace Modules\Admin\Service;
use App\Service\BaseService AS Service;
use App\Bin\Contract\Service\AdminPermissionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use ToolLibrary\Unit\Tree;

class BasePermissionService extends Service
    implements AdminPermissionService
{

    /**
     * Request类
     * @var \Illuminate\Http\Request $request
     */
    protected $request;

    /**
     * 数据模型
     *
     * @var \Illuminate\Database\Eloquent\Model $model
     */
    protected $model;

    /**
     * 权限认证模块标识
     * @var int
     */
    protected $guardModuleId = 1;

    /**
     * 权限认证看守者名称
     *
     * @var string $guardName
     */
    protected $guardName = 'admin';

    /**
     * 构造方法
     *
     * @param \Illuminate\Http\Request $request
     *
     */
    public function __construct(Request $request=null)
    {
        if(!empty($request))$this->request = $request;
        $this->setGuardName(self::getGuardName());

        if(method_exists($this,'_initialize'))call_user_func_array([$this,'_initialize'], func_get_args());
    }

    /**
     * 设置GuardName看守者
     *
     * @param string $guardName
     *
     * @return void
     */
    public function setGuardName(string $guardName)
    {
        $this->guardName = $guardName;
        if(!empty($this->model)){
            $this->model->guard_name = $guardName;
        }
    }

    public function getGuardName()
    {
        return $this->guardName;
    }

    /**
     * 设置Guard模块ID
     *
     * @param int $moduleId
     *
     * @return void
     */
    public function setGuardModuleId(int $moduleId)
    {
        $this->guardModuleId = $moduleId;
    }

    public function getGuardModuleId()
    {
        return $this->guardModuleId;
    }

    /**
     * 设置Request类
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    public function getRequest()
    {
        return $this->request;
    }

    /**
     * 设置Models类
     *
     * @param \Illuminate\Database\Eloquent\Model|\App\Models\BaseModels $models
     *
     * @return void
     */
    public function setModel($models)
    {
        $this->model = $models;
        $this->setGuardName($this->guardName);
    }

    public function getModel()
    {
        return $this->model;
    }

    /**
     * 创建数据信息
     *
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * 删除数据信息
     *
     * @param int $id
     *
     * @return bool
     */
    public function delete($id)
    {
        // 自行实现
        return false;
    }

    /**
     * 更新指定数据信息
     *
     * @param array $data 更新数据
     * @param int $id ID
     *
     * @return bool|int|\Illuminate\Database\Eloquent\Model
     */
    public function update($data, $id)
    {
        if (empty($data))return false;

        $_isUpdate = $this->model
            ->where($this->model->getKeyName(), $id)
            ->update($data);
        $this->_clearCache();
        return $_isUpdate;
    }

    /**
     * 获取指定数据信息
     *
     * @param int $id ID
     *
     * @return false|array|\Illuminate\Database\Eloquent\Model
     */
    public function single($id)
    {
        $_info = $this->model->where($this->model->getKeyName(), $id)->first();
        if (!$_info)return false;
        return $_info->toArray();
    }

    /**
     * 获取数据列表
     *
     * @return array
     */
    public function allList()
    {
//        $_list = $this->model->whereTeamId($this->guardModuleId)
//            ->whereGuardName($this->guardName)->get()
//            ->makeHidden(['team_id','guard_name'])
//            ->toArray();
        $_list = $this->model->get()->toArray();

        // 将列表数据转化为树状结构
//        $_list = Tree::generateTree($_list,'id', 'parent_id','children');

        return $_list;
    }

    /**
     * 指定数据信息是否存在
     *
     * @param int $id ID
     *
     * @return bool
     */
    public function isExists($id)
    {
        return $this->model->where($this->model->getKeyName(), $id)
            ->isExists();
    }

    /**
     * 清除权限相关缓存
     *
     * @return void
     */
    protected function _clearCache()
    {
        Artisan::call('optimize:clear');
    }

}
