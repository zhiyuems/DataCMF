<?php
namespace Modules\Admin\Service;
use Modules\Admin\Service\BasePermissionService AS Service;
use Modules\Admin\Models\System\DictType AS mDict;
use Modules\Admin\Models\System\DictDetails AS mDictDetails;

/**
 * 服务类 - 字典管理服务
 */
class DictService extends Service
{
    /**
     * 字典数据模型
     *
     * @var mDict $typeModel
     */
    protected $typeModel;

    /**
     * 字典数据模型
     *
     * @var mDictDetails $model
     */
    protected $model;

    /**
     * 构造方法
     */
    public function _initialize()
    {
        $this->setModel(new mDictDetails);
        $this->typeModel = new mDict;
    }

    /**
     * 创建数据信息
     *
     * @param array $data
     *
     * @return mDictDetails|false
     */
    public function create(array $data)
    {
        // 创建字典信息
        if ($_data = $this->model->createDetails($data)) {
            return $_data;
        }
        return false;
    }

    /**
     * 删除数据信息
     *
     * @param int $id
     *
     * @return bool
     */
    public function delete($id)
    {
        $tpye = func_get_arg(1);
        if (!$tpye)return false;

        $_m = $this->model->where('dict_type', $tpye)
            ->find($id);

        if ($_m->delete()){
            return true;
        }

        return false;
    }

    /**
     * 更新指定数据信息
     *
     * @param array $data 更新数据
     * @param int $id ID
     *
     * @return bool|int|mDict
     */
    public function update($data, $id)
    {
        if ( empty($data) )return false;

        // 更新字典信息
        $_isUpdate = $this->model
            ->where($this->model->getKeyName(), $id)
            ->update($this->model->formatField($data));

        return $_isUpdate;
    }

    /**
     * 获取指定数据信息
     *
     * @param int $id ID
     *
     * @return false|mDictDetails
     */
    public function single($id)
    {
        $_info = $this->model->oneInfo($id);
        if (!$_info)return false;

        return $_info;
    }

    /**
     * 获取数据列表
     *
     * @return \Modules\Admin\Models\System\Admin;
     */
    public function allList()
    {
        $_pageSize = intval($this->request->input('pageSize', 20));

        // 根据字典类型code过滤列表数据
        $_code = null;
        if ($this->request->exists('code')) {
            $_code = $this->request->input('code');
        }

        $_list = $this->model->getDictList($_code, null, $_pageSize);

        return $_list;
    }

    /**
     * 指定数据信息是否存在
     *
     * @param int $id ID
     *
     * @return bool
     */
    public function isExists($id)
    {
        $tpye = func_get_arg(1);
        if (!$tpye)return false;

        return $this->model->where('dict_type', $tpye)
            ->where($this->model->getKeyName(), $id)
            ->isExists();
    }
}
