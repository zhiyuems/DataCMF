<?php
namespace Modules\Admin\Service;
use Modules\Admin\Service\BasePermissionService AS Service;
use Illuminate\Http\Request;
use Modules\Admin\Models\Permission\Permission AS mMenu;
use ToolLibrary\Unit\Tree;
use App\Bin\Unit\MetaUnit;

/**
 * 服务类 - 菜单管理服务
 */
class MenuService extends Service
{

    /**
     * 后台管理菜单模型
     *
     * @var \App\Models\Permission\Permission $model
     */
    protected $model;

    /**
     * 构造方法
     *
     * @param \Illuminate\Http\Request $request
     *
     */
    public function _initialize(Request $request=null)
    {
        $this->setModel(new mMenu);
    }

    /**
     * 创建菜单(权限)信息
     *
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Model|$this
     */
    public function create(array $data, ...$variable)
    {
        $data['guard_name'] = $this->guardName;

        return $this->model->create($data);
    }

    /**
     * 删除菜单(权限)信息
     *
     * @param int|array $roleId
     *
     * @return bool
     */
    public function delete($id, ...$variable)
    {
        if (is_array($id)){
            $_m = $this->model->whereIn($this->model->getKeyName(), $id);
        }else{
            $_m = $this->model->where($this->model->getKeyName(), $id);
        }

        if ($_m->delete()){
            $this->_clearCache();
            return true;
        }
        return false;
    }

    /**
     * 更新指定菜单(权限)信息
     *
     * @param array $data 更新数据
     * @param int   $id   菜单(权限)ID
     *
     * @return bool
     */
    public function update($data, $id, ...$variable)
    {
        $_isUpdate = $this->model->whereGuardName($this->guardName)
            ->where($this->model->getKeyName(), $id);

        $_isUpdate = $_isUpdate->update($data);
        $this->_clearCache();
        return $_isUpdate;
    }

    /**
     * 获取当前管理员后台菜单
     * - 执行前先设置setRequest
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function myList()
    {
        $_userInfo = $this->request->user();
        if (!$_userInfo)return ['menu'=>[],'permissions'=>[]];
        if ($_userInfo->hasRole(config('permission.SuperAdministrator')) ? true : null)
        {
            $_list = $this->model->getAll()->toArray();
        } else {
            $_list = $_userInfo->getAllPermissions()->toArray();
            // ->filter(function ($value, $key) {
            //                return !($value['meta_type'] === 'button');
            //            })
        }

        $_list = $this->formatFrontendApi($_list);


        $_permissions = data_get($_list, '*.ability', []);
        $_permissions = array_unique($_permissions);
//        $_list = $this->model->get()->makeHidden(['created_at','updated_at'])->toArray();
        $_list = Tree::generateTree($_list,'id', 'parent_id','children');

        return [
            'menu' => $_list,
            'permissions' => $_permissions
        ];
    }

    /**
     * 获取管理后台全部菜单列表
     *
     * @return array
     */
    public function allList(...$variable)
    {
        $_list = $this->model->getAll()->toArray();

        $_list = $this->formatFrontendApi($_list);

        // 将列表数据转化为树状结构
        $_list = Tree::generateTree($_list,'id', 'parent_id','children');

        return $_list;
    }

    /**
     * 获取管理后台指定菜单(权限)信息
     *
     * @param int $roleId 菜单(权限)ID
     *
     * @return false|\Illuminate\Database\Eloquent\Model|object|static
     */
    public function single($id, ...$variable)
    {
        $_info = $this->model->oneInfo($id);
        if (!$_info)return false;
        return $_info->makeHidden(['guard_name']);
    }

    /**
     * 指定菜单(权限)信息是否存在
     *
     * @param int|array $roleId 菜单(权限)ID
     *
     * @return bool
     */
    public function isExists($id)
    {
        $_m = $this->model->whereGuardName($this->guardName);

        if (is_array($id)){
            $_m = $_m->whereIn($this->model->getKeyName(), $id);
        }else{
            $_m = $_m->where($this->model->getKeyName(), $id);
        }

        return $_m->isExists();
    }

    /**
     * 格式化菜单列表格式数据
     *
     * @return array
     */
    public function formatFrontendApi($list)
    {
        foreach ($list AS $i=>$item)
        {
            $item['ability'] = $item['name'];
            $item['name'] = $item['view_name'];
            $item['api_url'] = empty($item['api_url'])?[]:[
                ['httpmode'=>['get'], 'url'=>$item['api_url']]
            ];

            unset($item['view_name']);
            $item = array_filter($item, function($var){
                return is_null($var) !== true;
            });
            if(!isset($item['parent_id']))$item['parent_id'] = 0;
            $list[$i] = $item;
        }

        // 格式化聚合Meta字段数据
        $list = MetaUnit::gather($list);

        return $list;
    }
}
