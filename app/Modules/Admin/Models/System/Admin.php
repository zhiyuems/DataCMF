<?php
namespace Modules\Admin\Models\System;
use App\Models\System\Admin AS Model;

class Admin extends Model
{

    /**
     * 获取指定管理员信息
     *
     * @param int $id 管理员ID
     *
     * @return \Illuminate\Database\Eloquent\Model|object|static|null
     */
    public function oneInfo($id)
    {
        return $this->where($this->getKeyName(), $id)
            ->first();
    }

    /**
     * 提前管理员所有分配角色
     *
     * @return array
     */
    public function getRolePluck($value='id',$key=null, $uid=null)
    {
        if ($uid){
            return $this->find($uid)->roles->pluck($value, $key)->toArray();
        }
        return $this->roles->pluck($value, $key)->toArray();
    }

    /**
     * 软删除
     *
     * @param int $id 管理员ID
     *
     * @return bool|null
     */
    public function softDelete($id)
    {
        $this->where($this->getKeyName(), $id)->update(['account'=>'del_'.$id]);
        return $this->where($this->getKeyName(), $id)->delete();
    }
}
