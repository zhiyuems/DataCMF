<?php
namespace Modules\Admin\Models\System;
use App\Models\System\DictDetails AS Model;

class DictDetails extends Model
{
    /**
     * 获取指定信息
     *
     * @param int $id 主键ID
     *
     * @return \Illuminate\Database\Eloquent\Model|DictDetails|null
     */
    public function oneInfo($id)
    {
        return $this->detailsSelect()->where($this->getKeyName(), $id)
            ->first();
    }
}
