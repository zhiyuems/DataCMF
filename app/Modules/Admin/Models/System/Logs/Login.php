<?php
namespace Modules\Admin\Models\System\Logs;
use App\Models\System\Logs\Login AS Model;

/**
 * 模型 - 日志 - 登录日志
 */
class Login extends Model
{
    /**
     * 获取日志列表
     *
     * @param string $type      日志类型值
     * @param int    $paginate
     * @param string $level     日志级别
     */
    public function getLogsList($type=null, $paginate=null, $level=null)
    {
        $_m = $this->orderBy('created_at','desc');
        $_m = $_m->orderBy('id','desc');

        return $paginate?$_m->paginate($paginate):$_m->get();
    }
}
