<?php
namespace Modules\Admin\Models\System\Logs;
use App\Models\System\Logs\System AS Model;

/**
 * 模型 - 日志 - 系统日志
 */
class System extends Model
{

    /**
     * 获取日志列表
     *
     * @param string $type      日志类型值
     * @param int    $paginate
     * @param string $level     日志级别
     */
    public function getLogsList($type=null, $paginate=null, $level=null)
    {
        $_m = $this->orderBy('created_at','desc');
        $_m = $_m->orderBy('id','desc');
        if ($type){
            $_m = $_m->where('log_type', $type);
        }
        if ($level){
            $_m = $_m->where('log_level', $level);
        }

        return $paginate?$_m->paginate($paginate):$_m->get();
    }
}
