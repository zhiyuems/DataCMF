<?php
namespace Modules\Admin\Models\Permission;
use App\Models\Permission\Permission AS Model;

/**
 * 系统模型 - 后台菜单(权限明细)
 */
class Permission extends Model
{
    /**
     * 获取展示的菜单数据列表
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getShowMenuList()
    {
        return $this->where('meta_type','<>','button')
            ->where('meta_hidden',1)
            ->orderBy('sort','asc')
            ->get();
    }

    /**
     * 获取所有菜单数据列表
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getAll()
    {
        return $this->orderBy('sort','asc')
            ->get();
    }
}
