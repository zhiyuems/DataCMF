<?php
namespace App\Service;
use App\Service\BaseService AS Service;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Bin\Unit\UserAgent;
use App\Bin\Enum\LogsType AS LogsTypeEnum;
use App\Bin\Enum\LogsLevel AS LogsLevelEnum;
use phpDocumentor\Reflection\Types\ClassString;

/**
 * 服务 - 应用全局日志管理
 */
final class LogsService extends Service
{
    /**
     * 日志基本场景:system、login
     *
     * @var string $scene
     */
    protected $scene = 'system';

    /**
     * 日志类型
     *
     * @var string $logs_type
     */
    protected $logs_type = LogsTypeEnum::OPERATION;

    /**
     * 日志级别
     *
     * @var string $logs_level
     */
    protected $logs_level = LogsLevelEnum::INFO;

    /**
     * 模型Map
     *
     * @var array $modelsMap
     */
    protected $modelsMap = [
        'system' => \App\Models\System\Logs\System::class,
        'login'  => \App\Models\System\Logs\Login::class,
    ];

    /**
     * 日志模型
     *
     * @var \App\Modules\Admin\Models\System\Logs\System|\App\Modules\Admin\Models\System\Logs\Login $models
     */
    protected $models;

    /**
     * Request类
     * @var \Illuminate\Http\Request $request
     */
    protected $request;

    /**
     * Response类
     * @var \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory $response
     */
    protected $response;

    /**
     * 允许记录的响应class
     *
     * @var array $recordClass
     */
    protected $recordClass = [
        \Illuminate\Http\JsonResponse::class
    ];

    /**
     * 是否记录
     *
     * @var bool $isRecord
     */
    protected $isRecord = true;

    /**
     * 记录默认值
     *
     * @var array $record
     */
    protected $record = [
        'ip_address' => '127.0.0.1',
        'response'   => ''
    ];

    public function _initialize($scene=null)
    {
        $this->setScene(empty($scene)?$this->scene:$scene);
    }

    /**
     * 设置日志基本场景
     *
     * @param string $scene
     *
     * @return void
     */
    public function setScene(string $scene)
    {
        $this->scene = $scene;
        if (isset($this->modelsMap[$this->scene])){
            $this->models = new $this->modelsMap[$this->scene];
        }
    }

    /**
     * 设置日志类型
     *
     * @param string $type
     *
     * @return void
     */
    public function setLogType(string $type)
    {
        $this->logs_type = $type;
    }

    /**
     * 设置日志级别
     *
     * @param string $level
     *
     * @return void
     */
    public function setLogLevel(string $level)
    {
        $this->logs_level = $level;
    }

    /**
     * 设置当前操作模块
     *
     * @param string|ClassString $class    当前操作类
     * @param string    $method   当前操作方法名称
     *
     * @return void
     */
    public function setModule($class, $method)
    {
        $this->record['class_name'] = is_string($class)?$class:get_class($class);
        $this->record['class_method'] = $method;
    }

    /**
     * 设置Request类
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
        $this->record['ip_address'] = $this->request->getClientIp();
        if ($this->scene === 'login'){
            UserAgent::setUaText($this->request->header('user-agent'));

            $this->record['browser'] = UserAgent::getBrowserName();
            if(!$this->record['browser'])$this->record['browser']='未知';
            $this->record['system'] = UserAgent::getOsPlatform();
            if(!$this->record['system'])$this->record['system']='未知';
        }else{
            $this->record['log_type'] = $this->logs_type;
            $this->record['log_level'] = $this->logs_level;
            $this->record['header'] = json_encode($this->request->header());
            $this->record['url'] = $this->request->getPathInfo();
            $this->record['param'] = json_encode($this->request->input());
            $this->record['method'] = $this->request->getMethod();
            $this->record['operator'] = $this->_operator($request);
        }
    }

    /**
     * 设置Response
     *
     * @param \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory $response
     *
     * @return void
     */
    public function setResponse($response)
    {
        if (!in_array(get_class($response), $this->recordClass)){
            $this->isRecord = false;
            return;
        }
        $this->response = $response;
        $this->record['response'] = $this->response?$this->response->getContent():null;
    }

    /**
     * 登录记录
     *
     * @param string $account 登录账号
     * @param string $notes   注释
     *
     * @return void
     */
    public function record_login($account, $notes=null)
    {
        $this->_record($notes, $account);
    }

    /**
     * 系统记录
     *
     * @param string $notes   注释
     *
     * @return void
     */
    public function record_sys($notes=null)
    {
        $this->_record($notes);
    }

    protected function _record($notes=null, $account=null)
    {
        if ($this->isRecord===false)return;
        if (isset($this->record['method']) && $this->record['method'] === 'GET')return;
        if ($notes){
            $this->record['notes'] = $notes;
        }
        if ($account){
            $this->record['account'] = $account;
        }
        $this->models->create($this->record);
    }

    protected function _operator($request)
    {
        $_operator = '未知';
        if ( $_user = $request->user('sanctum') ){
            $_operator = sprintf('%d|%s', $_user->id, $_user->username);
        }
        return $_operator;
    }
}
