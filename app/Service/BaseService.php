<?php
namespace App\Service;


class BaseService
{
    /**
     * 构造方法 __construct
     * - 继承类尽可能使用_initialize方法来代替
     *
     * @return void
     */
    public function __construct()
    {
        if(method_exists($this,'_initialize'))call_user_func_array([$this,'_initialize'], func_get_args());
    }
}
