<?php
namespace App\Service;
use App\Service\BaseService AS Service;

/**
 * 服务 - 应用全局字典管理
 */
final class DictService extends Service
{
    /**
     * 字典配置模型
     *
     * @var string $modelsClass
     */
    private $modelsClass = \App\Models\System\DictType::class;

    /**
     * 字典配置模型
     *
     * @var \App\Models\System\DictType $models
     */
    private $models;

    /**
     * 系统字典
     *
     * @var array $variableList
     */
    private $variableList = [];

    public function _initialize()
    {
        $this->models = new $this->modelsClass;
        $this->variableList = $this->models->getAllConfig();
    }

    /**
     * 获取字典列表
     *
     * @return array
     */
    public function getVariableList()
    {
        return $this->variableList;
    }
}
