<?php
namespace App\Service;
use App\Bin\Enum\ApiCode;
use App\Bin\Enum\ApiSubCode;

final class ApiResponseService
{
    /**
     * ApiService 响应配置
     *
     * @var array
     */
    private $config = [];

    private $code;
    private $msg;
    private $subcode;
    private $submsg;
    private $data;

    public function __construct($app)
    {
        $this->config = config('api.response_configure');
        $this->code = $this->msg = '';
        $this->subcode = $this->submsg = '';
        $this->data = [];
    }

    /**
     * 设置错误码
     *
     * @param string $code
     * @param string $msg
     *
     * @return $this
     */
    public function code($code, $msg=null)
    {
        $this->code = $code;
        $this->msg = $msg;
        return $this;
    }

    /**
     * 设置子错误码
     *
     * @param string $code
     * @param string $msg
     *
     * @return $this
     */
    public function sub($code, $msg=null)
    {
        $this->subcode = $code;
        $this->submsg = $msg;
        return $this;
    }

    /**
     * 设置响应数据
     *
     * @param array|mixed $data
     *
     * @return $this
     */
    public function data($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * 统一响应执行
     *
     * @return \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory|mixed
     */
    public function response()
    {
        $response = [];

        $response[$this->config['code']] = !isset($this->code)?ApiCode::ERROR_SUCCESS:
            ApiCode::searchKey($this->code, ApiCode::ERROR_SUCCESS);
        $response[$this->config['msg']] = ApiCode::getMsg($response[$this->config['code']], 'name');
        if (!empty($this->subcode)){
            $response[$this->config['sub_code']] = ApiSubCode::searchKey($this->subcode, null);
            $response[$this->config['sub_msg']] = !empty($this->submsg)?$this->submsg:
                ApiSubCode::getMsg($response[$this->config['sub_code']], 'name');
        }

        $response[$this->config['data']] = $this->data;


        return response()->json($response);
    }

}
