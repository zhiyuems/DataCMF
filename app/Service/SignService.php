<?php
namespace App\Service;
use App\Service\BaseService AS Service;
use App\Bin\Traits\LogsTrait;
use ToolLibrary\Unit\Arr;

class SignService extends Service
{
    use LogsTrait;

    /**
     * SIGN的盐
     *
     * @var string $SIGN_KEY
     */
    private static $SIGN_KEY;

    /**
     * 对照Sign规则验证给定的值
     *
     * @param  array   $value
     * @param  string  $checkValue
     *
     * @return bool
     */
    public static function check($value, $checkValue)
    {
        return self::make($value) === $checkValue;
    }

    /**
     * 生成给定值的SIGN
     *
     * @param  array  $value
     *
     * @return string
     */
    public static function make(array $value)
    {
        // ascii排序
        ksort($value);

        // 构建http_build_query
        $_str = [];
        foreach ($value AS $name=>$val)
        {
            if (empty($val) && !is_numeric($val))continue;
            // 若为数组转化为JSON字符串
            if ( is_array($val) ){
                $val = Arr::filter($val);
                $val = json_encode($val, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
            }
            $_str[] = sprintf('%s=%s', $name, $val);
        }
        $_str = implode('&', $_str);

        // 生成Sign之前完整字符串
        $_str = sprintf('%s#%s', $_str, self::getKey());
        self::logRecord('[SIGN加密前：]'.$_str,'debug');
        $_sign = strtoupper(md5($_str));
        self::logRecord('[SIGN加密后：]'.$_sign,'debug');

        return $_sign;
    }

    /**
     * 设置生成SIGN的盐
     *
     * @param string $key
     *
     * @return void
     */
    public static function setKey($key)
    {
        self::$SIGN_KEY = $key;
    }

    /**
     * 获取SIGN生成盐
     *
     * @return string
     */
    public static function getKey()
    {
        if ( empty(self::$SIGN_KEY) )
        {
            self::$SIGN_KEY = env('ADMIN_API_KEY');
        }
        return self::$SIGN_KEY;
    }
}
