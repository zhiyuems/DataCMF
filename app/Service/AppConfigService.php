<?php
namespace App\Service;
use App\Service\BaseService AS Service;

/**
 * 服务 - 应用全局配置管理
 */
final class AppConfigService extends Service
{
    /**
     * 变量配置模型
     *
     * @var string $modelsClass
     */
    private $modelsClass = \App\Models\System\Configure::class;

    /**
     * 变量配置模型
     *
     * @var \App\Models\System\Configure $models
     */
    private $models;

    /**
     * 系统变量
     *
     * @var array $variableList
     */
    private $variableList = [];

    public function _initialize()
    {
        $this->models = new $this->modelsClass;
        $this->variableList = $this->models->getAllConfig();
    }

    /**
     * 获取变量列表
     *
     * @return array
     */
    public function getVariableList()
    {
        return $this->variableList;
    }
}
