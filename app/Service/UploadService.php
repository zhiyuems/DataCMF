<?php
namespace App\Service;
use App\Service\BaseService AS Service;
use App\Bin\Traits\LogsTrait;
use Illuminate\Http\UploadedFile;
use App\Bin\Unit\FileUploadUnit;

/**
 * 服务 - 文件上传
 */
class UploadService extends Service
{
    use LogsTrait;

    /**
     * 上传处理组件
     *
     * @var \App\Bin\Unit\FileUploadUnit $_uploadUnit
     */
    private $_uploadUnit;

    public function _initialize()
    {
        $this->_uploadUnit = new FileUploadUnit;
    }

    /**
     * 设置当前上传类型
     *
     * @param string $type
     *
     * @return void
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function setCurrentType(string $type)
    {
        $this->_uploadUnit->setCurrentType($type);
    }

    /**
     * 执行文件上传
     *
     * @param UploadedFile $file
     */
    public function upload(UploadedFile $file)
    {
        if ( $file instanceof \Illuminate\Http\UploadedFile ){
            $_models = $this->_uploadUnit->getModel();
            $_modelsFieldMap = $this->_uploadUnit->getStorageModelFieldMap();
            $_fileInfo = $this->_uploadUnit->upload($file)->getSaveFileInfo();

            if ( isset($_fileInfo['is_cache']) &&
                $_fileInfo['is_cache']===true ) {
                return $_fileInfo;
            }else{
                // 缓存保险 - 查询数据库中是否存在
                if( $this->_uploadUnit->fileExist($_fileInfo['local_path'], $_fileInfo['file_hash'], true) ){
                    return $_fileInfo;
                }
            }

            // 存储资源到数据库
            $_data = [];
            array_walk($_modelsFieldMap,
                function($fileName, $mFileName) use (&$_data, $_fileInfo) {
                    $_data[$mFileName] = $_fileInfo[$fileName];
            });
            if ( !$_models->create($_data) ){
                $this->_uploadUnit->deleteFile($_fileInfo['local_path'], $_fileInfo['file_hash']);
                abort(apiFailResponse('FILE_UPLOAD_FAIL',null,'ERROR_BUSINESS_PROCESSING_FAILED'));
            }

            return $_fileInfo;
        }
        abort(apiFailResponse('INVALID_PARAMETER',null,'ERROR_ILLEGAL_PARAMETER'));
    }

}
