<?php
namespace App\Listeners;
use App\Bin\Traits\LogsTrait;
use App\Bin\Enum\LogsLevel AS LogsLevelEnum;
use Illuminate\Database\Events\QueryExecuted;

class SqlQueryListener
{
    use LogsTrait;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \Illuminate\Database\Events\QueryExecuted  $event
     * @return void
     */
    public function handle(QueryExecuted $event)
    {
        $_debug = (bool) env('APP_DEBUG', false) === true;
        try {
            if ($_debug === true) {
                foreach ($event->bindings as $i => $binding) {
                    if ($binding instanceof \DateTime) {
                        $event->bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
                    } else {
                        if (is_string($binding)) {
                            $event->bindings[$i] = "'$binding'";
                        }
                    }
                }
                // $sql = str_replace("?", "'%s'", $event->sql);
                // 参考：https://blog.csdn.net/weixin_43674113/article/details/90446250
                // 调整为如下替换方案：
                $sql = str_replace(array('%', '?'), array('%%', '%s'), $event->sql);
                $log = vsprintf($sql, $event->bindings);
                $log = '[ RunTime:' . $event->time . 'ms ]：'.$log;
                self::logRecord($log, LogsLevelEnum::DEBUG);
            }
        }catch (\Exception $e){
            $log = sprintf('[SqlQueryListener] %s',$e->getMessage());
            if ($_debug === true) {
                throw new \Exception($log);
            }else{
                self::logRecord($log, LogsLevelEnum::ERROR);
            }
        }
    }
}
