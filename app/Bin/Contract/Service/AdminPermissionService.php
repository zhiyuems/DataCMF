<?php
namespace App\Bin\Contract\Service;
use Illuminate\Http\Request;

/**
 * 管理后台权限服务接口
 */
interface AdminPermissionService
{

    /**
     * 设置GuardName看守者
     *
     * @param  string $guardName
     *
     * @return void
     */
    public function setGuardName(string $guardName);
    public function getGuardName();

    /**
     * 设置Guard模块ID
     *
     * @param  int $moduleId
     *
     * @return void
     */
    public function setGuardModuleId(int $moduleId);
    public function getGuardModuleId();

    /**
     * 设置Request类
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function setRequest(Request $request);
    public function getRequest();

    /**
     * 设置Models类
     *
     * @param \App\Models\BaseModels $models
     *
     * @return void
     */
    public function setModel($models);
    public function getModel();

    /**
     * 创建数据信息
     *
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Model|$this
     */
    public function create(array $data);

    /**
     * 删除数据信息
     *
     * @param int $id
     *
     * @return bool
     */
    public function delete($id);

    /**
     * 更新指定数据信息
     *
     * @param array $data 更新数据
     * @param int   $id   ID
     *
     * @return bool
     */
    public function update($data, $id);

    /**
     * 获取指定数据信息
     *
     * @param int $id ID
     *
     * @return false|array
     */
    public function single($id);

    /**
     * 获取数据列表
     *
     * @return array
     */
    public function allList();

    /**
     * 指定数据信息是否存在
     *
     * @param int $id ID
     *
     * @return bool
     */
    public function isExists($id);

}
