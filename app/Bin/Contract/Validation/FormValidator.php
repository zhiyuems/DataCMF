<?php
namespace App\Bin\Contract\Validation;

/**
 * 表单验证接口类
 */
interface FormValidator
{
    /**
     * 预设验证场景
     *
     * @return array
     */
    public function scene();

    /**
     * 获取验证错误的自定义属性
     *
     * @return array
     */
    public function attributes();

    /**
     * 获取已定义验证规则的错误消息
     *
     * @return array
     */
    public function messages();

    /**
     * 应用于请求的验证规则
     *
     * @return array
     */
    public function rules();
}
