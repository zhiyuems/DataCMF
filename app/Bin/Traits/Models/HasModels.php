<?php
namespace App\Bin\Traits\Models;

trait HasModels
{
    /**
     * 获取指定信息
     *
     * @param int $id 主键ID
     *
     * @return \Illuminate\Database\Eloquent\Model|object|static|null
     */
    public function oneInfo($id)
    {
        return $this->where($this->getKeyName(), $id)
            ->first();
    }
}
