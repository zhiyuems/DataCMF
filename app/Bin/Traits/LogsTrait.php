<?php
namespace App\Bin\Traits;
use Illuminate\Support\Facades\Log;

/**
 * Trait Logs
 *
 * @version  1.0.0
 * @author   Yunuo <ciwdream@gmail.com>
 */
trait LogsTrait
{

    /**
     * 提供日志快捷记录
     *
     * @access  protected
     * @version 1.0.0
     *
     * @param string $message 日志信息内容
     * @param string $level   日志级别: emergency、alert、 critical、 error、 warning、 notice、 info 和 debug
     * @param string $context 上下文信息
     *
     * @return \Psr\Log\LoggerInterface
     */
    public static function logRecord($message, $level='info', $context = [])
    {
        return Log::$level($message,$context);
    }
}
