<?php
namespace App\Bin\Traits\Permission;

trait HasModels
{
    public $guard_name;
    public $teams_id;

    public function guardName()
    {
        return $this->guard_name;
    }

    /**
     * 附加条件 - 权限认证看守者名称
     *
     * @param  string $guardName
     *
     * @return \Illuminate\Database\Eloquent\Model|$this
     */
    public function whereGuardName($guardName=null)
    {
        if (empty($guardName))return $this;
        return $this->where('guard_name', $guardName);
    }

    /**
     * 附加条件 - 权限认证模块标识
     *
     * @param  int $teamId
     *
     * @return \Illuminate\Database\Eloquent\Model|$this
     */
    public function whereTeamId($teamId=null)
    {
        if (!empty($teamId) && (bool)config('permission.teams') === true) {
            return $this->where(config('permission.column_names.team_foreign_key'), $teamId);
        }
        return $this;
    }
}
