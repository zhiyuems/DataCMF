<?php
namespace App\Bin\Traits\Sanctum;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens AS SanctumHasApiTokens;
use Laravel\Sanctum\NewAccessToken;
use ToolLibrary\Unit\Tools;

trait HasApiTokens
{
    use SanctumHasApiTokens;

    /**
     * 为用户创建新的个人访问令牌
     *
     * @param  string  $name      [可选]别名Token，默认生成UUID
     * @param  array  $abilities  [可选，预留]权限能力控制，默认['*']
     * @return \Laravel\Sanctum\NewAccessToken
     */
    public function createToken(string $name=null, array $abilities = ['*'])
    {
        $token = $this->tokens()->create([
            'name' => empty($name)?Tools::createUuid():$name,
            'token' => hash('sha256', $plainTextToken = Str::random(40)),
            'abilities' => $abilities,
        ]);

        return new NewAccessToken($token, $token->getKey().'|'.$plainTextToken);
    }
}
