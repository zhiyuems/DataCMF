<?php
namespace App\Bin\Enum;
use App\Bin\Enum\BaseEnum AS Enum;

/**
 * 日志级别枚举数据
 */
class LogsLevel extends Enum
{
    /**
     * 突发事件
     * @var string EMERGENCY
     */
    const EMERGENCY = 'emergency';

    /**
     * 警惕
     * @var string ALERT
     */
    const ALERT = 'alert';

    /**
     * 严重
     * @var string CRITICAL
     */
    const CRITICAL = 'critical';

    /**
     * 错误
     * @var string ERROR
     */
    const ERROR = 'error';

    /**
     * 警告
     * @var string WARNING
     */
    const WARNING = 'warning';

    /**
     * 通知
     * @var string NOTICE
     */
    const NOTICE = 'notice';

    /**
     * 信息
     * @var string INFO
     */
    const INFO = 'info';

    /**
     * 调试
     * @var string DEBUG
     */
    const DEBUG = 'debug';

    public static function data()
    {
        return [
            self::EMERGENCY => [
                'name' => '突发',
                'value' => self::EMERGENCY
            ],
            self::ALERT => [
                'name' => '警惕',
                'value' => self::ALERT
            ],
            self::CRITICAL => [
                'name' => '严重',
                'value' => self::CRITICAL
            ],
            self::ERROR => [
                'name' => '错误',
                'value' => self::ERROR
            ],
            self::WARNING => [
                'name' => '警告',
                'value' => self::WARNING
            ],
            self::NOTICE => [
                'name' => '通知',
                'value' => self::NOTICE
            ],
            self::INFO => [
                'name' => '信息',
                'value' => self::INFO
            ],
            self::DEBUG => [
                'name' => '调试',
                'value' => self::DEBUG
            ],
        ];
    }
}
