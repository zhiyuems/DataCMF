<?php
namespace App\Bin\Enum;
use App\Bin\Enum\BaseEnum AS Enum;

class UserInfo extends Enum
{
    /**
     * 使用场景：男性
     * @var string 性别
     */
    const SEX_MAN = 'M';

    /**
     * 使用场景：女性
     * @var string 性别
     */
    const SEX_FEMALE = 'F';

    /**
     * 使用场景：未知
     * @var string 性别
     */
    const SEX_UNKNOWN = 'U';

    public static function data() {
        return [
            self::SEX_MAN => [
                'name' => __('term.sex.man'),
                'value' => self::SEX_MAN
            ],
            self::SEX_FEMALE => [
                'name' => __('term.sex.female'),
                'value' => self::SEX_FEMALE
            ],
            self::SEX_UNKNOWN => [
                'name' => __('term.sex.unknown'),
                'value' => self::SEX_UNKNOWN
            ],
        ];
    }
}
