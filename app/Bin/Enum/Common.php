<?php
namespace App\Bin\Enum;
use App\Bin\Enum\BaseEnum AS Enum;

/**
 * 通用枚举数据
 */
class Common extends Enum
{
    /**
     * 使用场景：状态开启/可用
     * @var string 状态开启
     */
    const STATUS_OPEN = 'Y';

    /**
     * 使用场景：状态关闭/不可用
     * @var string 状态关闭
     */
    const STATUS_CLOSE = 'N';


    public static function data()
    {
        return [
            self::STATUS_OPEN => [
                'name' => '开启',
                'value' => self::STATUS_OPEN
            ],
            self::STATUS_CLOSE => [
                'name' => '关闭',
                'value' => self::STATUS_CLOSE
            ],
        ];
    }
}
