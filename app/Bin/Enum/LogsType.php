<?php
namespace App\Bin\Enum;
use App\Bin\Enum\BaseEnum AS Enum;

/**
 * 日志类型枚举数据
 */
class LogsType extends Enum
{
    /**
     * 操作日志
     * @var string OPERATION
     */
    const OPERATION = 'operation';

    /**
     * 登录
     * @var string LOGIN
     */
    const LOGIN = 'login';

    /**
     * SQL日志
     * @var string SQL
     */
    const SQL = 'sql';

    public static function data()
    {
        return [
            self::OPERATION => [
                'name' => '系统日志',
                'value' => self::OPERATION
            ],
            self::LOGIN => [
                'name' => '登录日志',
                'value' => self::LOGIN
            ],
            self::SQL => [
                'name' => 'SQL日志',
                'value' => self::SQL
            ],
        ];
    }
}
