<?php
namespace App\Bin\Enum;
use App\Bin\Enum\BaseEnum AS Enum;

/**
 * API公共错误码
 *
 * - 2xxx 系统错误码
 * - 4xxx 业务错误码
 */
final class ApiCode extends Enum
{
    /**
     * 使用场景：接口调用成功
     * @var int 接口调用成功
     */
    const ERROR_SUCCESS = 10000;

    /**
     * 使用场景：服务暂不可用（业务系统不可用）
     * @var int 服务不可用
     */
    const ERROR_SERVICE_UNAVAILABLE = 20000;

    /**
     * 使用场景：无效的访问令牌、访问令牌已过期
     * @var int 授权权限不足
     */
    const ERROR_INSUFFICIENT_AUTHORIZATION = 20001;

    /**
     * 使用场景：缺少方法名参数、签名参数
     * @var int 缺少必选参数
     */
    const ERROR_MISSING_REQUIRED_ARGUMENTS = 40001;

    /**
     * 使用场景：参数无效
     * @var int 非法的参数
     */
    const ERROR_ILLEGAL_PARAMETER = 40002;

    /**
     * 使用场景：业务处理失败
     * @var int 业务处理失败
     */
    const ERROR_BUSINESS_PROCESSING_FAILED = 40004;

    /**
     * 使用场景：用户权限不足
     * @var int 权限不足
     */
    const ERROR_INSUFFICIENT_PERMISSIONS = 40006;

    public static function data() {
        return [
            self::ERROR_SUCCESS => [
                'name' => __('api_code.error_success'),
                'value' => self::ERROR_SUCCESS
            ],
            self::ERROR_SERVICE_UNAVAILABLE => [
                'name' => __('api_code.error_service_unavailable'),
                'value' => self::ERROR_SERVICE_UNAVAILABLE
            ],
            self::ERROR_INSUFFICIENT_AUTHORIZATION => [
                'name' => __('api_code.error_insufficient_authorization'),
                'value' => self::ERROR_INSUFFICIENT_AUTHORIZATION
            ],
            self::ERROR_MISSING_REQUIRED_ARGUMENTS => [
                'name' => __('api_code.error_missing_required_arguments'),
                'value' => self::ERROR_MISSING_REQUIRED_ARGUMENTS
            ],
            self::ERROR_ILLEGAL_PARAMETER => [
                'name' => __('api_code.error_illegal_parameter'),
                'value' => self::ERROR_ILLEGAL_PARAMETER
            ],
            self::ERROR_BUSINESS_PROCESSING_FAILED => [
                'name' => __('api_code.error_business_processing_failed'),
                'value' => self::ERROR_BUSINESS_PROCESSING_FAILED
            ],
            self::ERROR_INSUFFICIENT_PERMISSIONS => [
                'name' => __('api_code.error_insufficient_permissions'),
                'value' => self::ERROR_INSUFFICIENT_PERMISSIONS
            ],
        ];
    }

}
