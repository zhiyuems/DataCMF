<?php
namespace App\Bin\Enum;
use App\Bin\Enum\BaseEnum AS Enum;

/**
 * 菜單枚举数据
 */
class Menu extends Enum
{
    /**
     * 使用场景：catalogue目录
     * @var string 菜单类型
     */
    const TYPE_CATALOGUE = 'catalogue';

    /**
     * 使用场景：menu菜单
     * @var string 菜单类型
     */
    const TYPE_MENU = 'menu';

    /**
     * 使用场景：iframe内嵌页面
     * @var string 菜单类型
     */
    const TYPE_IFRAME = 'iframe';

    /**
     * 使用场景：link外链
     * @var string 菜单类型
     */
    const TYPE_LINK = 'link';

    /**
     * 使用场景：button按钮
     * @var string 菜单类型
     */
    const TYPE_BUTTON = 'button';

    public static function data()
    {
        return [
            self::TYPE_CATALOGUE => [
                'name' => '目录',
                'value' => self::TYPE_CATALOGUE
            ],
            self::TYPE_MENU => [
                'name' => '菜单',
                'value' => self::TYPE_MENU
            ],
            self::TYPE_IFRAME => [
                'name' => 'Iframe',
                'value' => self::TYPE_IFRAME
            ],
            self::TYPE_LINK => [
                'name' => '外链',
                'value' => self::TYPE_LINK
            ],
            self::TYPE_BUTTON => [
                'name' => '按钮',
                'value' => self::TYPE_BUTTON
            ],
        ];
    }
}
