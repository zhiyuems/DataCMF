<?php
namespace App\Bin\Enum;
use App\Bin\Enum\BaseEnum AS Enum;

/**
 * API公共子错误码
 */
final class ApiSubCode extends Enum
{
    /**
     * 使用场景：服务不可用
     * @var int 服务暂不可用
     */
    const UNKNOW_ERROR = 'unknow-error';

    /**
     * 使用场景：无效的访问令牌
     * @var int 授权权限不足
     */
    const INVALID_AUTH_TOKEN = 'invalid-auth-token';

    /**
     * 使用场景：访问令牌已过期
     * @var int 授权权限不足
     */
    const INVALID_TOKEN_TIME_OUT = 'auth-token-time-out';

    /**
     * 使用场景：缺少签名参数
     * @var int 缺少必选参数
     */
    const MISSING_SIGNATURE = 'missing-signature';

    /**
     * 使用场景：缺少参数
     * @var int 缺少必选参数
     */
    const MISSING_PARAMETER = 'missing-parameter';

    /**
     * 使用场景：参数无效
     * @var int 非法的参数
     */
    const INVALID_PARAMETER = 'invalid-parameter';

    /**
     * 使用场景：文件上传失败
     * @var int 非法的参数
     */
    const FILE_UPLOAD_FAIL = 'upload-fail';

    /**
     * 使用场景：文件扩展名无效
     * @var int 非法的参数
     */
    const FILE_FAIL_EXTENSION = 'invalid-file-extension';

    /**
     * 使用场景：无效文件
     * @var int 非法的参数
     */
    const FILE_INVALID = 'invalid-file';

    /**
     * 使用场景：文件大小无效
     * @var int 非法的参数
     */
    const FILE_FAIL_SIZE = 'invalid-file-size';

    /**
     * 使用场景：上传文件类型无效
     * @var int 非法的参数
     */
    const FILE_FAIL_TYPE = 'invalid-file-type';

    /**
     * 使用场景：无效签名
     * @var int 非法的参数
     */
    const INVALID_SIGNATURE = 'invalid-signature';

    /**
     * 使用场景：无效令牌
     * @var int 非法的参数
     */
    const INVALID_TOEKN = 'invalid-token';

    /**
     * 使用场景：数据创建失败
     * @var int 业务处理失败
     */
    const DATA_CREATION_FAILED = 'creation-failed';

    /**
     * 使用场景：数据更新失败
     * @var int 业务处理失败
     */
    const DATA_UPDATE_FAILED = 'update-failed';

    /**
     * 使用场景：数据删除失败
     * @var int 业务处理失败
     */
    const DATA_DELETE_FAILED = 'delete-failed';

    // Passport Code

    /**
     * 使用场景：登录账号或密码填写信息错误
     * @var int 业务处理失败
     */
    const INVALID_LOGIN_INFO = 'invalid-login-information';

    /**
     * 使用场景：退出登录失败
     * @var int 业务处理失败
     */
    const PASSPORT_LOGOUT_FAIL = 'logout-fail';

    public static function data() {
        return [
            self::UNKNOW_ERROR => [
                'name' => __('api_code.sub.unknow_error'),
                'value' => self::UNKNOW_ERROR
            ],
            self::INVALID_AUTH_TOKEN => [
                'name' => __('api_code.sub.invalid_auth_token'),
                'value' => self::INVALID_AUTH_TOKEN
            ],
            self::INVALID_TOKEN_TIME_OUT => [
                'name' => __('api_code.sub.invalid_token_time_out'),
                'value' => self::INVALID_TOKEN_TIME_OUT
            ],
            self::MISSING_SIGNATURE => [
                'name' => __('api_code.sub.missing_signature'),
                'value' => self::MISSING_SIGNATURE
            ],
            self::MISSING_PARAMETER => [
                'name' => __('api_code.sub.missing_parameter'),
                'value' => self::MISSING_PARAMETER
            ],
            self::INVALID_PARAMETER => [
                'name' => __('api_code.sub.invalid_parameter'),
                'value' => self::INVALID_PARAMETER
            ],
            self::FILE_UPLOAD_FAIL => [
                'name' => __('api_code.sub.file_upload_fail'),
                'value' => self::FILE_UPLOAD_FAIL
            ],
            self::FILE_INVALID => [
                'name' => __('api_code.sub.file_invalid'),
                'value' => self::FILE_INVALID,
            ],
            self::FILE_FAIL_EXTENSION => [
                'name' => __('api_code.sub.file_fail_extension'),
                'value' => self::FILE_FAIL_EXTENSION
            ],
            self::FILE_FAIL_SIZE => [
                'name' => __('api_code.sub.file_fail_size'),
                'value' => self::FILE_FAIL_SIZE
            ],
            self::FILE_FAIL_TYPE => [
                'name' => __('api_code.sub.file_fail_type'),
                'value' => self::FILE_FAIL_TYPE
            ],
            self::INVALID_SIGNATURE => [
                'name' => __('api_code.sub.invalid_signature'),
                'value' => self::INVALID_SIGNATURE
            ],
            self::INVALID_TOEKN => [
                'name' => __('api_code.sub.invalid_toekn'),
                'value' => self::INVALID_TOEKN
            ],
            self::DATA_CREATION_FAILED => [
                'name' => __('api_code.sub.data_creation_failed'),
                'value' => self::DATA_CREATION_FAILED
            ],
            self::DATA_UPDATE_FAILED => [
                'name' => __('api_code.sub.data_update_failed'),
                'value' => self::DATA_UPDATE_FAILED
            ],
            self::DATA_DELETE_FAILED => [
                'name' => __('api_code.sub.data_delete_failed'),
                'value' => self::DATA_DELETE_FAILED
            ],
            self::INVALID_LOGIN_INFO => [
                'name' => __('api_code.sub.invalid_login_info'),
                'value' => self::INVALID_LOGIN_INFO
            ],
            self::PASSPORT_LOGOUT_FAIL => [
                'name' => __('api_code.sub.passport_logout_fail'),
                'value' => self::PASSPORT_LOGOUT_FAIL
            ],
        ];
    }
}
