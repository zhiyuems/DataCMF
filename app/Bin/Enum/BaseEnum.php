<?php
namespace App\Bin\Enum;
use MyCLabs\Enum\Enum;

/**
 * 枚举类基类
 */
abstract class BaseEnum extends Enum
{
    /**
     * 获取枚举扩展信息
     *
     * @param mixed $value
     * @param string|array|int|null $key
     *
     * @return array|mixed
     */
    public static function getMsg($value, $key=null, $default = null)
    {
        try {
            $_map = static::data();
            if (isset($_map[$value]) && is_array($_map[$value]))
            {
                if (empty($key))return $_map[$value];
                return data_get($_map[$value], $key, $default);
            }
        } catch (\Exception $e) {
            //
            report($e);
            return $default;
        }
        return $value;
    }

    /**
     * 通过Key返回值
     *
     * @param string $key     要搜索的key
     * @param mixed  $default 当key不存在时返回
     *
     * @return int|mixed
     */
    public static function searchKey($key, $default = null)
    {
        if ( self::isValidKey($key) !== true ) {
            return $default;
        }

        return data_get(self::toArray(), $key, $default);
    }
}
