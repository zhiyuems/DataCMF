<?php
namespace App\Bin\Unit;
use App\Bin\Traits\LogsTrait;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;

/**
 * 组件 - 文件上传统一处理组件
 */
class FileUploadUnit
{
    use LogsTrait;

    protected $_currentType = 'image';

    /**
     * 允许上传文件大小(字节)
     *
     * @var int $_allowSize
     */
    protected $_allowSize = 1048576;

    /**
     * 允许上传文件类型
     *
     * @var array $_allowType
     */
    protected $_allowType = ['image','file'];

    /**
     * 文件存储驱动
     *
     * @var \Illuminate\Support\Facades\Storage|\Illuminate\Filesystem\FilesystemAdapter $_storage
     */
    protected $_storage;

    /**
     * 使用文件存储驱动
     *
     * @var string $_storageDisk
     */
    protected $_storageDisk = 'upload';

    /**
     * 文件存储路径
     *
     * @var string $_storagePath
     */
    protected $_storagePath = '';

    /**
     * 文件HASH缓存驱动
     *
     * @var \Illuminate\Support\Facades\Cache|\Illuminate\Contracts\Cache\Repository  $_cache
     */
    protected $_cache;

    /**
     * 资源模型
     *
     * @var \App\Models\ResourcesImage $_models
     */
    protected $_models;

    /**
     * 资源模型MAP
     *
     * @var array $_modelsMap
     */
    protected $_modelsMap = [
        'image' => \App\Models\ResourcesImage::class,
    ];

    /**
     * 模型存储字段MAP
     *
     * @var array $_storageModelMap
     */
    protected $_storageModelMap = [
        'image' => [
            'file_name' => 'file_name',
            'file_path' => 'file_path',
            'file_root_path' => 'local_path',
            'file_size' => 'size',
            'file_suffix' => 'extension',
            'file_mime' => 'mime',
            'file_hash' => 'file_hash',
        ]
    ];

    /**
     * 上传文件信息
     *
     * @var array $_fileInfo
     */
    protected $_fileInfo = [];

    /**
     * 构造方法 __construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->_cache = Cache::store(env('UPLOAD_CACHE_STORE','file'));
        $this->_storage = Storage::disk($this->_storageDisk);
    }

    /**
     * 设置当前上传类型
     *
     * @param string $type
     *
     * @return void
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function setCurrentType(string $type)
    {
        if (!in_array($type, $this->_allowType)){
            abort(apiFailResponse('FILE_FAIL_TYPE',null,'ERROR_ILLEGAL_PARAMETER'));
        }

        $this->_currentType = $type;
    }

    /**
     * 设置文件存储驱动
     *
     * @param string $disk
     *
     * @return void
     */
    public function setStorageDisk(string $disk)
    {
        $this->_storageDisk = $disk;
        $this->_storage = Storage::disk($this->_storageDisk);
    }

    /**
     * 设置文件存储路径
     *
     * @param string $path
     *
     * @return void
     */
    public function setStoragePath(string $path)
    {
        $this->_storagePath = $path;
    }

    /**
     * 判断文件是否存在
     *
     * @param string $path
     * @param string $hash
     *
     * @return bool
     */
    public function fileExist($path, $hash, $isCache=false)
    {
        $_isExist = true;

        // 判断本地文件是否存在
        if ( !file_exists($path) ){
            $_isExist = false;
            if ($this->_models->existHash($hash)){
                $this->_models->deleteFileOrHash($path, $hash);
            }
        }else{
            // 若模型中HASH不存在，删除本地文件
            if (!$this->_models->existHash($hash)){
                $this->deleteFile($path);
                $_isExist = false;
            }
        }

        if ($_isExist){
            // 重置缓存
            $_cacheName = $this->generateCacheName($hash);
            if($isCache)$this->_cache->put($_cacheName, json_encode($this->_fileInfo));
        }

        return $_isExist;
    }

    /**
     * 执行文件上传
     *
     * @param UploadedFile $file
     */
    public function upload(UploadedFile $file)
    {
        // 文件HASH
        $this->_fileInfo['file_hash'] = hash_file('md5', $file->getRealPath());

        $_cacheName = $this->generateCacheName($this->_fileInfo['file_hash']);
//        $this->_cache->delete($_cacheName);
        if ( $this->_cache->has($_cacheName) ){
            $this->_fileInfo = $this->_cache->get($_cacheName,'[]');
            $this->_fileInfo = json_decode($this->_fileInfo, true);

            $this->_fileInfo['file_url'] = $this->_storage->url($this->_fileInfo['file_path']);
            $this->_fileInfo['is_cache'] = true;
        }else{
            $this->checkingFile($file);

            // 原始文件名
            $this->_fileInfo['original_name'] = $file->getClientOriginalName();

            // 文件扩展名
            $this->_fileInfo['extension'] = $file->getClientOriginalExtension();

            // 文件Mime类型
            $this->_fileInfo['size'] = filesize($file->getRealPath());

            // 文件大小
            $this->_fileInfo['mime'] = $file->getClientMimeType();

            // 生成新的统一格式的文件名
            $this->_fileInfo['file_name'] = sprintf('%s.%s', $this->generateFileName($this->_fileInfo['original_name']), $this->_fileInfo['extension']);

            // 本地存储路径(绝对路径)、文件存储路径(相对)、url访问地址
            [
                'local_path' => $this->_fileInfo['local_path'],
                'path' => $this->_fileInfo['file_path'],
                'url'  => $this->_fileInfo['file_url'],
            ] = $this->storageFile($this->generateFileDirPath(), $file);

            $this->_cache->put($_cacheName, json_encode($this->_fileInfo));
        }
        return $this;
    }

    /**
     * 获取上传文件信息
     *
     * @return array
     */
    public function getSaveFileInfo()
    {
        return $this->_fileInfo;
    }

    /**
     * 获取存储模型字段映射信息
     *
     * @return array
     */
    public function getStorageModelFieldMap()
    {
        return $this->_storageModelMap[$this->_currentType];
    }

    /**
     * 存储文件
     *
     * @param string $savePath 存储路径
     * @param \Illuminate\Http\File|\Illuminate\Http\UploadedFile|string $file
     *
     * @return array
     * @return array.local_path 本地存储路径
     * @return array.path 存储相对路径
     * @return array.url  WEB访问路径
     */
    public function storageFile($savePath, $file)
    {
        $_path = $this->_storage->putFileAs($savePath, $file, $this->_fileInfo['file_name']);
        $_localPath = sprintf('%s/%s',config('filesystems.disks.upload.root'), $_path);
        return [
            'local_path' => $_localPath,
            'path' => $_path,
            'url'  => $this->_storage->url($_path)
        ];
    }

    /**
     * 删除文件
     *
     * @param string $filePath 删除文件路径
     *
     * @return bool
     */
    public function deleteFile($filePath, $hash=null)
    {
        if (file_exists($filePath)) {
            $_isDel = Storage::delete($filePath);
        }else{
            $_isDel = $this->_storage->delete($filePath);
        }
        if ( $_isDel === false
            && file_exists($filePath) ) {
            $_isDel = @unlink($filePath);
        }

        if ( $_isDel && !empty($hash) ) {
            $this->_cache->delete($this->generateCacheName($hash));
        }
        return $_isDel;
    }

    /**
     * 检查上传文件
     *
     * @param UploadedFile $file
     *
     * @return bool
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function checkingFile(UploadedFile $file)
    {
        $_isPast = true;
        // 验证文件上传有效性
        if (!$file->isValid()) {
            abort(apiFailResponse('FILE_INVALID',null,'ERROR_ILLEGAL_PARAMETER'));
        }

        // 根据上传类型，进行相应数据校验
        switch ($this->_currentType){
            case 'image':
                $this->isImage($file->getRealPath());
                break;
        }
        return $_isPast;
    }

    /**
     * 检测是否为真实图片格式
     *
     * @param string $filePath 文件绝对路径
     *
     * @return bool
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function isImage($filePath)
    {
        $_isImage  = false;
        $_fileInfo = getimagesize($filePath);

        if ( function_exists('exif_imagetype') ) { // 需要GD库支持
            $_isImage = exif_imagetype($filePath) === $_fileInfo[2];
        }else{
            $_file = fopen($filePath,'rb');
            $_bin  = fread($_file, 2);
            fclose($_file);
            $strInfo  = @unpack('C2chars', $_bin);
            $typeCode = intval($strInfo['chars1'].$strInfo['chars2']);
            // jpg，jpeg，png，bmp，gif，svg，或 webp
            $fileType = null;
            switch ($typeCode){
                case 255216:
                    $fileType = 'jpg';break;
                case 13788:
                    $fileType = 'png';break;
                case 6677:
                    $fileType = 'bmp';break;
                case 7173:
                    $fileType = 'gif';break;
            }
            $_isImage = !(is_null($fileType) === true);
        }
        if ( $_isImage === false ){
            abort(apiFailResponse('FILE_INVALID',null,'ERROR_ILLEGAL_PARAMETER'));
        }
        return true;
    }

    /**
     * 生成新的文件名称
     *
     * @param string $name
     *
     * @return string
     */
    public function generateFileName($name)
    {
        return md5($name . time() . mt_rand(1, 10000));
    }

    /**
     * 生成新的文件目录名称
     *
     * @return string
     */
    public function generateFileDirPath()
    {
        return sprintf('%s/%s', $this->_currentType, date('Ymd'));
    }

    /**
     * 生成缓存KEY名称
     *
     * @param string $key
     *
     * @return string
     */
    public function generateCacheName($key, $type='HASH')
    {
        return sprintf('UPLOAD:%s:%s', $type, $key);
    }

    /**
     * 获取资源模型
     *
     * @return \App\Models\ResourcesImage;
     */
    public function getModel()
    {
        return $this->_models = new $this->_modelsMap[$this->_currentType];
    }
}
