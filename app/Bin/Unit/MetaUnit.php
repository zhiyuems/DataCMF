<?php
namespace App\Bin\Unit;
/**
 * 组件 - Meta字段数据处理
 */

class MetaUnit
{
    /**
     * 格式化 - 聚合Meta字段数据
     *
     * @param array  $list  待处理数据
     *
     * @return array
     */
    public static function gather($list)
    {
        foreach ($list AS $i=>$row)
        {
            $_row  = [];
            $_meta = [];
            $_metaIndex = null;
            foreach ($row AS $fieldName=>$fieldValue)
            {
                if (strpos($fieldName,'meta_') === 0){
                    if(empty($_metaIndex)){
                        $_row['meta'] = [];
                        $_metaIndex = $fieldName;
                    }
                    $_meta[substr($fieldName, 5)] = $fieldValue;
                }else{
                    $_row[$fieldName] = $fieldValue;
                }
            }
            $_row['meta'] = $_meta;
            if (isset($_row['guard_name']))unset($_row['guard_name']);
            $list[$i] = $_row;
        }
        return $list;
    }

    /**
     * 格式化 - 水平Meta字段数据
     *
     * @param array  $list  待处理数据
     *
     * @return array
     */
    public static function flat($list)
    {
        if (!empty($list['meta']))
        {
            foreach ($list['meta'] AS $fieldName=>$value)
            {
                $list[sprintf('meta_%s', $fieldName)] = $value;
            }
            unset($list['meta']);
        }
        return $list;
    }
}
