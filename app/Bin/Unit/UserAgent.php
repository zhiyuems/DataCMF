<?php
namespace App\Bin\Unit;

class UserAgent
{
    /**
     * 原始UserAgent文本
     * @var string $uaText
     */
    private static $uaText;

    /**
     * 操作系统类型
     * @var string $osType
     */
    private static $osType;

    /**
     * 操作系统版本
     * @var string $osVersion
     */
    private static $osVersion;

    /**
     * 浏览器类型
     * @var string $browserType
     */
    private static $browserType;

    /**
     * 浏览器版本
     * @var string $browserVersion
     */
    private static $browserVersion;

    /**
     * 设置原始UserAgent文本
     * @param  string $uaText
     */
    public static function setUaText($text)
    {
        self::$uaText = $text;
    }

    /**
     * 获取UA中的操作系统信息
     *
     * @return string|false
     */
    public static function getOsPlatform()
    {
        $os = false;
        if (preg_match('/win/i', self::$uaText) && strpos(self::$uaText, '95')) {
            $os = 'Windows 95';
        } else if (preg_match('/win 9x/i', self::$uaText) && strpos(self::$uaText, '4.90')) {
            $os = 'Windows ME';
        } else if (preg_match('/win/i', self::$uaText) && preg_match('/98/i', self::$uaText)) {
            $os = 'Windows 98';
        } else if (preg_match('/win/i', self::$uaText) && preg_match('/nt 6.0/i', self::$uaText)) {
            $os = 'Windows Vista';
        } else if (preg_match('/win/i', self::$uaText) && preg_match('/nt 6.1/i', self::$uaText)) {
            $os = 'Windows 7';
        } else if (preg_match('/win/i', self::$uaText) && preg_match('/nt 6.2/i', self::$uaText)) {
            $os = 'Windows 8';
        } else if (preg_match('/win/i', self::$uaText) && preg_match('/nt 10.0/i', self::$uaText)) {
            $os = 'Windows 10';#添加win10判断
        } else if (preg_match('/win/i', self::$uaText) && preg_match('/nt 5.1/i', self::$uaText)) {
            $os = 'Windows XP';
        } else if (preg_match('/win/i', self::$uaText) && preg_match('/nt 5/i', self::$uaText)) {
            $os = 'Windows 2000';
        } else if (preg_match('/win/i', self::$uaText) && preg_match('/nt/i', self::$uaText)) {
            $os = 'Windows NT';
        } else if (preg_match('/win/i', self::$uaText) && preg_match('/32/i', self::$uaText)) {
            $os = 'Windows 32';
        } else if (preg_match('/linux/i', self::$uaText)) {
            $os = 'Linux';
        } else if (preg_match('/unix/i', self::$uaText)) {
            $os = 'Unix';
        } else if (preg_match('/sun/i', self::$uaText) && preg_match('/os/i', self::$uaText)) {
            $os = 'SunOS';
        } else if (preg_match('/ibm/i', self::$uaText) && preg_match('/os/i', self::$uaText)) {
            $os = 'IBM OS/2';
        } else if (preg_match('/Mac/i', self::$uaText) && preg_match('/PC/i', self::$uaText)) {
            $os = 'Macintosh';
        } else if (preg_match('/PowerPC/i', self::$uaText)) {
            $os = 'PowerPC';
        } else if (preg_match('/AIX/i', self::$uaText)) {
            $os = 'AIX';
        } else if (preg_match('/HPUX/i', self::$uaText)) {
            $os = 'HPUX';
        } else if (preg_match('/NetBSD/i', self::$uaText)) {
            $os = 'NetBSD';
        } else if (preg_match('/BSD/i', self::$uaText)) {
            $os = 'BSD';
        } else if (preg_match('/OSF1/i', self::$uaText)) {
            $os = 'OSF1';
        } else if (preg_match('/IRIX/i', self::$uaText)) {
            $os = 'IRIX';
        } else if (preg_match('/FreeBSD/i', self::$uaText)) {
            $os = 'FreeBSD';
        } else if (preg_match('/teleport/i', self::$uaText)) {
            $os = 'teleport';
        } else if (preg_match('/flashget/i', self::$uaText)) {
            $os = 'flashget';
        } else if (preg_match('/webzip/i', self::$uaText)) {
            $os = 'webzip';
        } else if (preg_match('/offline/i', self::$uaText)) {
            $os = 'offline';
        }
        return self::$osType = $os;
    }

    /**
     * 获取UA中的浏览器信息
     *
     * @return string|false
     */
    public static function getBrowserName()
    {
        $browser = false;
        if (preg_match('#(Camino|Chimera)[ /]([a-zA-Z0-9.]+)#i', self::$uaText, $matches)) {
            self::$browserVersion = $matches[2];
            $browser = 'Camino';
        } elseif (preg_match('#SE 2([a-zA-Z0-9.]+)#i', self::$uaText, $matches)) {
            self::$browserVersion = '2'.$matches[1];
            $browser = '搜狗浏览器';
        } elseif (preg_match('#360([a-zA-Z0-9.]+)#i', self::$uaText, $matches)) {
            self::$browserVersion = $matches[1];
            $browser = '360浏览器';
        } elseif (preg_match('#Maxthon( |\/)([a-zA-Z0-9.]+)#i', self::$uaText, $matches)) {
            self::$browserVersion = $matches[1];
            $browser = 'Maxthon';
        } elseif (preg_match('#Chrome/([a-zA-Z0-9.]+)#i', self::$uaText, $matches)) {
            self::$browserVersion = $matches[1];
            $browser = 'Chrome';
        } elseif (preg_match('#XiaoMi/MiuiBrowser/([0-9.]+)#i', self::$uaText, $matches)) {
            self::$browserVersion = $matches[1];
            $browser = '小米浏览器';
        } elseif (preg_match('#Safari/([a-zA-Z0-9.]+)#i', self::$uaText, $matches)) {
            self::$browserVersion = $matches[1];
            $browser = 'Safari';
        } elseif (preg_match('#opera mini#i', self::$uaText)) {
            preg_match('#Opera/([a-zA-Z0-9.]+)#i', self::$uaText, $matches);
            self::$browserVersion = $matches[1];
            $browser = 'Opera Mini';
        } elseif (preg_match('#Opera.([a-zA-Z0-9.]+)#i', self::$uaText, $matches)) {
            self::$browserVersion = $matches[1];
            $browser = 'Opera';
        } elseif (preg_match('#TencentTraveler ([a-zA-Z0-9.]+)#i', self::$uaText, $matches)) {
            self::$browserVersion = $matches[1];
            $browser = '腾讯TT浏览器';
        } elseif (preg_match('#(UCWEB|UBrowser|UCBrowser)/([a-zA-Z0-9.]+)#i', self::$uaText, $matches)) {
            self::$browserVersion = $matches[1];
            $browser = 'UC';
        } elseif (preg_match('#Vivaldi/([a-zA-Z0-9.]+)#i', self::$uaText, $matches)) {
            self::$browserVersion = $matches[1];
            $browser = 'Vivaldi';
        } elseif (preg_match('#wp-(iphone|android)/([a-zA-Z0-9.]+)#i', self::$uaText, $matches)) {
            self::$browserVersion = $matches[1];
            $browser = 'WordPress客户端';
        } elseif (preg_match('#Edge ([a-zA-Z0-9.]+)#i', self::$uaText, $matches)) {
            self::$browserVersion = $matches[1];
            $browser = 'Edge';
        } elseif (preg_match('#MSIE ([a-zA-Z0-9.]+)#i', self::$uaText, $matches)) {
            self::$browserVersion = $matches[1];
            $browser = 'Internet Explorer';
        } elseif (preg_match('#(Firefox|Phoenix|SeaMonkey|Firebird|BonEcho|GranParadiso|Minefield|Iceweasel)/([a-zA-Z0-9.]+)#i',
            self::$uaText, $matches)) {
            self::$browserVersion = $matches[2];
            $browser = 'Firefox';
        }
        return self::$browserType = $browser;
    }

    public static function getBrowserVersion()
    {
        if (self::$browserVersion){

        }else{
            self::getBrowserName();
        }
        return self::$browserVersion;
    }
}
