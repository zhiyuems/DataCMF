<?php
/**
 * 自定义辅助函数
 */

/**
 * 获取模块应用路径
 *
 * @param  string  $path
 * @return string
 */
function modules_path($path = '')
{
    return app_path(sprintf('Modules/%s', $path));
}

/**
 * Api响应快捷调用方法
 *
 * @param array  $data
 * @param string $subCode
 * @param string $code
 *
 * @return \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory
 */
function apiResponse($data,
                     string $code=null, string $message=null,
                     string $subcode=null, string $submsg=null)
{
    $app = clone app('ApiResponse');
    $app->data($data);
    $app->code($code??'ERROR_SUCCESS', $message);
    if (!empty($subcode)){
        $app->sub($subcode, $submsg);
    }
    return $app->response();
}

/**
 * Api响应快捷调用方法 - 成功响应
 *
 * @param array|mixed  $data
 * @param string $message
 *
 * @return \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory
 */
function apiSuccessResponse($data, string $message=null)
{
    return apiResponse($data,'ERROR_SUCCESS', $message);
}

/**
 * Api响应快捷调用方法 - 失败响应
 *
 * @param string $subCode
 * @param string $subMsg
 * @param string $code
 * @param array|mixed  $data
 *
 * @return \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory
 */
function apiFailResponse($subCode, $subMsg=null,
                         $code=null, $data=[])
{
    return apiResponse($data,
        empty($code)?'ERROR_BUSINESS_PROCESSING_FAILED':$code,null,
        $subCode, $subMsg);
}

