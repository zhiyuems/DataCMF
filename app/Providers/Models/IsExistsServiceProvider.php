<?php
namespace App\Providers\Models;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Query\Builder AS queryBuilder;
use Illuminate\Database\Eloquent\Builder AS eloquentBuilder;
/**
 * Models 数据存在查询扩展
 *
 * @package Illuminate\Support\ServiceProvider
 * @package Illuminate\Database\Query\Builder
 * @package Illuminate\Database\Eloquent\Builder
 */
class IsExistsServiceProvider extends ServiceProvider
{
    public function register()
    {
        // 注册自定义query方法：isExists
        queryBuilder::macro('isExists', function(){
            // 执行逻辑
            return $this->selectRaw('(1) `exists`')->limit(1)->value('exists')?true:false;
        });

        // Eloquent ORM注册自定义query方法：toRawSql
        eloquentBuilder::macro('isExists', function(){
            return $this->getQuery()->isExists();
        });
    }
}
