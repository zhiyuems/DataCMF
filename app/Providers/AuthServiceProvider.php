<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * 应排除在授权验证之外的ability。
     *
     * @var array<int, string>
     */
    protected $abilityExcept = [
        'system.admin.myinfo',
        'system.menu.myinfo',
        'demo.link.datacmf',
        'demo.link.scuidoc',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
        Gate::before(function ($user, $ability) {
            if ( $user->hasRole(config('permission.SuperAdministrator')) ||
                in_array($ability, $this->abilityExcept) ){
                return true;
            }
            return null;
        });
    }
}
