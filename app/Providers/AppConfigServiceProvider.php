<?php
namespace App\Providers;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppConfigServiceProvider extends ServiceProvider
{
    private $_appConfig;

    /**
     * 引导应用服务
     *
     * @return void
     */
    public function boot()
    {
        if (!app()->runningInConsole()) {
            $this->_appConfig = (new \App\Service\AppConfigService)->getVariableList();
            $this->_variableRegister('View::share');
        }
    }

    private function _variableRegister($obj)
    {
        array_map(function($varInfo) use ($obj) {
            call_user_func($obj, $varInfo['variable_name'], $varInfo['variable_value']);
        }, $this->_appConfig);
    }
}
