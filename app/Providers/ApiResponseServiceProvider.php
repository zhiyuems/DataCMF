<?php
namespace App\Providers;
use Illuminate\Support\ServiceProvider;

class ApiResponseServiceProvider extends ServiceProvider
{
    /**
     * 注册 ApiResponse 服务
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('ApiResponse', function ($app) {
            return new \App\Service\ApiResponseService($app);
        });
    }
}
