<?php
namespace App\Providers;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Storage;

class ModulesServiceProvider extends ServiceProvider
{
    protected $modules = [];

    /**
     * 模块默认配置
     *
     * @var array $modulesDefaultConfig
     */
    protected $modulesDefaultConfig = [
        'route' => []
    ];

    /**
     * 注册应用服务
     *
     * @return void
     */
    public function register()
    {
        $this->registerFilesystemsDisks();
        $this->registerModules();
    }

    /**
     * 启动服务.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * 注册模块应用
     *
     * @return void
     */
    protected function registerModules()
    {
        $_storage = Storage::disk('modules');

        $modules = $_storage->directories();
        array_map(function($modulesName) use ($_storage) {
            $this->modules[$modulesName] = [
                'path'   => modules_path($modulesName),
                'config_path' => sprintf('%s/modules.php', $modulesName),
            ];

            // 引入模块配置
            if ( $_storage->exists($this->modules[$modulesName]['config_path']) ){
                $_modulesConfig = require modules_path($this->modules[$modulesName]['config_path']);
            }else{
                $_modulesConfig = $this->modulesDefaultConfig;
            }
            $this->app['config']->set('modules.app.modules.' . $modulesName, $_modulesConfig);

            //
            $_viewPaths = $this->app['config']->get('view.paths');
            $_viewPaths[] = resource_path('views/'. $modulesName);
            $this->app['config']->set('view.paths', $_viewPaths);

        }, $modules);

        $this->registerModulesRoute();
    }

    /**
     * 注册模块应用路由
     *
     * @return void
     */
    protected function registerModulesRoute()
    {
        $_modules = $this->app['config']->get('modules.app.modules');
        array_walk($_modules, function($config, $name){
            array_map(function($route) use ($name){
                $_routePath = $route['group'];unset($route['group']);
                $route['namespace'] = empty($route['namespace'])?'Modules\\'.$name.'\\Http\\Controllers':$route['namespace'];
                $this->app['router']->group($route, $_routePath);
            }, $config['route']);
        });
    }

    /**
     * 注册模块应用文件驱动配置
     *
     * @return void
     */
    protected function registerFilesystemsDisks()
    {
        $this->app['config']->set('filesystems.disks.modules',[
            'driver' => 'local',
            'root' => modules_path(),
        ]);
    }

}
