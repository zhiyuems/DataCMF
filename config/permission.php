<?php

return [
    /**
     * 此处设置超级权限角色标识
     */
    'SuperAdministrator' => 'administrator',

    'models' => [

        /*
         * 当使用这个包中的"HasPermissions"特性时，我们需要知道
         * 使用哪个 Eloquent 模型来获取您的权限。
         * 当然，它通常只是"权限（Permission）"模型，你也可以使用任何你喜欢的模型。
         *
         * 您使用的权限模型必须实现
         * `Spatie\Permission\Contracts\Permission` 契约。
         */

        'permission' => App\Models\Permission\Permission::class,

        /*
         * 当使用这个包中的 "HasRoles" 特性时，
         * 我们需要知道应该使用哪个 Eloquent 模型来检索你的角色。
         * 当然，它通常只是 "角色（Role）" 模型，你也可以使用任何你喜欢的模型。
         *
         * 您使用的权限模型必须实现
         * `Spatie\Permission\Contracts\Role` 契约。
         */

        'role' => App\Models\Permission\Roles::class,

    ],

    'table_names' => [

        /*
         * 当使用这个包中的 "HasRoles" 特性时，
         * 我们需要知道哪个表应该用来检索你的"角色"。
         * 我们选择了一个基本的默认值，但您可以轻松将其更改为您喜欢的。
         */

        'roles' => 'system_roles',

        /*
         * 当使用这个包中的 "HasRoles" 特性时，
         * 我们需要知道哪个表应该用来检索你的权限。
         * 我们选择了一个基本的默认值，但您可以轻松将其更改为您喜欢的任何表。
         */

        'permissions' => 'system_permissions',

        /*
         * 当使用这个包中的 "HasRoles" 特征时，
         * 我们需要知道应该使用哪个表来检索你的"模型权限"。
         * 我们选择了一个基本的默认值，但您可以轻松将其更改为您喜欢的任何表。
         */

        'model_has_permissions' => 'system_model_has_permissions',

        /*
         * 当使用这个包中的 "HasRoles" 特性时，
         * 我们需要知道哪个表应该用来检索你的"模型角色"。
         * 我们选择了一个基本的默认值，但您可以轻松将其更改为您喜欢的任何表。
         */

        'model_has_roles' => 'system_model_has_roles',

        /*
         * 当使用这个包中的 "HasRoles" 特性时，
         * 我们需要知道应该使用哪个表来检索您的"角色权限"。
         * 我们选择了一个基本的默认值，但您可以轻松将其更改为您喜欢的任何表。
         */

        'role_has_permissions' => 'system_roles_has_permission',
    ],

    'column_names' => [
        /*
         * 如果要命名默认值以外的相关轴，请更改此选项
         */
        'role_pivot_key' => 'role_id', //default 'role_id',
        'permission_pivot_key' => 'permission_id', //default 'permission_id',

        /*
         * 如果要将相关模型主键命名为
         * `model_id`.
         *
         * 例如，如果您的主键都是UUID，这将非常好。在里面
         * 在这种情况下，将其命名为"model_uuid"。
         */

        'model_morph_key' => 'model_id',

        /*
         * 如果要使用团队功能和相关模型的
         * 外键是 `team_id`.
         */

        'team_foreign_key' => 'team_id',
    ],

    /*
     * 当设置为true时，检查权限的方法将注册到gate上。
     * 如果要实现用于检查权限的自定义逻辑，请将其设置为false。
     */

    'register_permission_check_method' => true,

    /*
     * 当设置为true时，包使用"team_foreign_key"实现团队。如果你愿意
     * 要注册"team_foreign_key"的迁移，必须将其设置为true
     * 在进行迁移之前。如果您已经进行了迁移，那么您必须进行新的迁移
     * 迁移到"角色"、"模型角色"和"模型角色"中，同时添加"团队外部密钥"
     * "model_拥有_权限"（查看包的迁移文件的最新版本）
     */

    'teams' => true,

    /*
     * 设置为true时，所需的权限名称将添加到异常中
     * 信息。在某些情况下，这可能被视为信息泄露，所以
     * 为获得最佳安全性，此处的默认设置为false。
     */

    'display_permission_in_exception' => (bool) env('APP_DEBUG', false),

    /*
     * 当设置为true时，所需的角色名称将添加到异常中
     * 信息。在某些情况下，这可能被视为信息泄露，所以
     * 为获得最佳安全性，此处的默认设置为false。
     */

    'display_role_in_exception' => (bool) env('APP_DEBUG', false),

    /*
     * 默认情况下，禁用通配符权限查找。
     */

    'enable_wildcard_permission' => false,

    'cache' => [

        /*
         * 默认情况下，所有权限都会缓存24小时以提高性能。
         * 权限或角色更新后，缓存将自动刷新。
         */

        'expiration_time' => \DateInterval::createFromDateString('24 hours'),

        /*
         * 用于存储所有权限的缓存Key。
         */

        'key' => 'spatie.permission.cache',

        /*
         * 您可以选择指定特定的缓存驱动程序以用于权限和
         * 使用缓存中列出的任何"存储"驱动程序进行角色缓存。php配置
         * 档案。这里使用"default"意味着使用缓存中设置的"default"。php。
         */

        'store' => 'default',
    ],
];
