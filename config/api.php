<?php
/**
 * Api接口公共配置
 */

return [
    // 忽略sign验证接口
    'sign_routes' => [
        '/bin/upload/image',
        '/bin/upload/file',
    ],

    // API响应格式配置
    'response_configure' => [
        'code' => 'code',
        'msg'  => 'msg',
        'sub_code' => 'sub_code',
        'sub_msg' => 'sub_msg',
        'data' => 'data',
    ],
];
