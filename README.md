[![Php Version](https://img.shields.io/badge/php-%3E=7.4.0-brightgreen.svg?maxAge=2592000&color=yellow)](https://github.com/php/php-src)
[![Mysql Version](https://img.shields.io/badge/mysql-%3E=5.7-brightgreen.svg?maxAge=2592000&color=orange)](https://www.mysql.com/)
[![laravel Version](https://img.shields.io/badge/laravel-%3E=8.x-brightgreen.svg?maxAge=2592000)](https://github.com/laravel/framework/tree/8.x)
[![DataCMF License](https://img.shields.io/badge/license-MIT-green?maxAge=2592000&color=blue)](https://gitee.com/zhiyuems/DataCMF/blob/master/LICENSE)

[![DataCMF](https://img.shields.io/badge/DataCMF-0.0.1@dev-brightgreen.svg)](https://gitee.com/zhiyuems/DataCMF/)
[![star](https://gitee.com/zhiyuems/DataCMF/badge/star.svg?theme=dark)](https://gitee.com/zhiyuems/DataCMF/stargazers)
[![fork](https://gitee.com/zhiyuems/DataCMF/badge/fork.svg?theme=dark)](https://gitee.com/zhiyuems/DataCMF/members)

DataCMF 0.0.1 开发版
===============
***开发版，请不要用于正式环境！实际项目请下载正式版**

## 关于 DataCMF
> DataCMF是一款基于`Laravel8`为核心的免费开源内容管理框架(CMF：Content Management Framework)。\
> 框架自身提供基础的管理功能，而开发者可以根据自身的需求以应用的形式进行扩展。

开发初衷： 减少重复基础功能的重复开发！

## DataCMF 主要特性
* 框架协议依旧为`MIT`,让你更自由地飞
* 基于`Laravel 8.x`开发，参考<a href="https://learnku.com/docs/laravel/8.5">[官方文档]</a>，开箱即用
* 不做大幅度`入侵式`改变，保持Laravel原有目录结构、运行流程，降低入手门槛
* 实现基础常见功能模块，节约重复开发时间
* 前后端完全分离，让前端UI更加自由化放飞
* 基于RBAC角色权限控制管理
* Composer 一键引入三方扩展
* 源码中清晰中文注释

## 后台内置功能

| 功能模块     | 介绍                                          | 版本 | 情况  |
|----------|---------------------------------------------| ----  |-----|
| 菜单管理     | 配置系统菜单，操作权限，按钮权限标识等。                        | v0.0.1  | 已完成 |
| RBAC权限管理 | 权限管理、角色权限分配。                                | v0.0.1  | 已完成 |
| 机构管理     | 配置系统组织机构（公司、部门、岗位），为数据权限奠定基础。               | v0.0.1  | 已完成 |
| 参数管理     | 对系统动态配置常用参数。                                | v0.0.1  | 已完成 |
| 日志管理     | 对系统操作日志、登录日志等进行可视化管理。                       | v0.0.1  | 已完成 |
| 数据字典   | 对系统中经常使用的一些较为固定的数据进行维护。<br/>如：是否、男女、类别、级别等。 | v0.0.1  | 开发中 |
| 模块管理     | 功能模块按需启用。                                   | ----  | 计划  |
| 插件管理     | 插件按需启用。                                     | ----  | 计划  |
| 附件管理     | 对静态资源数据的管理，例如图片、文档等。                        | ----  | 计划  |

## 演示和文档

- 管理后台： 演示 (暂无)
- 管理后台API文档： <a href="https://console-docs.apipost.cn/cover.html?url=06d3986556c95309&salt=ff3903b773a3f7a4">文档</a> (持续更新中...)

## 源码下载

后端端 (又称服务端，用于管理后台和提供api接口)： \
`https://gitee.com/zhiyuems/DataCMF`

前端 (指DataCMF前端代码，基于SCUI Admin实现): \
`https://gitee.com/zhiyuems/DataCMF-Admin`


## 安装教程

>DataCMF 使用 Composer 来管理项目依赖。因此，在使用 DataCMF 之前，请确保你的机器已经安装了 Composer。

#### 通过git下载安装包，composer安装依赖包

```bash
第一步，下载安装包

git clone https://gitee.com/zhiyuems/DataCMF.git


第二步，安装依赖包
composer install

# 修改.env配置信息（若不存在，将.env.example修改为.env）
# 基础修改信息：数据库连接、redis缓存信息

# 注：ADMIN_API_URL、ADMIN_API_KEY为默认Admin后台接口地址及sign校验盐
# 若需要自定义后台访问路径可修改app/Modules/Admin/modules.php文件

第三步，迁移数据库
php artisan migrate

第四步，导入数据
php artisan db:seed

# 若无法全部导入可采用以下方式：
php artisan db:seed --class=ConfigureSeeder
php artisan db:seed --class=PermissionSeeder
php artisan db:seed --class=DeptSeeder
php artisan db:seed --class=JobsSeeder
```
## 代码风格
* PHP7强类型严格模式
* 严格遵守MVC设计模式 同时具有service层和枚举类enum支持
* 简约整洁的编码风格 绝不冗余一行代码
* 代码注释完整易读性高 尽量保障初级程序员也可读懂 极大提升二开效率
* 不允许直接调用和使用DB类（破坏封装性）
* 不允许使用原生SQL语句 全部使用链式操作（可维护性强）
* 不允许存在复杂SQL查询语句（可维护性强）
* 所有的CURD操作均通过ORM模型类 并封装方法（扩展性强）

## 鸣谢
DataCMF 基于以下扩展包:
- <a href="https://github.com/laravel/laravel">Laravel</a>
- <a href="https://github.com/laravel/sanctum">LaravelSanctum</a>
- <a href="https://github.com/spatie/laravel-permission">Permission</a>
- <a href="https://github.com/zedisdog/laravel-schema-extend">SchemaExtend</a>
- <a href="https://github.com/myclabs/php-enum">PhpEnum</a>
- <a href="https://gitee.com/nxqf/tool-library">ToolLibrary</a>

## 支持
如果觉得本项目还不错或在工作中有所启发，请在Gitee(码云)帮开发者点亮星星，这是对开发者最大的支持和鼓励！

## License
DataCMF is licensed under <a href="https://gitee.com/zhiyuems/DataCMF/blob/master/LICENSE">The MIT License (MIT)</a>.
