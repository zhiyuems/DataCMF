<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;
use App\Bin\Enum\Common AS CommonEnum;

class CreateSystemJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_jobs', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('岗位名称');       // For MySQL 8.0 use string('name', 125);
            $table->string('code',50)->nullable()->comment('岗位编码');
            $table->unsignedMediumInteger('sort')->default(50)->nullable()->comment('显示排序');
            $table->char('status',1)->default(CommonEnum::STATUS_OPEN)->nullable()->comment('状态(Y正常 N停用)');
            $table->timestamps();
            $table->comment = '系统 - RBAC - 岗位';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_jobs');
    }
}
