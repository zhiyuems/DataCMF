<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateSystemLogsLoginTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_logs_login', function (Blueprint $table) {
            $table->id();
            $table->string('account')->comment('登录账号');
            $table->ipAddress('ip_address')->comment('客户端IP');
            $table->string('browser')->comment('浏览器');
            $table->string('system')->comment('操作系统');
            $table->text('response')->nullable()->comment('响应内容');
            $table->string('notes')->nullable()->comment('注释');
            $table->timestamps();

            $table->comment = '系统 - 日志 - 登录记录';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_logs_login');
    }
}
