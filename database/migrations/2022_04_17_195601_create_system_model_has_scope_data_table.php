<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateSystemModelHasScopeDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_model_has_scope_data', function (Blueprint $table) {
            $table->string('model_type')->comment('数据模型');
            $table->json('model_rule')->comment('模型规则');

            // 索引
            $table->index(['model_type'], 'model_has_scope_data_model_type_index');

            // 主键
            $table->primary(['model_type'],'model_has_scope_data_model_type_primary');

            $table->comment = '系统 - RBAC - 模型数据范围';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_model_has_scope_data');
    }
}
