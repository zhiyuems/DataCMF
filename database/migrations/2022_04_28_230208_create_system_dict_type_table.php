<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;
use App\Bin\Enum\Common AS CommonEnum;

class CreateSystemDictTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_dict_type', function (Blueprint $table) {
            $table->string('dict_label')->comment('字典名称(表单展示)');
            $table->string('dict_key')->comment('字典类型(编码)');
            $table->string('remark')->nullable()->comment('字典备注');
            $table->char('status',1)->default(CommonEnum::STATUS_OPEN)->nullable()->comment('状态(Y正常 N停用)');
            $table->timestamps();

            // 索引
            $table->index(['dict_key'], 'system_dict_type_dict_key_index');

            // 主键
            $table->primary(['dict_key'],'system_dict_type_dict_key_primary');

            $table->comment = '系统 - 数据字典 - 字典类型';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_dict_type');
    }
}
