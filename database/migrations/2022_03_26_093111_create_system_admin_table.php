<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;
use App\Bin\Enum\UserInfo;

class CreateSystemAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_admin', function (Blueprint $table) {
            $table->id();
            $table->string('avatar')->nullable()->comment('账号头像');
            $table->string('username')->comment('账号昵称');
            $table->string('account')->unique()->comment('登录账号');
            $table->string('password')->comment('登录密码');
            $table->string('email')->nullable()->comment('电子邮箱');
            $table->date('birthday')->nullable()->comment('生日');
            $table->char('sex',1)->nullable()->default(UserInfo::SEX_UNKNOWN)->comment('性别(M男 F女 U未知)');
            $table->string('signature', 100)->nullable()->comment('个性签名');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes()->comment('软删除时间');
            $table->comment = '系统 - RBAC - 管理员基础信息';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_admin');
    }
}
