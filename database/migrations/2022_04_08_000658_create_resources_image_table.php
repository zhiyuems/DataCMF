<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateResourcesImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources_image', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->id();
            $table->string('file_name',120)->unique()->comment('图片文件名称');
            $table->string('file_path')->comment('存储路径(相对路径)');
            $table->string('file_root_path')->comment('服务器存储路径');
            $table->unsignedInteger('file_size')->comment('文件大小(字节)');
            $table->string('file_suffix',20)->comment('文件后缀');
            $table->string('file_mime',100)->comment('文件MIME类型');
            $table->char('file_hash',32)->unique()->comment('文件HASH');
            $table->timestamps();
            $table->comment = '资源 - 图标资源库';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resources_image');
    }
}
