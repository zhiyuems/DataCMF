<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;
use App\Bin\Enum\Common AS CommonEnum;

class CreateSystemDictDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_dict_details', function (Blueprint $table) {
            $table->string('dict_type')->comment('关联字典类型');
            $table->string('dict_code')->comment('字典编码');
            $table->string('dict_label')->comment('字典名称(表单展示)');
            $table->string('dict_value')->comment('字典键值');
            $table->string('remark')->nullable()->comment('字典备注');
            $table->unsignedMediumInteger('sort')->default(50)->nullable()->comment('显示排序');
            $table->char('status',1)->default(CommonEnum::STATUS_OPEN)->nullable()->comment('状态(Y正常 N停用)');
            $table->timestamps();

            // 索引
            $table->index(['dict_type','dict_code'], 'system_dict_details_dict_type_dict_code_index');

            // 外键约束
            $table->foreign('dict_type')
                ->references('dict_key')
                ->on('system_dict_type')
                ->onDelete('cascade');

            // 主键
            $table->primary(['dict_type','dict_code'],'system_dict_details_dict_type_dict_code_primary');

            $table->comment = '系统 - 数据字典 - 字典明细(详情)';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_dict_details');
    }
}
