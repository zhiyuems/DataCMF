<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateSystemModelHasJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_model_has_jobs', function (Blueprint $table) {
            $table->unsignedBigInteger('jobs_id')->comment('岗位ID');

            $table->string('model_type')->comment('用户模型');
            $table->unsignedBigInteger('model_id')->comment('用户ID');

            // 索引
            $table->index(['model_id', 'model_type'], 'model_has_jobs_model_id_model_type_index');

            // 外键约束
            $table->foreign('jobs_id')
                ->references('id')
                ->on('system_jobs')
                ->onDelete('cascade');

            // 主键
            $table->primary(['jobs_id', 'model_id', 'model_type'],
                'model_has_jobs_job_model_type_primary');

            $table->comment = '系统 - RBAC - 用户关联岗位';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_model_has_jobs');
    }
}
