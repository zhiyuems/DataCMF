<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateSystemConfigureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_configure', function (Blueprint $table) {
            $table->id();
            $table->string('variable_label',20)->comment('展示名称');
            $table->string('variable_name',20)->unique()->comment('变量名称');
            $table->string('variable_value')->nullable(true)->comment('变量值');
            $table->string('variable_tip',100)->nullable(true)->comment('变量提示');
            $table->string('variable_describe')->nullable(true)->comment('变量备注');
            $table->char('is_systemvar',1)->default('N')->comment('是否是系统变量(Y是 N否)');
            $table->string('group',10)->default('system')->comment('分组');
            $table->string('group_son',10)->nullable(true)->default('default')->comment('子分组');
            $table->timestamps();
            $table->comment = '系统 - 系统全局配置';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_configure');
    }
}
