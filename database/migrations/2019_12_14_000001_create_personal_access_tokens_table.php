<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;

class CreatePersonalAccessTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_access_tokens', function (Blueprint $table) {
            $table->id();
            // 快捷创建以tokenable为前缀的，type、id字段
            // 用户指定授权模型及模型对应ID
            $table->morphs('tokenable');
            $table->string('name')->comment('别名Token');
            $table->string('token', 64)->unique()->comment('授权Token');
            // 预留字段
            $table->text('abilities')->nullable()->comment('权限能力控制');
            $table->timestamp('last_used_at')->nullable()->comment('最后一次使用时间');
            // 创建/更新时间
            $table->timestamps();
            $table->comment = '登录令牌记录表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_access_tokens');
    }
}
