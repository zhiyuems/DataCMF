<?php

//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\PermissionRegistrar;

class CreatePermissionTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableNames = config('permission.table_names');
        $columnNames = config('permission.column_names');
        $teams = config('permission.teams');

        if (empty($tableNames)) {
            throw new \Exception('Error: config/permission.php not loaded. Run [php artisan config:clear] and try again.');
        }
        if ($teams && empty($columnNames['team_foreign_key'] ?? null)) {
            throw new \Exception('Error: team_foreign_key on config/permission.php not loaded. Run [php artisan config:clear] and try again.');
        }

        Schema::create($tableNames['permissions'], function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('parent_id')->nullable()->default(0)->comment('上级菜单ID');
            $table->string('name')->comment('权限标识');       // For MySQL 8.0 use string('name', 125);
            $table->string('api_url')->nullable()->comment('请求地址');
            $table->string('view_name')->comment('路由的标识，必填且与页面组件的name一致，否则 keep-alive 页面缓存效果失效。');
            $table->string('path')->nullable()->comment('前端-路由路径(在游览器地址栏里的hash值)');
            $table->string('component')->nullable()->comment('前端-页面组件位置');
            $table->string('redirect')->nullable()->comment('前端-重定向地址');
            $table->string('meta_title')->comment('菜单名称');
            $table->unsignedInteger('meta_hidden')->nullable()->default(1)->comment('菜单隐藏(1显示 0隐藏)');
            $table->unsignedInteger('meta_affix')->nullable()->default(0)->comment('菜单固定(0不固定 1固定)');
            $table->string('meta_icon',100)->nullable()->comment('菜单图标');
            $table->char('meta_type',12)->default('catalogue')->comment('菜单类型(catalogue目录 menu菜单 iframe内嵌页面 link外链 button按钮)');
            $table->unsignedInteger('meta_hiddenBreadcrumb')->nullable()->default(1)->comment('隐藏面包屑(1显示 0隐藏)');
            $table->string('meta_active')->nullable()->comment('前端-高亮左侧菜单的路由地址');
            $table->char('meta_color',7)->nullable()->comment('前端-菜单颜色值');
            $table->unsignedInteger('meta_fullpage')->default(0)->nullable()->comment('前端-整页打开(0否 1是)');
            $table->unsignedMediumInteger('sort')->default(50)->nullable()->comment('菜单排序');
            $table->string('guard_name'); // For MySQL 8.0 use string('guard_name', 125);
            $table->timestamps();

            $table->unique(['name', 'guard_name']);
            $table->comment = '系统 - RBAC - 后台菜单(权限明细)';
        });

        Schema::create($tableNames['roles'], function (Blueprint $table) use ($teams, $columnNames) {
            $table->bigIncrements('id');
            if ($teams || config('permission.testing')) { // permission.testing is a fix for sqlite testing
                $table->unsignedBigInteger($columnNames['team_foreign_key'])->nullable();
                $table->index($columnNames['team_foreign_key'], 'roles_team_foreign_key_index');
            }
            $table->string('name');       // For MySQL 8.0 use string('name', 125);
            $table->string('alias')->comment('角色别名');       // For MySQL 8.0 use string('alias', 125);
            $table->unsignedBigInteger('parent_id')->nullable()->default(0)->comment('上级角色ID');
            $table->unsignedMediumInteger('sort')->default(50)->nullable()->comment('角色排序');
            $table->string('guard_name'); // For MySQL 8.0 use string('guard_name', 125);
            $table->timestamps();
            if ($teams || config('permission.testing')) {
                $table->unique([$columnNames['team_foreign_key'], 'name', 'guard_name']);
            } else {
                $table->unique(['name', 'guard_name']);
            }
            $table->comment = '系统 - RBAC - 角色基础信息';
        });

        Schema::create($tableNames['model_has_permissions'], function (Blueprint $table) use ($tableNames, $columnNames, $teams) {
            $table->unsignedBigInteger(PermissionRegistrar::$pivotPermission);

            $table->string('model_type')->comment('权限模型路径(命名空间)');
            $table->unsignedBigInteger($columnNames['model_morph_key'])->comment('权限模型主键');
            $table->index([$columnNames['model_morph_key'], 'model_type'], 'model_has_permissions_model_id_model_type_index');

            $table->foreign(PermissionRegistrar::$pivotPermission)
                ->references('id')
                ->on($tableNames['permissions'])
                ->onDelete('cascade');
            if ($teams) {
                $table->unsignedBigInteger($columnNames['team_foreign_key']);
                $table->index($columnNames['team_foreign_key'], 'model_has_permissions_team_foreign_key_index');

                $table->primary([$columnNames['team_foreign_key'], PermissionRegistrar::$pivotPermission, $columnNames['model_morph_key'], 'model_type'],
                    'model_has_permissions_permission_model_type_primary');
            } else {
                $table->primary([PermissionRegistrar::$pivotPermission, $columnNames['model_morph_key'], 'model_type'],
                    'model_has_permissions_permission_model_type_primary');
            }
            $table->comment = '系统 - RBAC - 用户关联权限';
        });

        Schema::create($tableNames['model_has_roles'], function (Blueprint $table) use ($tableNames, $columnNames, $teams) {
            $table->unsignedBigInteger(PermissionRegistrar::$pivotRole);

            $table->string('model_type')->comment('角色模型路径(命名空间)');
            $table->unsignedBigInteger($columnNames['model_morph_key'])->comment('角色模型主键');
            $table->index([$columnNames['model_morph_key'], 'model_type'], 'model_has_roles_model_id_model_type_index');

            $table->foreign(PermissionRegistrar::$pivotRole)
                ->references('id')
                ->on($tableNames['roles'])
                ->onDelete('cascade');
            if ($teams) {
                $table->unsignedBigInteger($columnNames['team_foreign_key']);
                $table->index($columnNames['team_foreign_key'], 'model_has_roles_team_foreign_key_index');

                $table->primary([$columnNames['team_foreign_key'], PermissionRegistrar::$pivotRole, $columnNames['model_morph_key'], 'model_type'],
                    'model_has_roles_role_model_type_primary');
            } else {
                $table->primary([PermissionRegistrar::$pivotRole, $columnNames['model_morph_key'], 'model_type'],
                    'model_has_roles_role_model_type_primary');
            }
            $table->comment = '系统 - RBAC - 用户关联角色';
        });

        Schema::create($tableNames['role_has_permissions'], function (Blueprint $table) use ($tableNames) {
            $table->unsignedBigInteger(PermissionRegistrar::$pivotPermission)->comment('权限模型ID');
            $table->unsignedBigInteger(PermissionRegistrar::$pivotRole);

            $table->foreign(PermissionRegistrar::$pivotPermission)
                ->references('id')
                ->on($tableNames['permissions'])
                ->onDelete('cascade');

            $table->foreign(PermissionRegistrar::$pivotRole)
                ->references('id')
                ->on($tableNames['roles'])
                ->onDelete('cascade');

            $table->primary([PermissionRegistrar::$pivotPermission, PermissionRegistrar::$pivotRole], 'role_has_permissions_permission_id_role_id_primary');

            $table->comment = '系统 - RBAC - 角色关联权限';
        });

        app('cache')
            ->store(config('permission.cache.store') != 'default' ? config('permission.cache.store') : null)
            ->forget(config('permission.cache.key'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tableNames = config('permission.table_names');

        if (empty($tableNames)) {
            throw new \Exception('Error: config/permission.php not found and defaults could not be merged. Please publish the package configuration before proceeding, or drop the tables manually.');
        }

        Schema::drop($tableNames['role_has_permissions']);
        Schema::drop($tableNames['model_has_roles']);
        Schema::drop($tableNames['model_has_permissions']);
        Schema::drop($tableNames['roles']);
        Schema::drop($tableNames['permissions']);
    }
}
