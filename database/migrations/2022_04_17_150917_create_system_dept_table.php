<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;
use App\Bin\Enum\Common AS CommonEnum;

class CreateSystemDeptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_dept', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('parent_id')->nullable()->default(0)->comment('上级部门ID');
            $table->string('name')->comment('部门名称');       // For MySQL 8.0 use string('name', 125);
            $table->string('leader',50)->nullable()->comment('部门负责人');
            $table->char('phone',11)->nullable()->comment('联系电话');
            $table->string('email')->nullable()->comment('电子邮箱');
            $table->unsignedMediumInteger('sort')->default(50)->nullable()->comment('显示排序');
            $table->char('status',1)->default(CommonEnum::STATUS_OPEN)->nullable()->comment('状态(Y正常 N停用)');
            $table->timestamps();
            $table->comment = '系统 - RBAC - 部门';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_dept');
    }
}
