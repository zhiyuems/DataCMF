<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateSystemModelHasDeptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_model_has_dept', function (Blueprint $table) {
            $table->unsignedBigInteger('dept_id')->comment('部门ID');

            $table->string('model_type')->comment('用户或角色模型');
            $table->unsignedBigInteger('model_id')->comment('用户或角色ID');

            // 索引
            $table->index(['model_id', 'model_type'], 'model_has_dept_model_id_model_type_index');

            // 外键约束
            $table->foreign('dept_id')
                ->references('id')
                ->on('system_dept')
                ->onDelete('cascade');

            // 主键
            $table->primary(['dept_id', 'model_id', 'model_type'],
                'model_has_depts_dept_model_type_primary');

            $table->comment = '系统 - RBAC - 模型关联部门';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_model_has_dept');
    }
}
