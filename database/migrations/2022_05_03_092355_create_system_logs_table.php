<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateSystemLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_logs', function (Blueprint $table) {
            $table->id();
            $table->char('log_type', 10)->comment('日志类型');
            $table->char('log_level', 10)->comment('日志等级');
            $table->text('header')->comment('请求头信息');
            $table->ipAddress('ip_address')->comment('客户端IP');
            $table->string('url')->comment('请求接口');
            $table->text('param')->nullable()->comment('请求参数');
            $table->char('method', 8)->comment('请求方法');
            $table->text('response')->nullable()->comment('响应内容');
            $table->string('operator')->default('0')->comment('操作人');
            $table->string('notes')->nullable()->comment('注释');
            $table->string('class_name')->nullable()->comment('操作类');
            $table->string('class_method')->nullable()->comment('操作类方法');
            $table->timestamps();

            $table->comment = '系统 - 日志 - 系统记录';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_logs');
    }
}
