<?php
namespace Database\Factories\System;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\System\Jobs AS mJobs;
use App\Bin\Enum\Common AS CommonEnum;


class JobsFactory extends Factory
{
    /**
     * 定义工厂对应的模型
     *
     * @var string
     */
    protected $model = mJobs::class;

    /**
     * 定义模型的默认值.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => '知玥科技',
            'code' => 'DataCMF',
            'sort'   => 50,
            'status' => CommonEnum::STATUS_OPEN,
        ];
    }
}
