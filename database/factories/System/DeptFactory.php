<?php
namespace Database\Factories\System;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\System\Dept AS mDept;
use App\Bin\Enum\Common AS CommonEnum;


class DeptFactory extends Factory
{
    /**
     * 定义工厂对应的模型
     *
     * @var string
     */
    protected $model = mDept::class;

    /**
     * 定义模型的默认值.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'parent_id' => 0,
            'name' => '知玥科技',
            'leader' => 'DataCMF',
            'phone'  => '13688888888',
            'email'  => 'zy@qq.com',
            'sort'   => 50,
            'status' => CommonEnum::STATUS_OPEN,
        ];
    }
}
