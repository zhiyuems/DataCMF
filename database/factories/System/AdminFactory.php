<?php

namespace Database\Factories\System;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\System\Admin AS mAdmin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Bin\Enum\UserInfo;

class AdminFactory extends Factory
{
    /**
     * 定义工厂对应的模型
     *
     * @var string
     */
    protected $model = mAdmin::class;

    /**
     * 定义模型的默认值.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'username' => $this->faker->name(),
            'account' => 'admin',
            'password' => Hash::make(md5('admin123')), // password
            'remember_token' => '',//Str::random(10),
            'sex' => UserInfo::SEX_UNKNOWN,
            'signature' => '正所谓富贵险中求',
        ];
    }
}
