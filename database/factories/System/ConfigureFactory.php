<?php
namespace Database\Factories\System;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\System\Configure AS mConfigure;
use App\Bin\Enum\Common AS CommonEnum;

class ConfigureFactory extends Factory
{
    /**
     * 定义工厂对应的模型
     *
     * @var string
     */
    protected $model = mConfigure::class;

    /**
     * 定义模型的默认值.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'variable_label' => '演示变量', // 展示名称
            'variable_name' => 'demo_variable', // 变量名称
            'variable_value' => '', // 变量值
            'variable_tip' => '', // 变量提示
            'variable_describe' => '', // 变量备注
            'is_systemvar' => CommonEnum::STATUS_OPEN, // 是否是系统变量(Y是 N否)
            'group' => 'system', // 分组
            'group_son' => 'default',
        ];
    }
}
