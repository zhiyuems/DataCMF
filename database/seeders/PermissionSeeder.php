<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\System\Admin AS mAdmin;

class PermissionSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tableNames = config('permission.table_names');

        // 创建菜单数据
        $this->buildData($this->getDemoPermissionsData(), DB::table($tableNames['permissions']));

        // 创建角色数据
        $this->buildData($this->getDemoRolesData(), DB::table($tableNames['roles']));

        mAdmin::factory(1)->create(['avatar'=>'img/avatar.jpg','username'=>'DataCMF','account'=>'admin']);
        mAdmin::factory(1)->create(['avatar'=>'img/avatar.jpg','username'=>'DataCMF测试','account'=>'test']);

        // 创建账户关联角色数据
        $this->buildData($this->getDemoAdminData(), DB::table($tableNames['model_has_roles']),null);

        // 创建角色关联权限数据
        $this->buildData($this->getDemoRolesPermissionsData(), DB::table($tableNames['role_has_permissions']),null);
    }

    private function buildData($data, $tableModel, $parentId=0)
    {
        foreach ($data AS $insertData)
        {
            $insertData['parent_id'] = $parentId;
            if (!empty($insertData['children'])){
                $_children = $insertData['children'];
                unset($insertData['children']);
                $this->buildData($_children, $tableModel, $tableModel->insertGetId($insertData));
            }else{
                if(is_null($parentId))unset($insertData['parent_id']);
                $tableModel->insert($insertData);
            }
        }
    }

    private function getDemoPermissionsData()
    {
        return [
            [
                'name' => 'home',
                'view_name' => 'home',
                'path' => '/home',
                'meta_title' => '首页',
                'meta_icon' => 'el-icon-eleme-filled',
                'meta_type' => 'catalogue',
                'guard_name' => 'admin',
                'children' => [
                    [
                        'name' => 'system.dashboard',
                        'api_url' => '/system/dashboard',
                        'view_name' => 'dashboard',
                        'path' => '/dashboard',
                        'component' => 'home',
                        'meta_title' => '控制台',
                        'meta_affix' => '1',
                        'meta_icon' => 'el-icon-menu',
                        'meta_type' => 'menu',
                        'guard_name' => 'admin',
                    ],
                    [
                        'name' => 'system.admin.myinfo',
                        'api_url' => '/system/admin/myinfo',
                        'view_name' => 'usercenter',
                        'path' => '/usercenter',
                        'component' => 'usercenter',
                        'meta_title' => '个人信息',
                        'meta_icon' => 'el-icon-user',
                        'meta_type' => 'menu',
                        'guard_name' => 'admin',
                    ],
                    [
                        'name' => 'demo.link.datacmf',
                        'view_name' => 'demo.link.datacmf',
                        'path' => 'https://gitee.com/nxqf/DataCMF',
                        'meta_title' => 'DataCMF',
                        'meta_icon' => 'el-icon-link',
                        'meta_type' => 'link',
                        'guard_name' => 'admin',
                    ],
                    [
                        'name' => 'demo.link.scuidoc',
                        'view_name' => 'demo.link.scuidoc',
                        'path' => 'https://lolicode.gitee.io/scui-doc',
                        'meta_title' => 'SCUI文档',
                        'meta_icon' => 'el-icon-link',
                        'meta_type' => 'iframe',
                        'guard_name' => 'admin',
                    ]
                ]
            ],
            [
                'name' => 'setting',
                'view_name' => 'setting',
                'path' => '/setting',
                'meta_title' => '配置',
                'meta_icon' => 'el-icon-setting',
                'meta_type' => 'catalogue',
                'guard_name' => 'admin',
                'children' => [
                    [
                        'name' => 'system.setting.viewAny',
                        'api_url' => '/system/setting',
                        'view_name' => 'system',
                        'path' => '/setting/system',
                        'component' => 'setting/system',
                        'meta_title' => '系统设置',
                        'meta_icon' => 'el-icon-tools',
                        'meta_type' => 'menu',
                        'guard_name' => 'admin',
                        'children' => [
                            [
                                'name' => 'system.setting.store',
                                'api_url' => '/system/setting',
                                'view_name' => 'setting.store',
                                'meta_title' => '参数修改',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ]
                        ]
                    ],
                    [
                        'name' => 'system.admin',
                        'api_url' => '/system/admin',
                        'view_name' => 'admin',
                        'path' => '/setting/admin',
                        'component' => 'setting/admin',
                        'meta_title' => '管理员',
                        'meta_icon' => 'el-icon-user-filled',
                        'meta_type' => 'menu',
                        'guard_name' => 'admin',
                        'children' => [
                            [
                                'name' => 'system.admin.viewAny',
                                'view_name' => 'admin.viewAny',
                                'meta_title' => '管理员查询',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ],
                            [
                                'name' => 'system.admin.create',
                                'view_name' => 'admin.create',
                                'meta_title' => '创建管理员',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ],
                            [
                                'name' => 'system.admin.update',
                                'view_name' => 'admin.update',
                                'meta_title' => '更新管理员',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ],
                            [
                                'name' => 'system.admin.destroy',
                                'view_name' => 'admin.destroy',
                                'meta_title' => '删除管理员',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ],
                        ]
                    ],
                    [
                        'name' => 'system.role',
                        'api_url' => '/system/role',
                        'view_name' => 'role',
                        'path' => '/setting/role',
                        'component' => 'setting/role',
                        'meta_title' => '角色管理',
                        'meta_icon' => 'el-icon-notebook',
                        'meta_type' => 'menu',
                        'guard_name' => 'admin',
                        'children' => [
                            [
                                'name' => 'system.role.viewAny',
                                'view_name' => 'role.viewAny',
                                'meta_title' => '角色查询',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ],
                            [
                                'name' => 'system.role.create',
                                'view_name' => 'role.create',
                                'meta_title' => '创建角色',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ],
                            [
                                'name' => 'system.role.update',
                                'view_name' => 'role.update',
                                'meta_title' => '更新角色',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ],
                            [
                                'name' => 'system.role.destroy',
                                'view_name' => 'role.destroy',
                                'meta_title' => '删除角色',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ],
                        ]
                    ],
                    [
                        'name' => 'system.menu',
                        'api_url' => '/system/menu',
                        'view_name' => 'settingMenu',
                        'path' => '/setting/menu',
                        'component' => 'setting/menu',
                        'meta_title' => '菜单管理',
                        'meta_icon' => 'el-icon-fold',
                        'meta_type' => 'menu',
                        'guard_name' => 'admin',
                        'children' => [
                            [
                                'name' => 'system.menu.viewAny',
                                'view_name' => 'settingMenu.viewAny',
                                'meta_title' => '菜单查询',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ],
                            [
                                'name' => 'system.menu.create',
                                'view_name' => 'settingMenu.create',
                                'meta_title' => '创建菜单',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ],
                            [
                                'name' => 'system.menu.update',
                                'view_name' => 'settingMenu.update',
                                'meta_title' => '更新菜单',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ],
                            [
                                'name' => 'system.menu.destroy',
                                'view_name' => 'settingMenu.destroy',
                                'meta_title' => '删除菜单',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ],
                        ]
                    ],
                    [
                        'name' => 'system.dept',
                        'api_url' => '/system/dept',
                        'view_name' => 'dept',
                        'path' => '/setting/dept',
                        'component' => 'setting/dept',
                        'meta_title' => '部门管理',
                        'meta_icon' => 'sc-icon-tree',
                        'meta_type' => 'menu',
                        'guard_name' => 'admin',
                        'children' => [
                            [
                                'name' => 'system.dept.viewAny',
                                'view_name' => 'dept.viewAny',
                                'meta_title' => '部门查询',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ],
                            [
                                'name' => 'system.dept.create',
                                'view_name' => 'dept.create',
                                'meta_title' => '创建部门',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ],
                            [
                                'name' => 'system.dept.update',
                                'view_name' => 'dept.update',
                                'meta_title' => '更新部门',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ],
                            [
                                'name' => 'system.dept.destroy',
                                'view_name' => 'dept.destroy',
                                'meta_title' => '删除部门',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ],
                        ]
                    ],
                    [
                        'name' => 'system.jobs',
                        'api_url' => '/system/jobs',
                        'view_name' => 'jobs',
                        'path' => '/setting/jobs',
                        'component' => 'setting/jobs',
                        'meta_title' => '岗位管理',
                        'meta_icon' => 'sc-icon-job-position',
                        'meta_type' => 'menu',
                        'guard_name' => 'admin',
                        'children' => [
                            [
                                'name' => 'system.jobs.viewAny',
                                'view_name' => 'jobs.viewAny',
                                'meta_title' => '岗位查询',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ],
                            [
                                'name' => 'system.jobs.create',
                                'view_name' => 'jobs.create',
                                'meta_title' => '创建岗位',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ],
                            [
                                'name' => 'system.jobs.update',
                                'view_name' => 'jobs.update',
                                'meta_title' => '更新岗位',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ],
                            [
                                'name' => 'system.jobs.destroy',
                                'view_name' => 'jobs.destroy',
                                'meta_title' => '删除岗位',
                                'meta_type' => 'button',
                                'meta_hidden' => 0,
                                'guard_name' => 'admin',
                            ],
                        ]
                    ]
                ]
            ],
            [
                'name' => 'develop',
                'view_name' => 'develop',
                'path' => '/develop',
                'meta_title' => '开发',
                'meta_hidden' => '1',
                'meta_icon' => 'sc-icon-code',
                'meta_type' => 'catalogue',
                'guard_name' => 'admin',
                'children' => [
                    [
                        'name' => 'system.dict',
                        'view_name' => 'dict',
                        'path' => '/develop/dict',
                        'component' => 'develop/dict',
                        'meta_title' => '数据字典',
                        'meta_hidden' => '1',
                        'meta_icon' => 'el-icon-document',
                        'meta_type' => 'menu',
                        'guard_name' => 'admin',
                    ],
                    [
                        'name' => 'system.logs',
                        'view_name' => 'log',
                        'path' => '/develop/log',
                        'component' => 'develop/log',
                        'meta_title' => '系统日志',
                        'meta_hidden' => '1',
                        'meta_icon' => 'el-icon-warning',
                        'meta_type' => 'menu',
                        'guard_name' => 'admin',
                    ],
                ]
            ],
        ];
    }

    private function getDemoRolesData()
    {
        return [
            [
                'team_id' => '1',
                'name' => config('permission.SuperAdministrator'),
                'alias' => '超级管理员',
                'guard_name' => 'admin',
            ],
            [
                'team_id' => '1',
                'name' => 'manager',
                'alias' => '普通管理员',
                'guard_name' => 'admin',
            ],
        ];
    }

    private function getDemoAdminData()
    {
        return [
            [
                'role_id' => '1',
                'model_type' => 'Modules\Admin\Models\System\Admin',
                'model_id' => '1',
                'team_id' => '1',
            ],
            [
                'role_id' => '2',
                'model_type' => 'Modules\Admin\Models\System\Admin',
                'model_id' => '2',
                'team_id' => '1',
            ],
        ];
    }

    private function getDemoRolesPermissionsData()
    {
        $_permissionsIds = [1,2,3,4,5,6,7,8];
        foreach ($_permissionsIds AS $key => $id)
        {
            $_permissionsIds[$key] = [
                'permission_id' => $id,
                'role_id' => 2,
            ];
        }
        return $_permissionsIds;
    }
}
