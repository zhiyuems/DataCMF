<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\System\Dept AS mDept;

class DeptSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 创建组织结构演示数据
        $this->buildData($this->getDemoData());
    }

    private function buildData($data, $parentId=0)
    {
        foreach ($data AS $insertData)
        {
            if (!empty($insertData['children'])){
                $this->buildData($insertData['children'],
                    mDept::factory(1)->createOne(['parent_id'=>$parentId,'name'=>$insertData['name']])->id
                );
            }else{
                mDept::factory(1)->createOne(['parent_id'=>$parentId,'name'=>$insertData['name']]);
            }
        }
    }

    private function getDemoData()
    {
        return [
            [
                'name' => '知玥科技',
                'children' => [
                    [
                        'name' => '厦门总公司',
                        'children' => [
                            [
                                'name' => '研发部门',
                            ],
                            [
                                'name' => '市场部门',
                            ],
                            [
                                'name' => '测试部门',
                            ],
                            [
                                'name' => '财务部门',
                            ],
                            [
                                'name' => '运维部门',
                            ],
                        ]
                    ],
                    [
                        'name' => '深圳分公司',
                        'children' => [
                            [
                                'name' => '市场部门',
                            ],
                            [
                                'name' => '财务部门',
                            ],
                        ]
                    ]
                ]
            ]
        ];
    }
}
