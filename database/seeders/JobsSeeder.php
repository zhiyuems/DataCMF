<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\System\Jobs AS mJobs;

class JobsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 创建岗位演示数据
        foreach ($this->getDemoData() AS $item){
            mJobs::factory(1)->create($item);
        }

    }

    private function getDemoData()
    {
        return [
            [
                'name' => '董事长',
                'code' => 'CEO',
            ],
            [
                'name' => '项目经理',
                'code' => 'PM',
            ],
            [
                'name' => '人力资源',
                'code' => 'HR',
            ],
            [
                'name' => '普通员工',
                'code' => 'USER',
            ],
        ];
    }
}
