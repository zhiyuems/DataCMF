<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\System\Configure AS mConfigure;

class ConfigureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 创建系统变量演示数据
        $this->buildData($this->getDemoData());
    }

    private function buildData($data)
    {
        foreach ($data AS $insertData)
        {
            mConfigure::factory(1)->createOne($insertData);
        }
    }

    private function getDemoData()
    {
        return [
            [
                'variable_label' => '网站名称',
                'variable_name' => 'site_name',
                'variable_value' => 'DataCMF',
            ],
            [
                'variable_label' => 'ICP备案号',
                'variable_name' => 'site_icp',
            ],
            [
                'variable_label' => '统计代码',
                'variable_name' => 'site_analytics_code',
            ],
            [
                'variable_label' => '版权信息',
                'variable_name' => 'site_copyright',
            ],
            [
                'variable_label' => '演示变量',
                'variable_name' => 'demo_variable',
                'variable_value' => 'false',
                'variable_tip' => '表单创建提示信息',
                'variable_describe' => '变量备注信息',
                'is_systemvar' => \App\Bin\Enum\Common::STATUS_CLOSE,
                'group' => 'extend',
            ]

        ];
    }
}
