<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "admin" middleware group. Enjoy building your API!
|
*/

Route::get('', function(){
    return response('',444);
});

// 通行证处理路由组
Route::group([
    'prefix' => 'passport',
], function(){
    // 管理员登录
    Route::post('login', 'PassportController@toLogin')->name('login');

    // 用户认证模块
    Route::group([
        'middleware' => ['auth:sanctum']
    ],function(){

        // 退出登录
        Route::put('logout', 'PassportController@toLogout');
    });
});

// 独立接口(带登录验证)
Route::group([
    'prefix' => 'bin',
    'namespace' => 'Bin',
    'middleware' => ['auth:sanctum']
], function(){

    // 文件上传
//    Route::middleware(['throttle:uploads'])->group(function(){
        Route::post('upload/{type}', 'UploadController@toUpload')
            ->where(['type'=>'image|file']);
//    });

    // 获取系统环境信息
    Route::get('system-environment', 'SystemController@toEnvironment');

});

// 系统路由组
Route::group([
    'prefix' => 'system',
    'namespace' => 'System',
    'middleware' => ['auth:sanctum'],
], function(){

    // 系统配置信息
    Route::group([
        'prefix' => 'setting',
    ],function(){
        // 获取系统配置信息
        Route::get('', 'SettingController@toAllSetting');

        // 更新或创建系统配置信息
        Route::put('', 'SettingController@toCreateOrUpdate');

        // 批量更新系统配置信息
        Route::put('batch', 'SettingController@toBatchUpdate');
    });

    // 菜单路由组
    Route::group([
        'prefix' => 'menu',
    ], function(){
        // 获取当前管理员后台菜单
        Route::get('my', 'MenuController@toMyMenu');
        // 批量删除指定菜单信息
        Route::delete('batch', 'MenuController@destroyBatch');
    });
    // 菜单CRUD操作
    Route::resource('menu', 'MenuController');

    // 角色路由组
    Route::resource('role', 'RoleController');

    // 指定角色权限配置操作
    Route::group([
        'prefix' => 'role/permission',
    ],function(){
        Route::get('{roleId}', 'RolePermissionController@edit')->where(['roleId'=>'[0-9]+']);
        Route::put('{roleId}', 'RolePermissionController@update')->where(['roleId'=>'[0-9]+']);
    });

    // 管理员路由组(资源路由)
    Route::resource('admin', 'AdminController');

    // 部门路由组(资源路由)
    Route::resource('dept', 'DeptController');

    // 岗位路由组(资源路由)
    Route::resource('jobs', 'JobsController');


    // 数据字典类型路由组(资源路由)
    Route::resource('dict/type', 'DictTypeController');
    // 数据字典明细路由组(资源路由)
    Route::resource('dict', 'DictController');

    // 日志路由组
    Route::group([
        'prefix' => 'logs',
    ],function(){
        // 日志类型
        Route::get('logstype', 'LogsController@toLogsType');
        // 操作日志
        Route::get('list', 'LogsController@toRecordList');
    });

});
