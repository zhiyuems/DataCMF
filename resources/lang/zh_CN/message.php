<?php

return [
    'data' => [
        'no_updatable' => '没有可更新数据',
        'not_exist' => '数据不存在',

        'create' => [
            'success' => ':attribute 信息创建成功。',
            'fail' => ':attribute 信息创建失败。',
        ],
        'update' => [
            'success' => ':attribute 信更新成功。',
            'fail' => ':attribute 信息创建失败。',
            'invalid_parameter' => ':attribute 参数无效。',
        ],
        'delete' => [
            'success' => ':attribute 信息删除成功。',
            'fail' => ':attribute 信息创建失败。',
        ],
        'prohibit_update' => [
            'prohibit' => ':attribute 禁止修改。',
            'exist_data' => ':attribute 存在 :dataname 禁止修改。',
        ],
        'prohibit_delete' => [
            'prohibit' => ':attribute 禁止删除。',
            'exist_data' => ':attribute 存在 :dataname 禁止删除。',
        ],
    ],

    'login' => [
        'success' => '\':attribute\' 登录成功。',
        'fail' => '\':attribute\' 登录失败。',
        'again' => '身份验证失败，请重新登录。'
    ],

    'logout' => [
        'success' => '\':attribute\' 退出成功。',
        'fail' => '\':attribute\' 退出失败.'
    ],
];
