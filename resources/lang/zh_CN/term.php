<?php
/**
 * 术语语言包
 */
return [
    // 昵称
    'username' => '昵称',
    // 账号
    'account' => '账号',
    // 密码
    'password' => '密码',
    // 文件
    'file' => '文件',
    'file_image' => '图片文件',

    // 系统配置
    'sys_setting' => '系统配置',

    // 类型
    'menu_type' => '菜单类型',

    // 管理员
    'administrators' => '管理员',

    // 超级管理员
    'super_administrators' => '超级管理员',

    // 上级
    'superior' => '上级',

    // 角色
    'role' => '角色',
    // 权限标识
    'role_permissionsid' => '权限标识',

    // 权限or菜单
    'permissions' => [
        'name' => '权限',
        'as' => '菜单',
    ],

    // 部门
    'dept' => '部门',

    // 岗位
    'jobs' => '岗位',
    'jobs_code' => '岗位编码',

    // 性别
    'sex' => [
        'man' => '男',
        'female' => '女',
        'unknown' => '未知',
    ]
];
