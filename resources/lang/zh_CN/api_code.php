<?php

return [
    // ApiCode错误码对应语言包
    'error_success' => '接口调用成功',
    'error_service_unavailable' => '服务不可用',
    'error_insufficient_authorization' => '授权权限不足',
    'error_missing_required_arguments' => '缺少必选参数',
    'error_illegal_parameter' => '无效参数',
    'error_business_processing_failed' => '业务处理失败',
    'error_insufficient_permissions' => '用户权限不足',


    // ApiSubCode错误码对应语言包
    'sub' => [
        'unknow_error' => '服务暂不可用',
        'invalid_auth_token' => '无效的访问令牌',
        'invalid_token_time_out' => '访问令牌已过期',
        'missing_signature' => '缺少签名参数',
        'missing_parameter' => '缺少参数',
        'invalid_parameter' => '参数无效',
        'file_upload_fail' => '文件上传失败',
        'file_invalid' => '无效的上传文件',
        'file_fail_extension' => '文件扩展名无效',
        'file_fail_size' => '文件大小无效',
        'file_fail_type' => '上传文件类型暂不支持',
        'invalid_signature' => '无效签名',
        'invalid_toekn' => '无效令牌',
        'data_creation_failed' => '数据创建失败',
        'data_update_failed' => '数据更新失败',
        'data_delete_failed' => '数据删除失败',
        'invalid_login_info' => '登录失败',
        'passport_logout_fail' => '退出失败',
    ]

];
