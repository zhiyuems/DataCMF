<?php
/**
 * 术语语言包
 */
return [
    // 昵称
    'username' => 'Username',
    // 账号
    'account' => 'Account',
    // 密码
    'password' => 'Password',
    // 文件
    'file' => 'File',
    'file_image' => 'Image File',

    // 系统配置
    'sys_setting' => 'System Setting',

    // 类型
    'menu_type' => 'Menu Type',

    // 管理员
    'administrators' => 'Administrators',

    // 超级管理员
    'super_administrators' => 'Super Administrators',

    // 上级
    'superior' => 'Superior',

    // 角色
    'role' => 'Role',
    // 权限标识
    'role_permissionsid' => 'PermissionID',

    // 权限or菜单
    'permissions' => [
        'name' => 'permissions',
        'as' => 'Menu',
    ],

    // 部门
    'dept' => 'Dept',

    // 岗位
    'jobs' => 'Jobs',
    'jobs_code' => 'Jobs Code',

    // 性别
    'sex' => [
        'man' => 'Male',
        'female' => 'Female',
        'unknown' => 'Unknown',
    ]
];
