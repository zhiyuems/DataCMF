<?php

return [
    'data' => [
        'no_updatable' => 'No Updatable Data',
        'not_exist' => 'Data Does Not Exist',

        'create' => [
            'success' => 'The :attribute Information Create Success.',
            'fail' => 'The :attribute Information Update Failed.',
        ],
        'update' => [
            'success' => 'The :attribute Information Update Success.',
            'fail' => 'The :attribute Information Update Failed.',
            'invalid_parameter' => 'The :attribute Invalid Parameter.',
        ],
        'delete' => [
            'success' => 'The :attribute Information Delete Success.',
            'fail' => 'The :attribute Information Delete Failed.',
        ],
        'prohibit_update' => [
            'prohibit' => ':attribute Prohibit Update.',
            'exist_data' => ':attribute Exist :dataname Prohibit Update.',
        ],
        'prohibit_delete' => [
            'prohibit' => ':attribute Prohibit Deletion.',
            'exist_data' => ':attribute Exist :dataname Prohibit Deletion.',
        ],
    ],

    'login' => [
        'success' => 'The \':attribute\' login succeeded.',
        'fail' => 'The \':attribute\' login failed.',
        'again' => 'Authentication Failed, Please Login Again.'
    ],

    'logout' => [
        'success' => 'The \':attribute\' logout succeeded.',
        'fail' => 'The \':attribute\' logout failed.'
    ],
];
