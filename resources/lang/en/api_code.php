<?php

return [
    // ApiCode错误码对应语言包
    'error_success' => 'Interface Call Succeeded.',
    'error_service_unavailable' => 'Service Unavailable.',
    'error_insufficient_authorization' => 'Insufficient Authorization.',
    'error_missing_required_arguments' => 'Missing Required Arguments.',
    'error_illegal_parameter' => 'Invalid Parameter.',
    'error_business_processing_failed' => 'Business Processing Failed.',
    'error_insufficient_permissions' => 'Insufficient User Permissions.',


    // ApiSubCode错误码对应语言包
    'sub' => [
        'unknow_error' => 'Service Temporarily Unavailable.',
        'invalid_auth_token' => 'Invalid Access Token.',
        'invalid_token_time_out' => 'The Access Token Has Expired.',
        'missing_signature' => 'Missing Signature Parameter.',
        'missing_parameter' => 'Missing Parameter.',
        'invalid_parameter' => 'Invalid Parameter.',
        'file_upload_fail' => 'File Upload Failed.',
        'file_invalid' => 'Invalid Upload File',
        'file_fail_extension' => 'Filename Extension Is Invalid.',
        'file_fail_size' => 'Invalid File Size.',
        'file_fail_type' => 'Upload File Type Is Not Supported Temporarily',
        'invalid_signature' => 'Invalid Signature.',
        'invalid_toekn' => 'Invalid Token.',
        'data_creation_failed' => 'Data Creation Failed.',
        'data_update_failed' => 'Data Update Failed.',
        'data_delete_failed' => 'Data Deletion Failed.',
        'invalid_login_info' => 'Login Failed.',
        'passport_logout_fail' => 'Exit Failed.',
    ]

];
